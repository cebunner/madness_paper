import os

# Run all the calculate reactions
os.system("python calc_hf_reactions.py")
os.system("python calc_hf_unc_reactions.py")
os.system("python calc_lda_reactions.py")
os.system("python calc_lda_unc_reactions.py")

# Plot everything
print("\nHF")
os.system("python plot_hf.py")
print("")

print("\nHF unc")
os.system("python plot_hf_unc.py")
print("")

print("\nLDA")
os.system("python plot_lda.py")
print("")

print("\nLDA unc")
os.system("python plot_lda_unc.py")
print("")
