# CB 11/10/17 - A script for calculating reaction energy differences #
# Requirements: 1.) The database file "molecule_database.txt" is in the directory above
#               2.) A directory with the geometry for every molecule is in the same directory
#                   as this script and the files are named as the molecule name in the database file
#                   with no extension
#               3.) All lines that are not commented have an atom identifier and three coordinates (x,
#                   y,z in that order). In particular, any extra lines at the end of the file will
#                   cause this program to crash.

# Important Data Structures:
#
# molecules: The master dictionary that has all 227 molecules in the set

# BS 4/11/18 - Changed script to calculate energies of all reactions in a file that the
#              basis set has complete information for and created another script 
#              (write_reacctions.py) that will write out all possible reactions


import numpy as np

import reaction_tools as rtools
import os

# For HF
basis_sets = ['6-31G*','cc-pvdz','cc-pvtz','cc-pvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz']
basis_colors = dict(zip(basis_sets, ['red','green','purple','orange','teal','brown','gold']))

# HF
MAD_ENERGIES = "../../MAD_Data/mad_hf_defaults.csv"

nw_energy_dict = {}
mad_energy_dict = {}

atob_diff = {}

# Parser for HF NW
# Parsing all the .csv files in ../NWChem_Data
# (Should be 8, one per basis set)
for data_file in os.listdir("../../NWChem_Data/"):
    if not data_file.startswith("hf_unc") and data_file.startswith("hf") and data_file.endswith(".csv"):
        with open("../../NWChem_Data/" + data_file,"r") as nw:
            key = data_file[3:-4]
            for line in nw:
               if not line.startswith(","):
                    molname = line.strip().split(",")[0]
                    if molname not in nw_energy_dict.keys():
                        nw_energy_dict[molname] = {}
                    nw_energy_dict[molname][key] = float(line.strip().split(",")[1])

# Generic MADNESS parser
with open(MAD_ENERGIES,"r") as mad:
    for line in mad:
        if not line.startswith(","):
            mad_energy_dict[line.strip().split(",")[0]] = float(line.strip().split(",")[1])

#######################################################################################################


def atob(output = "atob.txt"):

    # An easy test-case. Let's look at rearrangements.


    # Keep track of products and reactants so that we can write a file showing all reactions. Good for
    # double-checking code and looking for errors
    products = {}
    reactants = {}
    reactant_coeffs = {}
    product_coeffs = {}
    mad_rxn_energy = {}
    nw_rxn_energy = {}
    rxn_natom = {}

    with open("../possible_atob.txt", "r") as inFile:
       for line in inFile:
          if line.startswith("Reaction"):
             # Do nothing
             continue
          else: 
             line = line.split()

             # Parse the line to match values needed from "reactions.py" (the file I copied this from)
             rxn_num = int(line[0])
             num_atoms = int(line[1])
             rea_coeff = int(line[2])
             reactant = line[3]
             pro_coeff = int(line[4])
             product = line[5]

             # Check if we have these molecules 
             if (reactant in nw_energy_dict.keys() and product in nw_energy_dict.keys() and 
                 reactant in mad_energy_dict.keys() and product in mad_energy_dict.keys()):

                # Add reacant/product pair to dictionary, as well as stoichiometric coefficients
                products[rxn_num] = product
                reactants[rxn_num] = reactant
                reactant_coeffs[rxn_num] = rea_coeff
                product_coeffs[rxn_num] = pro_coeff
                rxn_natom[rxn_num] = num_atoms 

                # Calculate the reaction energy using MADNESS energies 
                mad_rxn_energy[rxn_num] = ( (pro_coeff*mad_energy_dict[product]) - (
                  rea_coeff*mad_energy_dict[reactant]) )*627.509
                
                # Calculate the reaction energy using NWChem energies
                # Make sure to not overwrite old basis info
                nw_rxn_energy[rxn_num] = {}             
                for basis_set in basis_sets:
                   try:
                      nw_rxn_energy[rxn_num][basis_set] = ( (pro_coeff*nw_energy_dict[product]
                      [basis_set]) - (rea_coeff*nw_energy_dict[reactant][basis_set]) )*627.509
                   except KeyError:
                      print("Missing reaction in", basis_set, "\n   {} {} --> {} {}".format(rea_coeff, reactant, pro_coeff, product))

             # Tell user we're missing something
             else:
                   print("Potentially missing a reaction in", basis_set, "\n   {} {} --> {} {}".format(rea_coeff, reactant, pro_coeff, product))
               
    # Store the reactions in a file by number
    with open(output,"w") as out:
        for basis in basis_sets:
            out.write("Basis: {}\n".format(basis))
            out.write("{:<38s}{:<32s}{:<32s}{:<20s}{:<20s}{:<20s}{:<20s}\n".format("Reaction Number     NATOM","Reactant","Product","E_rxn (MADNESS)",
              "E_rxn (NWChem)","E_rxn (Diff.)","E_rxn (Scaled)"))
            for rxn_num in products.keys():
                out.write("{:<20d}{:<18d}{:<32s}{:<32s}{:<20.12s}{:<20.12s}{:<20.12s}{:<20.12s}\n".format(rxn_num,rxn_natom[rxn_num],
                  "{} {}".format(reactant_coeffs[rxn_num],reactants[rxn_num]), 
                  "{} {}".format(product_coeffs[rxn_num],products[rxn_num]),
                  "{}".format(mad_rxn_energy[rxn_num]),
                  "{}".format(nw_rxn_energy[rxn_num][basis]),
                  "{}".format(abs(mad_rxn_energy[rxn_num]-nw_rxn_energy[rxn_num][basis])),
                  "{}".format(abs(mad_rxn_energy[rxn_num]-nw_rxn_energy[rxn_num][basis])/rxn_natom[rxn_num])))


#######################################################################################################

def abtoc(output = "abtoc_reactions.txt"):

    # An easy test-case. Let's look at rearrangements.


    # Keep track of products and reactants so that we can write a file showing all reactions. Good for
    # double-checking code and looking for errors
    products = {}
    reactants1 = {}
    reactants2 = {}
    reactant1_coeffs = {}
    reactant2_coeffs = {}
    product_coeffs = {}
    mad_rxn_energy = {}
    nw_rxn_energy = {} 
    rxn_natom = {}

    with open("../possible_abtoc.txt", "r") as inFile:
       for line in inFile:
          if line.startswith("Reaction"):
             # Do nothing
             continue
          else:
             line = line.split()

             # Parse the line to match values needed from "Reactions.py" (the file I copied this from)
             rxn_num = int(line[0])
             num_atoms = int(line[1])
             rea_coeff1 = int(line[2])
             reactant1 = line[3]
             rea_coeff2 = int(line[4])
             reactant2 = line[5]
             pro_coeff = int(line[6])
             product = line[7]

             # Check if we have these molecules 
             if (reactant1 in nw_energy_dict.keys() and reactant2 in nw_energy_dict.keys() and product in nw_energy_dict.keys() and 
                 reactant1 in mad_energy_dict.keys() and reactant2 in mad_energy_dict.keys() and product in mad_energy_dict.keys()):

                # Add reacant/product pair to dictionary, as well as stoichiometric coefficients
                products[rxn_num] = product
                reactants1[rxn_num] = reactant1
                reactant1_coeffs[rxn_num] = rea_coeff1
                reactants2[rxn_num] = reactant2
                reactant2_coeffs[rxn_num] = rea_coeff2
                product_coeffs[rxn_num] = pro_coeff
                rxn_natom[rxn_num] = num_atoms 

                # Calculate the reaction energy using MADNESS energies 
                mad_rxn_energy[rxn_num] = ( (pro_coeff*mad_energy_dict[product]) - (
                  rea_coeff1*mad_energy_dict[reactant1] + rea_coeff2*mad_energy_dict[reactant2]) )*627.509
                
                # Calculate the reaction energy using NWChem energies
                # Make sure to not overwrite old basis info
                nw_rxn_energy[rxn_num] = {}             
                for basis_set in basis_sets:
                   try:
                      nw_rxn_energy[rxn_num][basis_set] = ( (pro_coeff*nw_energy_dict[product]
                      [basis_set]) - (rea_coeff1*nw_energy_dict[reactant1][basis_set] + 
                                      rea_coeff2*nw_energy_dict[reactant2][basis_set]) )*627.509
                   except KeyError:
                      print("Missing reaction in", basis_set, "\n   {} {} + {} {} --> {} {}".format(rea_coeff1, reactant1, rea_coeff2, reactant2, pro_coeff, product))

             # Tell user we're missing something
             else:
                   print("Potentially missing a reaction in", basis_set, "\n   {} {} + {} {} --> {} {}".format(rea_coeff1, reactant1, rea_coeff2, reactant2, pro_coeff, product))
 

    # Store the reactions in a file by number
    with open(output,"w") as out:
        for basis in basis_sets:
            out.write("Basis: {}\n".format(basis))
            out.write("{:<38s}{:<32s}{:<32s}{:<32s}{:<20s}{:<20s}{:<20s}{:<20s}\n".format("Reaction Number     NATOM","Reactant1","Reactant2","Product","E_rxn (MADNESS)",
              "E_rxn (NWChem)","E_rxn (Diff.)","E_rxn (Scaled)"))
            for rxn_num in products.keys():
                out.write("{:<20d}{:<18d}{:<32s}{:<32s}{:<32s}{:<20.12s}{:<20.12s}{:<20.12s}{:<20.12s}\n".format(rxn_num,rxn_natom[rxn_num],
                  "{} {}".format(reactant1_coeffs[rxn_num],reactants1[rxn_num]), 
                  "{} {}".format(reactant2_coeffs[rxn_num],reactants2[rxn_num]), 
                  "{} {}".format(product_coeffs[rxn_num],products[rxn_num]),
                  "{}".format(mad_rxn_energy[rxn_num]),
                  "{}".format(nw_rxn_energy[rxn_num][basis]),
                  "{}".format(abs(mad_rxn_energy[rxn_num]-nw_rxn_energy[rxn_num][basis])),
                  "{}".format(abs(mad_rxn_energy[rxn_num]-nw_rxn_energy[rxn_num][basis])/rxn_natom[rxn_num])))

atob("atob_reactions_hf.txt")
abtoc("abtoc_reactions_hf.txt")
