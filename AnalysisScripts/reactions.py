# CB 11/10/17 - A script for calculating reaction energy differences #
# Requirements: 1.) The database file "molecule_database.txt" is in the directory above
#               2.) A directory with the geometry for every molecule is in the same directory
#                   as this script and the files are named as the molecule name in the database file
#                   with no extension
#               3.) All lines that are not commented have an atom identifier and three coordinates (x,
#                   y,z in that order). In particular, any extra lines at the end of the file will
#                   cause this program to crash.

# Important Data Structures:
#
# molecules: The master dictionary that has all 227 molecules in the set

import numpy as np
from matplotlib import rc
import reaction_tools as rtools
import os

# Some matplotlib stuff
rc('text', usetex=True)
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})

basis_sets = ['6-31g*','cc-pvdz','cc-pvtz','cc-pvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz']
basis_colors = dict(zip(basis_sets, ['red','green','purple','orange','teal','brown','gold']))

# For uncontracted LDA
basis_sets = ['aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz']
basis_colors = dict(zip(basis_sets, ['teal','brown','gold']))


# LDA
NW_ENERGIES = "../NWChem_Data/svwn5_total_energies.txt"
MAD_ENERGIES = "../MAD_Data/mad_lda_defaults.csv"

# HF
#MAD_ENERGIES = "../MAD_Data/mad_hf_defaults.csv"

nw_energy_dict = {}
mad_energy_dict = {}

atob_diff = {}

# Parser for LDA NW
#with open(NW_ENERGIES,"r") as nw:
#    for i, line in enumerate(nw):
#        if i == 0:
#            continue
#        elif i == 1:
#            bsets=line.split()[1:] # Skipping the # at the beginning of line
#        else:
#            clean_line = line.strip().split()
#            molname = clean_line[0]
#            nw_energy_dict[molname] = {}
#            for j, bs in enumerate(bsets):
#                nw_energy_dict[molname][bs] = float(clean_line[j+1])

# Parser for contracted LDA NW
# Parsing all the .csv files in ../NWChem_Data
# (Should be 8, one per basis set)
#for data_file in os.listdir("../NWChem_Data/"):
#    if not data_file.startswith("lda_unc") and data_file.startswith("lda") and data_file.endswith(".csv"):
#        with open("../NWChem_Data/" + data_file,"r") as nw:
#            key = data_file[4:-4]
#            for line in nw:
#               if not line.startswith(","):
#                    molname = line.strip().split(",")[0]
#                    if molname not in nw_energy_dict.keys():
#                        nw_energy_dict[molname] = {}
#                    nw_energy_dict[molname][key] = float(line.strip().split(",")[1])

# Parser for uncontracted LDA NW
# Parsing all the .csv files in ../NWChem_Data
# (Should be 8, one per basis set)
for data_file in os.listdir("../NWChem_Data/"):
    if data_file.startswith("lda_unc") and data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nw:
            key = data_file[8:-4]
            for line in nw:
               if not line.startswith(","):
                    molname = line.strip().split(",")[0]
                    if molname not in nw_energy_dict.keys():
                        nw_energy_dict[molname] = {}
                    nw_energy_dict[molname][key] = float(line.strip().split(",")[1])

# Parser for HF NW
# Parsing all the .csv files in ../NWChem_Data
# (Should be 8, one per basis set)
#for data_file in os.listdir("../NWChem_Data/"):
#    if data_file.startswith("hf") and data_file.endswith(".csv"):
#        with open("../NWChem_Data/" + data_file,"r") as nw:
#            key = data_file[3:-4]
#            for line in nw:
#               if not line.startswith(","):
#                    molname = line.strip().split(",")[0]
#                    if molname not in nw_energy_dict.keys():
#                        nw_energy_dict[molname] = {}
#                    nw_energy_dict[molname][key] = float(line.strip().split(",")[1])

# Generic MADNESS parser
with open(MAD_ENERGIES,"r") as mad:
    for line in mad:
        if not line.startswith(","):
            mad_energy_dict[line.strip().split(",")[0]] = float(line.strip().split(",")[1])

#######################################################################################################

# First things first, make a dictionary that has every molecule in our set as a key. What will we store
# as the values in this dictionary, you might ask? Why another dictionary of course!
molecules = {}

with open("../molecule_database.txt","r") as moldb:
    for line in moldb:
        if line.startswith("#"):
            continue
        else:
            molecules[line.strip().split()[0]] = {}

# Now store the elements and their associated counts in a dictionary for each molecule in the list
for molecule in molecules.keys():
    with open("geometries/{}".format(molecule),"r") as geom:
        for line in geom:
            clean_line = line.strip().split()
            element = clean_line[0]

            #Increment element count if already in the dictionary. Add it otherwise.
            try:
                molecules[molecule][element] += 1
            except KeyError:
                molecules[molecule][element] = 1

# Number of molecules in the test set and their names
nmol = len(molecules)
names = [key for key in molecules.keys()]

#######################################################################################################

def atob():

    # An easy test-case. Let's look at rearrangements.


    # Keep track of products and reactants so that we can write a file showing all reactions. Good for
    # double-checking code and looking for errors (i.e. bicyclo[3.1.0]hexane and spiropentane are not
    # structural isomers, I just copied the spiropentane input structure to bicyclo[3.1.0]hexane and
    # didn't catch the mistake until now).
    products = {}
    reactants = {}
    reactant_coeffs = {}
    product_coeffs = {}
    mad_rxn_energy = {}
    nw_rxn_energy = {}
    rxn_natom = {}

    rxn_num = 1

    # Double loop looks at all unique reactant, product pairs
    for i, reactant in enumerate(molecules):
        for j in range(i+1, nmol):
            # Names is a list of molecule names. The actual molecule is stored in the dictionary 'molecules.'
            product = names[j]

            # First make sure the element types in the reactants agree with the element types in the products.
            if rtools.possible_rxn([molecules[reactant]],[molecules[product]]):

                reactant_coeff, product_coeff = rtools.atob_lowest_integer_coeffs(molecules[reactant],
                  molecules[product],24)

                # -1 indicates appropriate coefficients less than max_coeff weren't found
                if reactant_coeff > 0 and product_coeff > 0:
                     # Add reacant/product pair to dictionary, as well as stoichiometric coefficients
                     products[rxn_num] = product
                     reactants[rxn_num] = reactant
                     reactant_coeffs[rxn_num] = reactant_coeff
                     product_coeffs[rxn_num] = product_coeff

                     rxn_natom[rxn_num] = sum([product_coeff*molecules[product][atom] for atom in 
                       molecules[product].keys()])


                     # Calculate the reaction energy using MADNESS energies 
                     mad_rxn_energy[rxn_num] = ( (product_coeff*mad_energy_dict[product]) - (
                       reactant_coeff*mad_energy_dict[reactant]) )*627.509
                     nw_rxn_energy[rxn_num] = {}

                     for basis_set in basis_sets:
                         nw_rxn_energy[rxn_num][basis_set] = ( (product_coeff*nw_energy_dict[product]
                           [basis_set]) - (reactant_coeff*nw_energy_dict[reactant][basis_set]) )*627.509

                     # Don't forget to increment the reaction number!! To see what a reaction corresponds to
                     # the reaction number can always be checked against the reaction file. (See next lines)
                     rxn_num += 1
                
                else:
                    continue

            else:
                continue

    # Store the reactions in a file by number
    with open("rearrangements.txt","w") as out:
        for basis in basis_sets:
            out.write("Basis: {}\n".format(basis))
            out.write("{:<40s}{:<30s}{:<30s}{:<20s}{:<20s}\n".format("Reaction Number     NATOM","Reactant","Product","E_rxn (MADNESS)",
              "E_rxn (NWChem)"))
            for rxn_num in products.keys():
                out.write("{:<20d}{:<18d}{:<32s}{:<32s}{:<20.12s}{:<20.12s}\n".format(rxn_num,rxn_natom[rxn_num],
                  "{} {}".format(reactant_coeffs[rxn_num],reactants[rxn_num]), 
                  "{} {}".format(product_coeffs[rxn_num],products[rxn_num]),
                  "{}".format(mad_rxn_energy[rxn_num]),
                  "{}".format(nw_rxn_energy[rxn_num][basis])))


#######################################################################################################

def abtoc():

    # An easy test-case. Let's look at rearrangements.


    # Keep track of products and reactants so that we can write a file showing all reactions. Good for
    # double-checking code and looking for errors
    products = {}
    reactants1 = {}
    reactants2 = {}
    reactant1_coeffs = {}
    reactant2_coeffs = {}
    product_coeffs = {}
    mad_rxn_energy = {}
    nw_rxn_energy = {} 
    rxn_natom = {}

    rxn_num = 1

    # Double loop looks at all unique reactant pairs. 3rd loop over all molecules allow to different
    # reactants to form a product that is in fact one of the reactants.
    for i, reactant1 in enumerate(molecules):
        for j in range(i+1, nmol):
            for k in range(nmol):
                # Names is a list of molecule names. The actual molecule is stored in the dictionary 'molecules.'
                reactant2 = names[j]
                product = names[k]

                # First make sure the element types in the reactants agree with the element types in the products.
                if rtools.possible_rxn([molecules[reactant1],molecules[reactant2]],[molecules[product]]):

                    reactant1_coeff, reactant2_coeff, product_coeff = rtools.abtoc_lowest_integer_coeffs(
                      molecules[reactant1], molecules[reactant2], molecules[product], 24)

                    # -1 indicates appropriate coefficients less than max_coeff weren't found
                    if reactant1_coeff > 0 and reactant2_coeff > 0 and product_coeff > 0:
                        # Add reacant/product pair to dictionary, as well as stoichiometric coefficients
                        products[rxn_num] = product
                        reactants1[rxn_num] = reactant1
                        reactants2[rxn_num] = reactant2
                        reactant1_coeffs[rxn_num] = reactant1_coeff
                        reactant2_coeffs[rxn_num] = reactant2_coeff
                        product_coeffs[rxn_num] = product_coeff
 
                        rxn_natom[rxn_num] = sum([product_coeff*molecules[product][atom] for atom in 
                           molecules[product].keys()])

        
                        # Temporary workaround for missing molecules. Need to e-mail Bryan about getting all the
                        # molecules coordinated.

                        try:
                            # Calculate the reaction energy using MADNESS energies 
                            mad_rxn_energy[rxn_num] = ((product_coeff*mad_energy_dict[product]) - (
                              reactant1_coeff*mad_energy_dict[reactant1] + reactant2_coeff*mad_energy_dict[reactant2])
                              )*627.509
                            nw_rxn_energy[rxn_num] = {}

                            for basis_set in basis_sets:
                                nw_rxn_energy[rxn_num][basis_set] = ( (product_coeff*nw_energy_dict[product]
                                  [basis_set]) - (reactant1_coeff*nw_energy_dict[reactant1][basis_set] +  
                                  reactant2_coeff*nw_energy_dict[reactant2][basis_set]) )*627.509

                            # Don't forget to increment the reaction number!! To see what a reaction corresponds to
                            # the reaction number can always be checked against the reaction file. (See next lines)
                            rxn_num += 1

                        except KeyError:
                            print("Potential Missing Molecules: {:>12s} {:>12s} {:>12s}".format(reactant1,reactant2,product))
                
                else:
                    continue

            else:
                continue
    # Store the reactions in a file by number
    with open("abtoc_reactions_lda_unc.txt","w") as out:
        for basis in basis_sets:
            out.write("Basis: {}\n".format(basis))
            out.write("{:<38s}{:<64s}{:<30s}{:<20s}{:<20s}\n".format("Reaction Number     NATOM","Reactant","Product","E_rxn (MADNESS)",
              "E_rxn (NWChem)"))
            for rxn_num in products.keys():
                out.write("{:<20d}{:<18d}{:<32s}{:<32s}{:<32s}{:<20.12s}{:<20.12s}\n".format(rxn_num,
                  rxn_natom[rxn_num],
                  "{} {}".format(reactant1_coeffs[rxn_num],reactants1[rxn_num]), 
                  "{} {}".format(reactant2_coeffs[rxn_num],reactants2[rxn_num]), 
                  "{} {}".format(product_coeffs[rxn_num],products[rxn_num]),
                  "{}".format(mad_rxn_energy[rxn_num]),
                  "{}".format(nw_rxn_energy[rxn_num][basis])))

atob()
#abtoc()
