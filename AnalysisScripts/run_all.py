import os

# Runs all .py files not named 'reaction_tools.py' in current directory
for f in os.listdir("./"):
   if f.endswith(".py") and f.startswith("calc") and f != 'reaction_tools.py' and f != 'run_all.py':
      print("Starting", f)
      os.system("python {}".format(f))
      print("Finished", f, "\n")
