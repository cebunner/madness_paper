import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib import rc
import os

# Matplotlib formating things
rc('text', usetex=True)
basis_sets = ['3-21g','6-31g*','cc-pvdz','cc-pvtz','cc-pvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz']
basis_markers = { '3-21g':'tab:red', '6-31g*':'tab:blue', 'cc-pvdz':'tab:green', 'cc-pvtz':'tab:purple', 
                 'cc-pvqz':'tab:orange', 'aug-cc-pvdz':'tab:cyan', 
                  'aug-cc-pvtz':'tab:brown', 'aug-cc-pvqz':'tab:olive', 'MADNESS':'tab:red',
                  'unc-aug-cc-pvtz':'tab:brown', 'unc-aug-cc-pvqz':'tab:olive'}

basis_styles = { '3-21g':'v', '6-31g*':'v', 'cc-pvdz':'x', 'cc-pvtz':'x', 
                 'cc-pvqz':'x', 'aug-cc-pvdz':'o', 
                  'aug-cc-pvtz':'o', 'aug-cc-pvqz':'o', 'MADNESS':'*',
                  'unc-aug-cc-pvtz':'o', 'unc-aug-cc-pvqz':'o'}

red_triangle = mlines.Line2D([],[],color='tab:red',marker='v',ls='None')
blue_triangle = mlines.Line2D([],[],color='tab:blue',marker='v',ls='None')
green_x = mlines.Line2D([],[],color='tab:green',marker='x',ls='None')
purple_x = mlines.Line2D([],[],color='tab:purple',marker='x',ls='None')
orange_x = mlines.Line2D([],[],color='tab:orange',marker='x',ls='None')
cyan_o = mlines.Line2D([],[],color='tab:cyan',marker='o',ls='None')
brown_o = mlines.Line2D([],[],color='tab:brown',marker='o',ls='None')
olive_o = mlines.Line2D([],[],color='tab:olive',marker='o',ls='None')
brown_s = mlines.Line2D([],[],color='tab:brown',marker='s',ls='None')
olive_s = mlines.Line2D([],[],color='tab:olive',marker='s',ls='None')

default_handles = [red_triangle, blue_triangle,green_x,purple_x,orange_x,cyan_o,brown_o,olive_o,brown_s,olive_s]
default_labels = ['3-21g','6-31g*','cc-pvdz','cc-pvtz','cc-pvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz','unc-aug-cc-pvtz','unc-aug-cc-pvqz']


#Directory with the geometry files
geom_dir = "../SI/geometries"

#Dictionaries for the total energy
nw_tot = {}
mad_tot = {}
atomic_mad = {}

#Dictionary for the atomization energies
nw_atom = {}
mad_atom = {}

# Parser for contracted LDA NW
# Parsing all the .csv files in ../NWChem_Data
for data_file in os.listdir("../NWChem_Data/"):
    if data_file.startswith("lda") and data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nwfile:
            key = data_file[4:-4]
            nw_tot[key] = {}
            for line in nwfile:
               if not line.startswith(","):
                    molname = line.strip().split(",")[0]
                    nw_tot[key][molname] = float(line.strip().split(",")[1])

# Generic MADNESS parser
with open("../MAD_Data/mad_lda_pm_new.csv", "r") as madfile:
    for line in madfile:
        if not line.startswith(","):
            mad_tot[line.strip().split(",")[0]] = float(line.strip().split(",")[1])

# Reads in the number of electrons each molecule possesses
num_electrons = {}
with open("../NWChem_Data/num_electrons.txt","r") as input_file:
    for line in input_file:
        num_electrons[line.split()[0]] = int(line.split()[1])

# Read pre-computed NWChem atomization energies
with open("../NWChem_Data/lda_atomization_energies.txt","r") as input_file:
    for i,line in enumerate([line.strip().replace("\n","") for line in input_file]):
        # Header
        if i == 0:
            continue
        # List of basis sets
        elif i == 1:
            bsets = line.split()
            nw_atom = {bset:{} for bset in bsets}
            # Store basis set column #
            # +1 accounts for molecule name column being empty
            colnums = {bsets[i]:i+1 for i in range(len(bsets))}
        else:
            sl = line.split()
            molname = sl[0]
            for bset in nw_atom.keys():
                nw_atom[bset][molname] = float(sl[colnums[bset]])

# Read in MADNESS atomic energies
with open("../MAD_Data/mad_lda_atomic.txt","r") as input_file:
    for i,line in enumerate([line.strip().replace("\n","") for line in input_file]):
        if i == 0:
            continue
        else:
            atomic_mad[line.strip().split()[0]] = float(line.strip().split()[1]) 


# Calculate MADNESS atomic energies
for i, molecule in enumerate(mad_tot.keys()):
    ae = 0.0
    with open("{}/{}".format(geom_dir,molecule)) as f:
        for line in f:
            ae += atomic_mad[line.strip().split()[0]]
    mad_atom[molecule] = (ae - mad_tot[molecule])*627.50956
#    print("{:<24s} {:<20.12f}".format(molecule,mad_atom[molecule]))

# Want same ylim on each plot in each column
ylims = []

# Plot energy differences
fig, ax = plt.subplots(2,3,sharey=False, sharex=True, figsize=(9,6))

# To get axis right
a = fig.add_subplot(111, frameon=False)
plt.tick_params(labelcolor='none',top=False, bottom=False, left=False, right=False)
plt.grid(False)
plt.xlabel("$\mathrm{Number~of~Electrons}$",size='x-large')
plt.ylabel("$\Delta \mathrm{E}~~ \mathrm{(kcal/mol)}$",size='x-large')

atomization_diff = {}
 
for j, basis in enumerate(['cc-pvdz','cc-pvtz','cc-pvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz']):
        
    electrons = [num_electrons[molecule] for molecule in mad_tot.keys()]

    for i, molecule in enumerate(mad_tot.keys()):
        atomization_diff[molecule] = (mad_atom[molecule] - nw_atom[basis][molecule])
        if atomization_diff[molecule] > 200:
            print(molecule)

    if j <= 2:
        z = 0
    else:
        z = 1
    ax[z,j%3].set_title(basis)
    ax[z,j%3].plot(electrons, np.zeros(len(electrons)),color='black',ls='-')   
    ax[z,j%3].errorbar(electrons, [atomization_diff[molecule] for molecule in atomization_diff.keys()],
                color=basis_markers[basis],label=basis,marker=basis_styles[basis],ls='None')
    
    # Making axes match inside each column
    if j <= 2:
       ylims.append(ax[z,j%3].get_ylim()) 
    if j >2:
       # Check if current min lim is smaller
       if ax[z,j%3].get_ylim()[0] < ylims[j%3][0]:
          # Current is smaller, so save it 
          ylims[j%3] = [ax[z,j%3].get_ylim()[0], ylims[j%3][1]]
       # Check if current max lim is larger
       if ax[z,j%3].get_ylim()[1] > ylims[j%3][1]:
          # Current is smaller, so save it
          ylims[j%3] = [ylims[j%3][0], ax[z,j%3].get_ylim()[1]]

       # And set current ylims to what they should be
       ax[0,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
       ax[1,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
 

#plt.tight_layout()
#plt.savefig("lda_atomization.pdf")
#plt.show() 

