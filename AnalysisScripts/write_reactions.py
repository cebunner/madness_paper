# CB 11/10/17 - A script for calculating reaction energy differences #
# Requirements: 1.) The database file "molecule_database.txt" is in the directory above
#               2.) A directory with the geometry for every molecule is in the same directory
#                   as this script and the files are named as the molecule name in the database file
#                   with no extension
#               3.) All lines that are not commented have an atom identifier and three coordinates (x,
#                   y,z in that order). In particular, any extra lines at the end of the file will
#                   cause this program to crash.

# Important Data Structures:
#
# molecules: The master dictionary that has all 227 molecules in the set

# BS 4/11/18 - Changed script to write out all possible reactions to a file
#              and created another script (calc_reacctions.py) that will 
#              calculate the energies of the different basis sets based off 
#              the data available for that basis set

import numpy as np
import reaction_tools as rtools

#######################################################################################################

# First things first, make a dictionary that has every molecule in our set as a key. What will we store
# as the values in this dictionary, you might ask? Why another dictionary of course!
molecules = {}

# Contains third-row element or lower? First one contains logical for each molecule in the set,
# second is temporary storage for each element in our set.
third_row = {}
el_third_row = {'Al':True,'Ar':True,'B':False,'Be':False,'Br':True,'C':False,'Cl':True,'F':False,
                'H':False,'Li':False,'Mg':True,'Na':True,'N':False,'Ne':False,'O':False,'P':True,
                'S':True,'Si':True}

# Read all molecules from database
with open("../molecule_database.txt","r") as moldb:
    for line in moldb:
        if line.startswith("#"):
            continue
        else:
            molecules[line.strip().split()[0]] = {}

# Read open-shelled molecules from file
with open("openshell.txt","r") as f:
    # First, initialize dictionary with molecule
    open_shell = {key:False for key in molecules.keys()}
    for line in f:
        open_shell[line.strip().split()[0]] = True

# Now store the elements and their associated counts in a dictionary for each molecule in the list
# Since we are already looping over the geometry, this is also a good place to add the logical 
# telling whether or not the molecule has a third-row or lower element
for molecule in molecules.keys():
    with open("geometries/{}".format(molecule),"r") as geom:
        # Contains third row element?
        third_row_logical = False
        for line in geom:
            clean_line = line.strip().split()
            element = clean_line[0]
            if el_third_row[element] == True:
                third_row_logical = True

            #Increment element count if already in the dictionary. Add it otherwise.
            try:
                molecules[molecule][element] += 1
                third_row[molecule] = third_row_logical
            except KeyError:
                molecules[molecule][element] = 1
                third_row[molecule] = third_row_logical

# Number of molecules in the test set and their names
nmol = len(molecules)
names = [key for key in molecules.keys()]

#######################################################################################################

def atob():

    # An easy test-case. Let's look at rearrangements.


    # Keep track of products and reactants so that we can write a file showing all reactions. Good for
    # double-checking code and looking for errors (i.e. bicyclo[3.1.0]hexane and spiropentane are not
    # structural isomers, I just copied the spiropentane input structure to bicyclo[3.1.0]hexane and
    # didn't catch the mistake until now).
    products = {}
    reactants = {}
    reactant_coeffs = {}
    product_coeffs = {}
    mad_rxn_energy = {}
    nw_rxn_energy = {}
    rxn_natom = {}
    rxn_op = {}
    rxn_tr = {}

    rxn_num = 1

    # Double loop looks at all unique reactant, product pairs
    for i, reactant in enumerate(molecules):
        for j in range(i+1, nmol):
            # Names is a list of molecule names. The actual molecule is stored in the dictionary 'molecules.'
            product = names[j]

            # First make sure the element types in the reactants agree with the element types in the products.
            if rtools.possible_rxn([molecules[reactant]],[molecules[product]]):

                reactant_coeff, product_coeff = rtools.atob_lowest_integer_coeffs(molecules[reactant],
                  molecules[product],36)

                # -1 indicates appropriate coefficients less than max_coeff weren't found
                if reactant_coeff > 0 and product_coeff > 0:
                     # Add reacant/product pair to dictionary, as well as stoichiometric coefficients
                     products[rxn_num] = product
                     reactants[rxn_num] = reactant
                     reactant_coeffs[rxn_num] = reactant_coeff
                     product_coeffs[rxn_num] = product_coeff

                     # Involves open shell molecule
                     rxn_op[rxn_num] = False
                     if open_shell[reactant] or open_shell[product]:
                         rxn_op[rxn_num] = True

                     # Involves third row (or lower) element 
                     rxn_tr[rxn_num] = False
                     if third_row[reactant]:
                         rxn_tr[rxn_num] = True

                     rxn_natom[rxn_num] = sum([product_coeff*molecules[product][atom] for atom in 
                       molecules[product].keys()])

                     # Don't forget to increment the reaction number!! To see what a reaction corresponds to
                     # the reaction number can always be checked against the reaction file. (See next lines)
                     rxn_num += 1
                
    # Store the reactions in a file by number
    with open("possible_atob.txt","w") as out:
       out.write("{:<40s}{:<30s}{:<30s}{:<12s}{:<12s}\n".format("Reaction Number     NATOM","Reactant","Product","Open?","Third Row?"))
       for rxn_num in products.keys():
           out.write("{:<20d}{:<18d}{:<32s}{:<32s}{:<12s}{:<12s}\n".format(rxn_num,rxn_natom[rxn_num],
             "{} {}".format(reactant_coeffs[rxn_num],reactants[rxn_num]), 
             "{} {}".format(product_coeffs[rxn_num],products[rxn_num]),
             str(rxn_op[rxn_num]),str(rxn_tr[rxn_num])))


#######################################################################################################

def abtoc():

    # An easy test-case. Let's look at rearrangements.


    # Keep track of products and reactants so that we can write a file showing all reactions. Good for
    # double-checking code and looking for errors
    products = {}
    reactants1 = {}
    reactants2 = {}
    reactant1_coeffs = {}
    reactant2_coeffs = {}
    product_coeffs = {}
    mad_rxn_energy = {}
    nw_rxn_energy = {} 
    rxn_natom = {}
    rxn_op = {}
    rxn_tr = {}

    rxn_num = 1

    # Double loop looks at all unique reactant pairs. 3rd loop over all molecules allow to different
    # reactants to form a product that is in fact one of the reactants.
    for i, reactant1 in enumerate(molecules):
        for j in range(i, nmol):
            for k in range(nmol):
                # Names is a list of molecule names. The actual molecule is stored in the dictionary 'molecules.'
                reactant2 = names[j]
                product = names[k]

                # First make sure the element types in the reactants agree with the element types in the products.
                if rtools.possible_rxn([molecules[reactant1],molecules[reactant2]],[molecules[product]]) and k != i:

                    reactant1_coeff, reactant2_coeff, product_coeff = rtools.abtoc_lowest_integer_coeffs(
                      molecules[reactant1], molecules[reactant2], molecules[product], 36)

                    # -1 indicates appropriate coefficients less than max_coeff weren't found
                    if reactant1_coeff > 0 and reactant2_coeff > 0 and product_coeff > 0:
                        # Add reacant/product pair to dictionary, as well as stoichiometric coefficients
                        products[rxn_num] = product
                        reactants1[rxn_num] = reactant1
                        reactants2[rxn_num] = reactant2
                        reactant1_coeffs[rxn_num] = reactant1_coeff
                        reactant2_coeffs[rxn_num] = reactant2_coeff
                        product_coeffs[rxn_num] = product_coeff
 
                        # Involves open shell molecule
                        rxn_op[rxn_num] = False
                        if open_shell[reactant1] or open_shell[reactant2] or open_shell[product]:
                            rxn_op[rxn_num] = True

                        # Involves third row (or lower) element 
                        rxn_tr[rxn_num] = False
                        if third_row[reactant1] or third_row[reactant2]:
                           rxn_tr[rxn_num] = True

                        rxn_natom[rxn_num] = sum([product_coeff*molecules[product][atom] for atom in 
                           molecules[product].keys()])

        
                        # Don't forget to increment the reaction number!! To see what a reaction corresponds to
                        # the reaction number can always be checked against the reaction file. (See next lines)
                        rxn_num += 1

    # Store the reactions in a file by number
    with open("possible_abtoc.txt","w") as out:
       out.write("{:<38s}{:<64s}{:<30s}{:<12s}{:<12s}\n".format("Reaction Number     NATOM","Reactant","Product","Open?","Third Row?"))
       for rxn_num in products.keys():
           out.write("{:<20d}{:<18d}{:<32s}{:<32s}{:<32s}{:<12s}{:<12s}\n".format(rxn_num, rxn_natom[rxn_num],
             "{} {}".format(reactant1_coeffs[rxn_num],reactants1[rxn_num]), 
             "{} {}".format(reactant2_coeffs[rxn_num],reactants2[rxn_num]), 
             "{} {}".format(product_coeffs[rxn_num],products[rxn_num]),
             str(rxn_op[rxn_num]),str(rxn_tr[rxn_num])))


atob()
abtoc()
