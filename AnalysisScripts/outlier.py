import operator
import sys

fname = "abtoc_reactions_pbe0.txt"

for basis in ["cc-pvdz", "cc-pvtz", "cc-pvqz", "aug-cc-pvdz", "aug-cc-pvtz", "aug-cc-pvqz", "aug-cc-pcvqz"]:
    data = []

    f = open(fname,"r")

    # find the desired basis in the main file
    while True:
        line = f.readline()
        fields = line.split()
        if len(fields) == 2:
            if basis == fields[1]:
                break
    else:
        print(basis,"not found")
        sys.exit(1)

    # process the basis set
    f.readline()
    while True:
        line = f.readline()
        fields = line.split()
        if len(fields) >= 11:
            fields[10] = float(fields[10])
            test = abs(float(fields[8])-float(fields[9]))
            if abs(test-abs(float(fields[10])))>0.01:
                print("bad", fields[0], test, float(fields[10]))
            data.append(fields)
        else:
            break
    f.close()

    data = sorted(data, key=lambda value : value[10])
    print(basis)
    for i in range(1,10):
        print("  ", data[-i])

