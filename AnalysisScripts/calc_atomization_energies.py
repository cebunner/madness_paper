import pandas as pd
import os, sys


geometry_directory = "geometries/"
functionals = ['hf', 'lda', 'b3lyp','pbe0']
basis_sets = ['3-21g','6-31g*','cc-pvdz','cc-pvtz','cc-pvqz',
              'cc-pcvdz','cc-pcvtz','cc-pcvqz',
              'aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz',
              'unc-aug-cc-pvdz','unc-aug-cc-pvtz','unc-aug-cc-pvqz',
              'aug-cc-pcvdz','aug-cc-pcvtz','aug-cc-pcvqz']

atom_energies = {}

# Read in atomic energies
for functional in functionals:
    atom_energies[functional] = {} 

    #Loop through each basis set
    for basis in basis_sets:
        #Create a dictionary for each basis set within the functional dictionary
        atom_energies[functional][basis] = {}
        
    with open("../NWChem_Data/{}_atomic_energies.txt".format(functional),"r") as infile:    
        for i,line in enumerate(infile):
            if i == 0:
                continue
            elif i == 1:
                headers = list(line.split())
            else:
                for j in range(len(line.split())-1):
                    if headers[j] not in basis_sets:
                       continue
                    atom_energies[functional][headers[j]][line.split()[0]] = float(line.split()[j+1])


molecular_energies = {}

# Read in molecular energies
for functional in functionals:

    molecular_energies[functional]= {}

    #Loop through all the output files for each basis set
    for basis in basis_sets:
        #Create a dictionary for each basis set within the functional dictionary
        molecular_energies[functional][basis] = {}
        
        with open("../NWChem_Data/{}_{}.csv".format(functional, basis),"r") as infile:
            for i,line in enumerate(infile):
                if i == 0:
                    continue  
                else:
                    molecule = line.split(',')[0] 
                    molecular_energies[functional][basis][molecule] = float(line.split(',')[1])


atomization_energies = {}

# Calculate atomization energies
for functional in functionals:
    
    atomization_energies[functional] = {}
 
    for basis in basis_sets:

        if functional == "b3lyp" and "unc" in basis:
           #print("Skipping {}/{}".format(functional,basis))
           continue       
 
        atomization_energies[functional][basis] = {}
        
        for molecule in os.listdir('geometries/'):
            
            if molecule not in ["decane", "aluminum", "argon", "beryllium", "boron", "bromine", "carbon", "chlorine", "fluorine", "hydrogen", "lithium", "magnesium", "neon", "nitrogen", "oxygen", "phosphorus", "silicon", "sodium", "sulfur"]:

                geometry = []
            
                with open("{}/{}".format('geometries/',molecule)) as inp:
                    for line in inp:
                        try:
                            geometry.append(line.split()[0])
                        except IndexError:
                            continue
            
                total_atomic_energy = 0.0
            
                for atom in geometry:
                    try:
                        total_atomic_energy += atom_energies[functional][basis][atom]
                    except:
                        print("could not find", functional, basis, atom)
                        raise ValueError
            
                #Multiplicative factor to convert Ha to kcal/mol    
                atomization_energies[functional][basis][molecule] = -627.50956*(molecular_energies[functional][basis][molecule] - total_atomic_energy)

for functional in functionals: 

        outFile = open("../NWChem_Data/{}_atomization_energies.txt".format(functional),"w")

        outFile.write("#Atomization energies (in kcal/mol) calculated by NWChem using the {} functional\n".format(functional))
      
        if functional == "hf" or functional == "lda" or functional == "pbe0":
           bases = ['3-21g','6-31g*','cc-pvdz','cc-pvtz','cc-pvqz','cc-pcvdz','cc-pcvtz','cc-pcvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz','unc-aug-cc-pvdz','unc-aug-cc-pvtz','unc-aug-cc-pvqz','cc-pcvdz','cc-pcvtz','cc-pcvqz','aug-cc-pcvdz','aug-cc-pcvtz','aug-cc-pcvqz']
        else:   
           bases = ['3-21g','6-31g*','cc-pvdz','cc-pvtz','cc-pvqz','cc-pcvdz','cc-pcvtz','cc-pcvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz',
                    'cc-pcvdz','cc-pcvtz','cc-pcvqz','aug-cc-pcvdz','aug-cc-pcvtz','aug-cc-pcvqz']
               
        basis_str = " "*25 
        for basis in bases:
           basis_str += "{:>18s}".format(basis)
        basis_str += "\n"
        outFile.write(basis_str)

        for molecule in os.listdir('geometries/'):
           if molecule not in ["decane", "aluminum", "argon", "beryllium", "boron", "bromine", "carbon", "chlorine", "fluorine", "hydrogen", "lithium", "magnesium", "neon", "nitrogen", "oxygen", "phosphorus", "silicon", "sodium", "sulfur"]:
              molecule_str = "{:<25s}".format(molecule)
              for basis in basis_str.split():
                 molecule_str += "{:>18f}".format(atomization_energies[functional][basis][molecule])
              molecule_str += "\n"
              outFile.write(molecule_str)
        
        outFile.close()  
