#
#   This python script will parse all the files ending with .out in a given directory,
#   assuming they are NWCHEM output files, and will create a .csv of the relevant 
#   relevant information. 
#
#    Information pulled out: 
#       Energy          - total energy
#       Iterations      - number of iterations for completion (not really relevant here but being consistent.)
#       Dipole moment   - dipole moment tensor
#       Eigenvalues     - orbital energies (only pulling out alpha values)
#       Derivatives     - tensor of derivative of energy gradients 
#       Derivative time - time spent calculating derivatives
#       Wall time       - total wall time
#
#
#    To use: python parse_nwchem.py directory_with_output_files/ 
#
#    NOTE: Relative or absolute paths seem ok after minimal testing
#    NOTE: Output will be printed to terminal, so you must pipe it to a file to save it.
#


import os
import sys
import math


# Get directory that houses all output files to parse from command line
mydir = sys.argv[1]

# Print an output header
print("{},{},{},{},{},{},{},{}".format("", "Energy", "Iterations", "Dipole Moment", "Eigens", "Derivatives", "Derivative Time", "Wall Time"))


# For each file in that directory
for file in os.listdir(mydir):

   # Open the file
   inFile = open(mydir + "/" + file, "r")

   # Initializing variables with a value that will be painfully obvious if
   # its not found during parsing
   total_energy = -10000 
   iterations = 0 
   dipole_moment = -10000 
   eigenvalues = []
   derivatives = []
   derivative_time = -1 
   wall_time = -1

   while True:
      line = inFile.readline()
      if not line: break 

      # Check if line contains a total energy
      if line.strip().startswith("Total DFT energy") or line.strip().startswith("Total SCF energy"): 
         total_energy = line.split()[-1]
      # Check if line contains an iteration of DFT 
      elif line.strip().startswith("d="):
         iterations += 1
      # Check if line contains an iteration of HF 
      elif line.strip().startswith("iter"):
         # Skip 2 lines
         for i in range(2): line = inFile.readline()
         # Now read until we get a blank line
         while line.strip():
            iterations += 1
            line = inFile.readline()
      # Check if line contains dipole moment
      elif line.strip().startswith("Multipole"):
         # Need to pull 3 values out to get 
         # the total dipole
         temp = []
         # Need to skip 7 lines
         for i in range(7): line = inFile.readline()
         # Next 3 lines contain what we want
         for i in range(3):
            temp.append(float(line.split()[4]))
            line = inFile.readline()
         # Now calculate the final value, leave in a.u. 
         dipole_moment = math.sqrt(temp[0]*temp[0] + temp[1]*temp[1] + temp[2]*temp[2]) 
      # Check if line contains an eigenvalue, open or closed shell
      # Note: Only reading alpha values from closed shell
      # Note: THERE ARE LINES IN BOTH SCF AND DFT CALCULATIONS THAT HAVE THIS LINE,
      #       BUT THIS FLAG SHOULD ONLY EVER BE TRUE IN DFT, AS SCF FILES WILL TRIP
      #       THE NEXT elif BEFORE THIS ONE AND THAT elif WILL READ THROUGH ALL THE
      #       VECTORS ITSELF
      elif line.strip().startswith("Vector"):
         # Only store data if the vector number is greater than the number of eigenvalues
         # we've already pulled out (since when we jump to the beta orbitals the vector
         # number resets to 1). This way we'll only ever get alpha values 
         if int(line.split()[1]) > len(eigenvalues):
            # Now we only want occupied orbitals
            if float(line.split()[2].replace("Occ","").replace("=","").replace("D","e")) > 0.0:
               # Keep this eigenvalue
               eigenvalues.append(float(line.split()[-1].replace("D", "e").replace("E","").replace("=","")))
      # Check if line contains an eigenvalue from a closed shell calculation
      # Not all vectors get printed, so we need to be smart here
      # SCF ONLY!!!!!
      elif line.strip().startswith("Final eigenvalues") or line.strip().startswith("Final alpha eigenvalues"):
         # Skip 4 lines
         for i in range(4): line = inFile.readline()
         # The next lines are a list of all eigenvalues, just store them all for now
         temp = []
         while line.strip():
            temp.append(float(line.split()[-1]))
            line = inFile.readline()
         # Now we need to read down till the vectors start
         while not (line.strip().startswith("ROHF Final") or line.strip().startswith("UHF Final Alpha")): line = inFile.readline()   
         # Skip 3 (more) lines
         for i in range(3): line = inFile.readline()
         # Now we start reading the vectors, and take all values from temp
         # with an index less than the starting vector index.
         vector_index = int(line.split()[1])
         # Compare the values of temp to this vector_energy
         for i in range(vector_index-1): # -1 because vectors start at 1
            eigenvalues.append(temp[i])
         # Now we want to grab every occupied vector below this
         keep_going = True
         while keep_going:
            if line.strip().startswith("Vector"):
               # Now we only want occupied orbitals
               if float(line.split()[2].replace("Occ","").replace("=","").replace("D","e")) > 0.0:
                  # Keep this eigenvalue
                  eigenvalues.append(float(line.split()[-1].replace("D", "e").replace("E","").replace("=","")))
               # If its not occupied, stop
               else:
                  keep_going = False
            line = inFile.readline()
      # Check if line contains derivative information
      elif line.strip().startswith("DFT ENERGY GRADIENTS"):
         # Need to skip 4 lines
         for i in range(4): line = inFile.readline()
         # Keep reading until we hit a blank line
         while line.strip():
            # Gradient is last three entries of each line
            derivatives.append(float(line.split()[-3]))
            derivatives.append(float(line.split()[-2]))
            derivatives.append(float(line.split()[-1]))
            line = inFile.readline()
      # Check if line contains derivative information
      elif line.strip().startswith("RHF ENERGY GRADIENTS") or line.strip().startswith("UHF ENERGY GRADIENTS"):
         # Need to skip 4 lines
         for i in range(4): line = inFile.readline()
         # Keep reading until we hit a blank line
         while line.strip():
            # Gradient is last three entries of each line
            derivatives.append(float(line.split()[-3]))
            derivatives.append(float(line.split()[-2]))
            derivatives.append(float(line.split()[-1]))
            line = inFile.readline()
      # Check if line contains derivative timing
      elif line.strip().startswith("|  WALL  |"):
         # Need indexes -2 and -4 added together
         derivative_time = float(line.split()[-2]) + float(line.split()[-4])
      # Check if line contains total times
      elif line.strip().startswith("Total times  cpu:"):
         # We want index -1, ignoring the "s" on the end of the time
         wall_time = float(line.split()[-1][:-1])

   # Always clean up
   inFile.close()

   # Output all that information
   print("{},{},{},{},\"{}\",\"{}\",{},{}".format(file[:-4], total_energy, iterations, dipole_moment, eigenvalues, derivatives, derivative_time, wall_time))
