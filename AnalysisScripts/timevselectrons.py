import matplotlib.pyplot as plt
import numpy as np

num_elec = {}
augtz = {}
augqz = {}
mad = {}

# Get NWChem Timings for aug-cc-pvqz and aug-cc-pvtz
with open("../NWChem_Data/svwn5_dftenergy_times.txt",'r') as timing_file:
    for i, line in enumerate(timing_file):

        if i > 1:
            augtz[line.strip().split()[0]] = float(line.strip().split()[5])
            augqz[line.strip().split()[0]] = float(line.strip().split()[4])
        else:
            continue

# Get MADNESS timings
with open("../MAD_Data/DATA/LDA_BOYS_dftenergy_times.txt",'r') as mad_timings:
    for i, line in enumerate(mad_timings):
        
        if i > 0:
            mad[line.strip().split()[0]] = float(line.strip().split()[1])

# Get the number of electrons in each molecule
with open("../NWChem_Data/num_electrons_nodec.txt",'r') as electrons:
    for i, line in enumerate(electrons):

        if i > 0:
            num_elec[line.strip().split()[0]] = int(line.strip().split()[1])

identify_bigguns = [key for key in mad.keys() if num_elec[key] > 100]

for i in identify_bigguns:

    print(i,mad[i])

plt.errorbar([num_elec[key] for key in mad.keys()],[mad[key] for key in mad.keys()],
  ls='None',mfc='white',mec='green',marker='^')
plt.xlabel("Number of Electrons")
plt.ylabel("Computer Time (s)")
plt.title("Computer Time vs Number of Electrons for MADNESS Calculations")

#plt.errorbar([num_elec[key] for key in num_elec.keys()],[augtz[key] for key in num_elec.keys()],
#  ls='None',mfc='white',mec='green',marker='^')
#plt.errorbar([num_elec[key] for key in num_elec.keys()],[augqz[key] for key in num_elec.keys()],
#  ls='None',mfc='white',mec='goldenrod',marker='o')

plt.show()

