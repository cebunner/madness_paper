# CB 11/10/17 - A script for calculating reaction energy differences #
# Requirements: 1.) The database file "molecule_database.txt" is in the directory above
#               2.) A directory with the geometry for every molecule is in the same directory
#                   as this script and the files are named as the molecule name in the database file
#                   with no extension
#               3.) All lines that are not commented have an atom identifier and three coordinates (x,
#                   y,z in that order). In particular, any extra lines at the end of the file will
#                   cause this program to crash.

# Important Data Structures:
#
# molecules: The master dictionary that has all 227 molecules in the set

# BS 4/11/18 - Changed script to calculate energies of all reactions in a file that the
#              basis set has complete information for and created another script 
#              (write_reacctions.py) that will write out all possible reactions


import numpy as np
import re
import reaction_tools as rtools
import os

basis_sets = ['6-31g*','cc-pvdz','cc-pvtz','cc-pvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz', 
              'unc-aug-cc-pvdz', 'unc-aug-cc-pvtz', 'unc-aug-cc-pvqz',
              'cc-pcvdz', 'cc-pcvtz', 'cc-pcvqz','aug-cc-pcvdz','aug-cc-pcvtz','aug-cc-pcvqz']

nw_energy_dict = {}
mad_energy_dict = {}

atob_diff = {}

# Parser for PBE0 NW
# Parsing all the .csv files in ../NWChem_Data
for data_file in os.listdir("../NWChem_Data/"):
    if data_file.startswith("pbe0") and data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nw:
            key = data_file[5:-4]
            for line in nw:
               if not line.startswith(","):
                    molname = line.strip().split(",")[0]
                    if molname not in nw_energy_dict.keys():
                        nw_energy_dict[molname] = {}
                    nw_energy_dict[molname][key] = float(line.strip().split(",")[1])

# Generic MADNESS parser
with open("../MAD_Data/mad_pbe0_pm_new.csv","r") as mad:
    for line in mad:
        if not line.startswith(","):
            mad_energy_dict[line.strip().split(",")[0]] = float(line.strip().split(",")[1])

#######################################################################################################


def atob(output = "atob.txt"):

    # An easy test-case. Let's look at rearrangements.


    # Keep track of products and reactants so that we can write a file showing all reactions. Good for
    # double-checking code and looking for errors
    products = {}
    reactants = {}
    reactant_coeffs = {}
    product_coeffs = {}
    mad_rxn_energy = {}
    nw_rxn_energy = {}
    rxn_natom = {}

    with open("possible_atob.txt", "r") as inFile:
       for line in inFile:
          if line.startswith("Reaction"):
             # Do nothing
             continue
          else: 
             line = line.split()

             # Parse the line to match values needed from "reactions.py" (the file I copied this from)
             rxn_num = int(line[0])
             num_atoms = int(line[1])
             rea_coeff = int(line[2])
             reactant = line[3]
             pro_coeff = int(line[4])
             product = line[5]

             # Check if we have these molecules 
             if (reactant in nw_energy_dict.keys() and product in nw_energy_dict.keys() and 
                 reactant in mad_energy_dict.keys() and product in mad_energy_dict.keys()):

                # Add reacant/product pair to dictionary, as well as stoichiometric coefficients
                products[rxn_num] = product
                reactants[rxn_num] = reactant
                reactant_coeffs[rxn_num] = rea_coeff
                product_coeffs[rxn_num] = pro_coeff
                rxn_natom[rxn_num] = num_atoms 

                # Calculate the reaction energy using MADNESS energies 
                mad_rxn_energy[rxn_num] = ( (pro_coeff*mad_energy_dict[product]) - (
                  rea_coeff*mad_energy_dict[reactant]) )*627.509
                
                # Calculate the reaction energy using NWChem energies
                # Make sure to not overwrite old basis info
                nw_rxn_energy[rxn_num] = {}             
                for basis_set in basis_sets:
                   try:
                      nw_rxn_energy[rxn_num][basis_set] = ( (pro_coeff*nw_energy_dict[product]
                      [basis_set]) - (rea_coeff*nw_energy_dict[reactant][basis_set]) )*627.509
                   except KeyError:
                      print("Missing reaction in", basis_set, "\n   {} {} --> {} {}".format(rea_coeff, reactant, pro_coeff, product))

             # Tell user we're missing something
             else:
                   print("Potentially missing a reaction in", basis_set, "\n   {} {} --> {} {}".format(rea_coeff, reactant, pro_coeff, product))
               
    # Store the reactions in a file by number
    with open(output,"w") as out:
        for basis in basis_sets:
            out.write("Basis: {}\n".format(basis))
            out.write("{:<38s}{:<32s}{:<32s}{:<20s}{:<20s}{:<20s}{:<20s}\n".format("Reaction Number     NATOM","Reactant","Product","E_rxn (MADNESS)",
              "E_rxn (NWChem)","E_rxn (Diff.)","E_rxn (Scaled)"))
            for rxn_num in products.keys():
                out.write("{:<20d}{:<18d}{:<32s}{:<32s}{:<20.12s}{:<20.12s}{:<20.12s}{:<20.12s}\n".format(rxn_num,rxn_natom[rxn_num],
                  "{} {}".format(reactant_coeffs[rxn_num],reactants[rxn_num]), 
                  "{} {}".format(product_coeffs[rxn_num],products[rxn_num]),
                  "{:12.8f}".format(mad_rxn_energy[rxn_num]),
                  "{:12.8f}".format(nw_rxn_energy[rxn_num][basis]),
                  "{:12.8f}".format(abs(mad_rxn_energy[rxn_num]-nw_rxn_energy[rxn_num][basis])),
                  "{:12.8f}".format(abs(mad_rxn_energy[rxn_num]-nw_rxn_energy[rxn_num][basis])/rxn_natom[rxn_num])))


#######################################################################################################

def abtoc(output = "abtoc_reactions.txt"):

    # An easy test-case. Let's look at rearrangements.


    # Keep track of products and reactants so that we can write a file showing all reactions. Good for
    # double-checking code and looking for errors
    products = {}
    reactants1 = {}
    reactants2 = {}
    reactant1_coeffs = {}
    reactant2_coeffs = {}
    product_coeffs = {}
    mad_rxn_energy = {}
    nw_rxn_energy = {} 
    rxn_natom = {}

    with open("possible_abtoc.txt", "r") as inFile:
       for line in inFile:
          if line.startswith("Reaction"):
             # Do nothing
             continue
          else:
             line = line.split()

             # Parse the line to match values needed from "Reactions.py" (the file I copied this from)
             rxn_num = int(line[0])
             num_atoms = int(line[1])
             rea_coeff1 = int(line[2])
             reactant1 = line[3]
             rea_coeff2 = int(line[4])
             reactant2 = line[5]
             pro_coeff = int(line[6])
             product = line[7]

             # Check if we have these molecules 
             if (reactant1 in nw_energy_dict.keys() and reactant2 in nw_energy_dict.keys() and product in nw_energy_dict.keys() and 
                 reactant1 in mad_energy_dict.keys() and reactant2 in mad_energy_dict.keys() and product in mad_energy_dict.keys()):

                # Add reacant/product pair to dictionary, as well as stoichiometric coefficients
                products[rxn_num] = product
                reactants1[rxn_num] = reactant1
                reactant1_coeffs[rxn_num] = rea_coeff1
                reactants2[rxn_num] = reactant2
                reactant2_coeffs[rxn_num] = rea_coeff2
                product_coeffs[rxn_num] = pro_coeff
                rxn_natom[rxn_num] = num_atoms 

                # Calculate the reaction energy using MADNESS energies 
                mad_rxn_energy[rxn_num] = ( (pro_coeff*mad_energy_dict[product]) - (
                  rea_coeff1*mad_energy_dict[reactant1] + rea_coeff2*mad_energy_dict[reactant2]) )*627.509
                
                # Calculate the reaction energy using NWChem energies
                # Make sure to not overwrite old basis info
                nw_rxn_energy[rxn_num] = {}             
                for basis_set in basis_sets:
                   try:
                      nw_rxn_energy[rxn_num][basis_set] = ( (pro_coeff*nw_energy_dict[product]
                      [basis_set]) - (rea_coeff1*nw_energy_dict[reactant1][basis_set] + 
                                      rea_coeff2*nw_energy_dict[reactant2][basis_set]) )*627.509
                   except KeyError:
                      print("Missing reaction in", basis_set, "\n   {} {} + {} {} --> {} {}".format(rea_coeff1, reactant1, rea_coeff2, reactant2, pro_coeff, product))

             # Tell user we're missing something
             else:
                   print("Potentially missing a reaction in", basis_set, "\n   {} {} + {} {} --> {} {}".format(rea_coeff1, reactant1, rea_coeff2, reactant2, pro_coeff, product))
 

    # Store the reactions in a file by number
    with open(output,"w") as out:
        for basis in basis_sets:
            out.write("Basis: {}\n".format(basis))
            out.write("{:<38s}{:<32s}{:<32s}{:<32s}{:<20s}{:<20s}{:<20s}{:<20s}\n".format("Reaction Number     NATOM","Reactant1","Reactant2","Product","E_rxn (MADNESS)",
              "E_rxn (NWChem)","E_rxn (Diff.)","E_rxn (Scaled)"))
            for rxn_num in products.keys():
                out.write("{:<20d}{:<18d}{:<32s}{:<32s}{:<32s}{:<20.12s}{:<20.12s}{:<20.12s}{:<20.12s}\n".format(rxn_num,rxn_natom[rxn_num],
                  "{} {}".format(reactant1_coeffs[rxn_num],reactants1[rxn_num]), 
                  "{} {}".format(reactant2_coeffs[rxn_num],reactants2[rxn_num]), 
                  "{} {}".format(product_coeffs[rxn_num],products[rxn_num]),
                  "{:12.8f}".format(mad_rxn_energy[rxn_num]),
                  "{:12.8f}".format(nw_rxn_energy[rxn_num][basis]),
                  "{:12.8f}".format(abs(mad_rxn_energy[rxn_num]-nw_rxn_energy[rxn_num][basis])),
                  "{:12.8f}".format(abs(mad_rxn_energy[rxn_num]-nw_rxn_energy[rxn_num][basis])/rxn_natom[rxn_num])))

    # Write out to a different file the reactions in a nice format that includes a small analysis
    with open("pretty_print_" + output, "w") as out:
        for basis in basis_sets:
            out.write("Basis: {}\nEnergy in kcal/mol\n".format(basis))
            for rxn_num in products.keys():
                out.write("{:<12d}{:<32s} + {:<32s} --> {:<32s}   {:<32s}\n".format(rxn_num,
                  "{} {}".format(reactant1_coeffs[rxn_num],reactants1[rxn_num]), 
                  "{} {}".format(reactant2_coeffs[rxn_num],reactants2[rxn_num]), 
                  "{} {}".format(product_coeffs[rxn_num],products[rxn_num]),
                  "Reaction Energy"))
                out.write("{:<12s}{:<32.5f}   {:<32.5f}     {:<32.5f}   {:<32.5f}\n".format("MADNESS", 
                  mad_energy_dict[reactants1[rxn_num]]*627.509*reactant1_coeffs[rxn_num],
                  mad_energy_dict[reactants2[rxn_num]]*627.509*reactant2_coeffs[rxn_num],
                  mad_energy_dict[products[rxn_num]]*627.509*product_coeffs[rxn_num],
                  mad_rxn_energy[rxn_num]))
                out.write("{:<12s}{:<32.5f}   {:<32.5f}     {:<32.5f}   {:<32.5f}\n".format("NWChem", 
                  nw_energy_dict[reactants1[rxn_num]][basis]*627.509*reactant1_coeffs[rxn_num],
                  nw_energy_dict[reactants2[rxn_num]][basis]*627.509*reactant2_coeffs[rxn_num],
                  nw_energy_dict[products[rxn_num]][basis]*627.509*product_coeffs[rxn_num],
                  nw_rxn_energy[rxn_num][basis])) 
                out.write("{:<12s}{:<32.5f}   {:<32.5f}     {:<32.5f}   {:<32.5f}\n\n".format("Diff. (M-N)", 
                      mad_energy_dict[reactants1[rxn_num]]*627.509*reactant1_coeffs[rxn_num] - nw_energy_dict[reactants1[rxn_num]][basis]*627.509*reactant1_coeffs[rxn_num],
                      mad_energy_dict[reactants2[rxn_num]]*627.509*reactant2_coeffs[rxn_num] - nw_energy_dict[reactants2[rxn_num]][basis]*627.509*reactant2_coeffs[rxn_num],
                      mad_energy_dict[products[rxn_num]]*627.509*product_coeffs[rxn_num] - nw_energy_dict[products[rxn_num]][basis]*627.509*product_coeffs[rxn_num],
                      mad_rxn_energy[rxn_num] - nw_rxn_energy[rxn_num][basis]))

    # Write out to a different file the biggest reactions by energy in a latex table format  
    # First want the chemical formula instead of words, so need to read in formulas
    name_to_symbol = {}
    
    # Make the regular expression
    numbers = re.compile(r'(\d+)')
    with open("../molecule_database.txt", "r") as molFile:
       for line in molFile:
          if line.startswith("#"): # Header lines start with this
             continue
          else:
             line = line.split()
             name_to_symbol[line[0]] = numbers.sub(r'$_\1$', line[2])

    with open("pbe0_reactions.tex", "w") as out:
        for basis in ['unc-aug-cc-pvqz']:
            out.write("\\begin{table}\n")
            out.write("  \\begin{adjustwidth}{-2cm}{}\n")
            out.write("    \\begin{tabular}{ c | r c c r c c r c c}\n")
            for rxn_num in products.keys():
              if abs(mad_rxn_energy[rxn_num] - nw_rxn_energy[rxn_num][basis]) > 6.2 and basis == "unc-aug-cc-pvqz":
                out.write("{:<12d} & {:<32s} & + & {:<32s} & $\\rightarrow$ & {:<32s} &  {:<32s} \\\\ \n".format(rxn_num,
                  "{} & {}".format(reactant1_coeffs[rxn_num],name_to_symbol[reactants1[rxn_num]]), 
                  "{} & {}".format(reactant2_coeffs[rxn_num],name_to_symbol[reactants2[rxn_num]]), 
                  "{} & {}".format(product_coeffs[rxn_num],name_to_symbol[products[rxn_num]]),
                  "Reaction Energy"))
                out.write("\hline\n")
                out.write("{:<12s} & & {:<32.5f} & & & {:<32.5f}  & & & {:<32.5f} &  {:<32.5f} \\\\ \n".format("MADNESS", 
                  mad_energy_dict[reactants1[rxn_num]]*627.509*reactant1_coeffs[rxn_num],
                  mad_energy_dict[reactants2[rxn_num]]*627.509*reactant2_coeffs[rxn_num],
                  mad_energy_dict[products[rxn_num]]*627.509*product_coeffs[rxn_num],
                  mad_rxn_energy[rxn_num]))
                out.write("{:<12s} & & {:<32.5f} & & & {:<32.5f}  & & & {:<32.5f} &  {:<32.5f} \\\\ \n".format("NWChem", 
                  nw_energy_dict[reactants1[rxn_num]][basis]*627.509*reactant1_coeffs[rxn_num],
                  nw_energy_dict[reactants2[rxn_num]][basis]*627.509*reactant2_coeffs[rxn_num],
                  nw_energy_dict[products[rxn_num]][basis]*627.509*product_coeffs[rxn_num],
                  nw_rxn_energy[rxn_num][basis])) 
                out.write(" \\bigskip {:<12s} & & {:<32.5f} & & & {:<32.5f}  & & &  {:<32.5f} &  {:<32.5f} \\\\ \n".format("Diff. (M-N)", 
                      mad_energy_dict[reactants1[rxn_num]]*627.509 - nw_energy_dict[reactants1[rxn_num]][basis]*627.509,
                      mad_energy_dict[reactants2[rxn_num]]*627.509 - nw_energy_dict[reactants2[rxn_num]][basis]*627.509,
                      mad_energy_dict[products[rxn_num]]*627.509 - nw_energy_dict[products[rxn_num]][basis]*627.509,
                      mad_rxn_energy[rxn_num] - nw_rxn_energy[rxn_num][basis]))
            out.write("    \\end{tabular}\n")
            out.write("    \\caption{Five largest reaction errors with basis unc-aug-cc-pvqz at the DFT/PBE0 level of theory. Energy is in kcal/mol. \\label{rxn:pbe0}}")
            out.write("  \\end{adjustwidth}\n")
            out.write("\\end{table}")


atob("atob_reactions_pbe0.txt")
abtoc("abtoc_reactions_pbe0.txt")
