import os

# Run all the calculate reactions
os.system("/home/cbunner/ext/anaconda3/bin/python3 calc_hf_reactions.py")
os.system("/home/cbunner/ext/anaconda3/bin/python3 calc_hf_unc_reactions.py")
os.system("/home/cbunner/ext/anaconda3/bin/python3 calc_lda_reactions.py")
os.system("/home/cbunner/ext/anaconda3/bin/python3 calc_lda_unc_reactions.py")

# Plot everything
print("\nHF")
os.system("/home/cbunner/ext/anaconda3/bin/python3 plot_hf_scaled.py")
print("")

print("\nHF unc")
os.system("/home/cbunner/ext/anaconda3/bin/python3 plot_hf_unc_scaled.py")
print("")

print("\nLDA")
os.system("/home/cbunner/ext/anaconda3/bin/python3 plot_lda_scaled.py")
print("")

print("\nLDA unc")
os.system("/home/cbunner/ext/anaconda3/bin/python3 plot_lda_unc_scaled.py")
print("")
