#
#    So I found one instance where the geometries between NWChem and MADNESS did not match.
#    This file will go through the outputs of each set specified and verify the nuclear repulsion 
#    energies match in order to verify that the geometries are the same.
# 
#


import sys
import os

# Get directories of output files from command line
mad_dir = sys.argv[1]
nw_dir = sys.argv[2]



print()
print("MADNESS output directory:", mad_dir)
print("NWChem output directory:", nw_dir)
print()

# Ask OS for list of files in each directory
mad_list = os.listdir(mad_dir)
nw_list = os.listdir(nw_dir)

# List of those not found
not_found = []
different = []

# Run over every output in MADNESS first
for madFile in mad_list:

   # First print
   print("\nVerifying molecule: ", madFile[:-4])

   # Verify a corresponding NWChem file exists
   if madFile not in nw_list:
      print("   {} not found in nwchem directory".format(madFile))
      not_found.append(madFile)

   else:
      # If both files exist, open and find nuclear repulsion energy
      mad = open(mad_dir + "/" + madFile, "r")
      nw = open(nw_dir + "/" + madFile, "r")

      # Need to store the repulsion values
      mad_rep = 0.0
      nw_rep = 0.0

      # Now find nuclear repulsion value in madness file
      for line in mad:
         if(line.strip()):
            #print(line[:-1])
            if(line.split()[0] == "nuclear-repulsion"):
               mad_rep = float(line.split()[1])
               break

      # Then find nuclear repulsion value in nwchem file
      for line in nw:
         if(line.strip()):
            if(len(line.split()) > 3):
               #print(line[:-1])
               if(line.split()[0] == "Effective" and line.split()[1] == "nuclear" and line.split()[2] == "repulsion"):
                  nw_rep = float(line.split()[5])
                  break
      
      # Let user know whats going on
      print("   madness energy:", mad_rep)
      print("   nwchem  energy:", nw_rep) 

      # Check if they are not within a tolerance
      if(abs(mad_rep - nw_rep) > 1e-4):
         # If we get here, bad things happened.
         # Cry, then save the info and move on.
         different.append([madFile, mad_rep, nw_rep])

      # Always clean up
      mad.close()
      nw.close()

# Update user on what hasn't been found
print("\nDone searching through", mad_dir)
if(len(not_found)):
   print("\n\nThese files were not found in", nw_dir)
   for line in not_found:
      print(line)
if(len(different)):
   print("\n\nThese files were found to have different geometries.")
   print("   {:>32s} {:>32s} {:>32s}".format("molecule", "Nuclear Repulsion (madness)", "Nuclear Repulsion (nwchem)"))
   for line in different:
      print(" {:>32s}:{:>32.8f} {:>32.8f}".format(line[0], line[1], line[2]))

# Make sure we handle case of more files in NWChem directory than in MADNESS directory
if len(nw_list) > len(mad_list):
   print("\nThese files were not checked due to no MADNESS file being present:")
   for nw in nw_list:
      if(nw not in mad_list):
         print("   {}".format(nw))

print() 
