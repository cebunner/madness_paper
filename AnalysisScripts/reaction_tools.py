# CB 11/10/17 - Lots of functions that will be used in the reactions.py script. I just thought it would
#               be a lot cleaner to include that stuff here.

# Function to multipy a dictionary containing elemental composition by an integer
def integer_multiple(molecule_dictionary,mult):

    #Input: A dictionary containing atoms as keys and integers corresponding to the atom count
    #       in the molecule as values
    #Output: A dictionary with the same keys, but with integer multiple values
    
    return {key: molecule_dictionary[key]*mult for key in molecule_dictionary.keys()}

def cat(dict1,dict2):

    #Input: A dictionary containing atoms as keys and integers corresponding to the atom count
    #       in the molecule as values
    #Output: A dictionary representing the combined molecules.

    for key in dict2.keys():
        try:
            dict1[key] = dict1[key] + dict2[key]
        except KeyError:
            dict1[key] = dict2[key]

    return dict1

def integer_balance(smaller,larger):

    if int(larger/smaller) == larger/smaller:
        return larger/smaller, 1

    else:
        return larger, smaller

def possible_rxn(reactant_list,product_list):

    #Input: Two lists containing molecule dictionaries (atom key, natom value), first reactant(s)
    # and second product(s)
    #Output: True if the atoms match, and hence a reaction is possible with the right
    #        stoichiometric coefficients. False if the atom lists don't match.
 
    # First build a master dictionary so that we can see whether or not we are dealing with the
    # same set of atoms. We can stop here if, say, the products have Si but not of the reactants
    # do.
    master_reactants = {}
    for i in range(len(reactant_list)):
        master_reactants = cat(master_reactants,reactant_list[i])

    master_products = {}
    for i in range(len(product_list)):
        master_products = cat(master_products,product_list[i])

    if  [key for key in master_reactants.keys()] != [key for key in master_products.keys()]:
        return False

    else:
        return True 

def atob_lowest_integer_coeffs(reactant_dict,product_dict,max_coeff):

    for reactant_coeff in range(1,max_coeff+1):
        for product_coeff in range(1,max_coeff+1):
            # Test stoichiometric multiples
            aug_reactant = integer_multiple(reactant_dict,reactant_coeff)
            aug_product = integer_multiple(product_dict,product_coeff)
 
            if aug_reactant == aug_product:
                # Add reacant/product pair to dictionary, as well as stoichiometric coefficients
                return reactant_coeff, product_coeff

            else:
                continue
    return -1, -1

def abtoc_lowest_integer_coeffs(reactant1_dict,reactant2_dict,product_dict,max_coeff):
    # Triple loop over all possible coefficient combinations
    for reactant1_coeff in range(1,max_coeff+1):
        for reactant2_coeff in range(1,max_coeff+1):
            for product_coeff in range(1,max_coeff+1):

                # Test stoichiometric multiples
                aug_reactant1 = integer_multiple(reactant1_dict,reactant1_coeff)
                aug_reactant2 = integer_multiple(reactant2_dict,reactant2_coeff)
                aug_product = integer_multiple(product_dict,product_coeff)
 
                if cat(aug_reactant1,aug_reactant2) == aug_product:
                    # Add reacant/product pair to dictionary, as well as stoichiometric coefficients
                    return reactant1_coeff, reactant2_coeff, product_coeff

                else:
                    continue

    return -1, -1, -1

