Basis: pcseg-1
Energy in kcal/mol
Basis: pcseg-2
Energy in kcal/mol
Basis: pcseg-3
Energy in kcal/mol
Basis: aug-pcseg-1
Energy in kcal/mol
Basis: aug-pcseg-2
Energy in kcal/mol
24          1 acetic_acid                    + 2 isopropyl_alcohol              --> 4 ethanol_radical                  Reaction Energy                 
MADNESS     -142705.31061                      -241946.47957                        -384388.41658                      263.37361                       
NWChem      -142692.02968                      -241923.42459                        -384342.96416                      272.49011                       
Diff. (M-N) -13.28093                          -11.52749                            -11.36310                          -9.11650                        

28          1 acetic_acid                    + 2 methanol                       --> 4 methanol_radical                 Reaction Energy                 
MADNESS     -142705.31061                      -144139.00568                        -286588.88343                      255.43286                       
NWChem      -142692.02968                      -144125.42118                        -286548.59333                      268.85754                       
Diff. (M-N) -13.28093                          -6.79225                             -10.07253                          -13.42468                       

29          1 acetic_acid                    + 2 methoxyethane                  --> 4 ethanol_radical                  Reaction Energy                 
MADNESS     -142705.31061                      -241920.56277                        -384388.41658                      237.45681                       
NWChem      -142692.02968                      -241897.45678                        -384342.96416                      246.52230                       
Diff. (M-N) -13.28093                          -11.55299                            -11.36310                          -9.06549                        

1343        1 carbon_monoxide                + 3 methanol                       --> 4 methanol_radical                 Reaction Energy                 
MADNESS     -70581.06988                       -216208.50852                        -286588.88343                      200.69497                       
NWChem      -70574.04138                       -216188.13177                        -286548.59333                      213.57982                       
Diff. (M-N) -7.02850                           -6.79225                             -10.07253                          -12.88485                       

1345        2 carbon_monoxide                + 4 methanol_radical               --> 3 acetic_acid                      Reaction Energy                 
MADNESS     -141162.13977                      -286588.88343                        -428115.93184                      -364.90864                      
NWChem      -141148.08276                      -286548.59333                        -428076.08905                      -379.41297                      
Diff. (M-N) -7.02850                           -10.07253                            -13.28093                          14.50433                        

1346        2 carbon_monoxide                + 4 methanol_radical               --> 3 methyl_formate                   Reaction Energy                 
MADNESS     -141162.13977                      -286588.88343                        -428061.66281                      -310.63961                      
NWChem      -141148.08276                      -286548.59333                        -428021.61935                      -324.94326                      
Diff. (M-N) -7.02850                           -10.07253                            -13.34782                          14.30366                        

2086        1 butane                         + 18 vinyl_radical                 --> 8 spiropentane                     Reaction Energy                 
MADNESS     -98517.94718                       -871813.76994                        -971537.20720                      -1205.49008                     
NWChem      -98508.21026                       -871724.49915                        -971445.86976                      -1213.16036                     
Diff. (M-N) -9.73693                           -4.95949                             -11.41718                          7.67029                         

3174        3 ethanol                        + 1 ketene                         --> 4 ethanol_radical                  Reaction Energy                 
MADNESS     -289559.74282                      -95023.32578                         -384388.41658                      194.65203                       
NWChem      -289532.14745                      -95014.29662                         -384342.96416                      203.47991                       
Diff. (M-N) -9.19846                           -9.02916                             -11.36310                          -8.82788                        

3176        2 ethanol                        + 1 oxygen_diatomic                --> 4 methanol_radical                 Reaction Energy                 
MADNESS     -193039.82855                      -93713.18374                         -286588.88343                      164.12886                       
NWChem      -193021.43163                      -93704.43383                         -286548.59333                      177.27213                       
Diff. (M-N) -9.19846                           -8.74991                             -10.07253                          -13.14328                       

3178        3 ethanol                        + 1 ozone                          --> 6 methanol_radical                 Reaction Energy                 
MADNESS     -289559.74282                      -140549.05061                        -429883.32515                      225.46828                       
NWChem      -289532.14745                      -140535.90567                        -429822.88999                      245.16314                       
Diff. (M-N) -9.19846                           -13.14494                            -10.07253                          -19.69485                       

3184        2 ethanol_radical                + 1 hydrogen_peroxide              --> 4 methanol_radical                 Reaction Energy                 
MADNESS     -192194.20829                      -94472.18217                         -286588.88343                      77.50703                        
NWChem      -192171.48208                      -94463.47263                         -286548.59333                      86.36139                        
Diff. (M-N) -11.36310                          -8.70954                             -10.07253                          -8.85436                        

3580        4 hydroxymethyl_radical          + 1 isobutylene                    --> 4 ethanol_radical                  Reaction Energy                 
MADNESS     -286641.97494                      -97763.99452                         -384388.41658                      17.55288                        
NWChem      -286614.96236                      -97754.41359                         -384342.96416                      26.41179                        
Diff. (M-N) -6.75314                           -9.58092                             -11.36310                          -8.85891                        

3587        5 hydroxymethyl_radical          + 1 cyclopentane                   --> 5 ethanol_radical                  Reaction Energy                 
MADNESS     -358302.46867                      -122227.45877                        -480485.52072                      44.40672                        
NWChem      -358268.70295                      -122215.54437                        -480428.70520                      55.54211                        
Diff. (M-N) -6.75314                           -11.91440                            -11.36310                          -11.13539                       

3640        3 hexane                         + 22 methlidyne_radical            --> 8 spiropentane                     Reaction Energy                 
MADNESS     -442239.36077                      -525927.95136                        -971537.20720                      -3369.89507                     
NWChem      -442195.95225                      -525871.12579                        -971445.86976                      -3378.79173                     
Diff. (M-N) -14.46951                          -2.58298                             -11.41718                          8.89666                         

3739        1 hexane                         + 22 vinyl_radical                 --> 10 spiropentane                    Reaction Energy                 
MADNESS     -147413.12026                      -1065550.16326                       -1214421.50900                     -1458.22548                     
NWChem      -147398.65075                      -1065441.05451                       -1214307.33721                     -1467.63195                     
Diff. (M-N) -14.46951                          -4.95949                             -11.41718                          9.40646                         

3787        4 heptane                        + 26 methlidyne_radical            --> 9 bicyclo[3.1.0]hexane             Reaction Energy                 
MADNESS     -687443.64317                      -621551.21525                        -1313292.48169                     -4297.62327                     
NWChem      -687377.01176                      -621484.05775                        -1313167.72414                     -4306.65463                     
Diff. (M-N) -16.65785                          -2.58298                             -13.86195                          9.03136                         

3881        2 heptane                        + 26 vinyl_radical                 --> 11 bicyclo[3.1.0]hexane            Reaction Energy                 
MADNESS     -343721.82159                      -1259286.55658                       -1605135.25539                     -2126.87723                     
NWChem      -343688.50588                      -1259157.60988                       -1604982.77395                     -2136.65819                     
Diff. (M-N) -16.65785                          -4.95949                             -13.86195                          9.78097                         

3882        1 heptane                        + 24 vinyl_radical                 --> 11 spiropentane                    Reaction Energy                 
MADNESS     -171860.91079                      -1162418.35992                       -1335863.65990                     -1584.38918                     
NWChem      -171844.25294                      -1162299.33219                       -1335738.07093                     -1594.48579                     
Diff. (M-N) -16.65785                          -4.95949                             -11.41718                          10.09661                        

4054        1 isobutane                      + 18 vinyl_radical                 --> 8 spiropentane                     Reaction Energy                 
MADNESS     -98519.65831                       -871813.76994                        -971537.20720                      -1203.77895                     
NWChem      -98509.90873                       -871724.49915                        -971445.86976                      -1211.46189                     
Diff. (M-N) -9.74958                           -4.95949                             -11.41718                          7.68295                         

4320        3 magnesium_diatomic             + 2 ozone                          --> 6 magnesium_oxide                  Reaction Energy                 
MADNESS     -749782.34170                      -281098.10122                        -1030905.30107                     -24.85815                       
NWChem      -749764.65128                      -281071.81135                        -1030846.57547                     -10.11283                       
Diff. (M-N) -5.89681                           -13.14494                            -9.78760                           -14.74531                       

4600        26 methlidyne_radical            + 2 nonane                         --> 11 bicyclo[1.1.0]butane            Reaction Energy                 
MADNESS     -621551.21525                      -441512.53012                        -1066872.41788                     -3808.67251                     
NWChem      -621484.05775                      -441469.67028                        -1066770.93971                     -3817.21168                     
Diff. (M-N) -2.58298                           -21.42992                            -9.22529                           8.53917                         

4608        26 methlidyne_radical            + 2 nonane                         --> 11 methylenecyclopropane           Reaction Energy                 
MADNESS     -621551.21525                      -441512.53012                        -1066910.62666                     -3846.88129                     
NWChem      -621484.05775                      -441469.67028                        -1066808.93363                     -3855.20560                     
Diff. (M-N) -2.58298                           -21.42992                            -9.24482                           8.32431                         

4613        30 methlidyne_radical            + 4 nonane                         --> 11 bicyclo[3.1.0]hexane            Reaction Energy                 
MADNESS     -717174.47913                      -883025.06024                        -1605135.25539                     -4935.71602                     
NWChem      -717096.98971                      -882939.34055                        -1604982.77395                     -4946.44369                     
Diff. (M-N) -2.58298                           -21.42992                            -13.86195                          10.72766                        

4614        28 methlidyne_radical            + 3 nonane                         --> 11 spiropentane                    Reaction Energy                 
MADNESS     -669362.84719                      -662268.79518                        -1335863.65990                     -4232.01753                     
NWChem      -669290.52373                      -662204.50541                        -1335738.07093                     -4243.04178                     
Diff. (M-N) -2.58298                           -21.42992                            -11.41718                          11.02425                        

4637        26 methlidyne_radical            + 3 octane                         --> 10 spiropentane                    Reaction Energy                 
MADNESS     -621551.21525                      -588924.35350                        -1214421.50900                     -3945.94025                     
NWChem      -621484.05775                      -588867.07439                        -1214307.33721                     -3956.20507                     
Diff. (M-N) -2.58298                           -19.09304                            -11.41718                          10.26482                        

4659        22 methlidyne_radical            + 4 pentane                        --> 7 bicyclo[3.1.0]hexane             Reaction Energy                 
MADNESS     -525927.95136                      -491861.61993                        -1021449.70798                     -3660.13669                     
NWChem      -525871.12579                      -491813.10460                        -1021352.67433                     -3668.44394                     
Diff. (M-N) -2.58298                           -12.12883                            -13.86195                          8.30726                         

4660        20 methlidyne_radical            + 3 pentane                        --> 7 spiropentane                     Reaction Energy                 
MADNESS     -478116.31942                      -368896.21495                        -850095.05630                      -3082.52193                     
NWChem      -478064.65981                      -368859.82845                        -850015.13604                      -3090.64778                     
Diff. (M-N) -2.58298                           -12.12883                            -11.41718                          8.12585                         

4783        2 methanol                       + 1 methyl_formate                 --> 4 methanol_radical                 Reaction Energy                 
MADNESS     -144139.00568                      -142687.22094                        -286588.88343                      237.34318                       
NWChem      -144125.42118                      -142673.87312                        -286548.59333                      250.70097                       
Diff. (M-N) -6.79225                           -13.34782                            -10.07253                          -13.35779                       

4790        3 methanol                       + 1 acetate_anion                  --> 5 methanol_radical                 Reaction Energy                 
MADNESS     -216208.50852                      -142358.16694                        -358236.10429                      330.57116                       
NWChem      -216188.13177                      -142345.02732                        -358185.74166                      347.41743                       
Diff. (M-N) -6.79225                           -13.13962                            -10.07253                          -16.84627                       

5155        2 nonane                         + 30 vinyl_radical                 --> 13 bicyclo[3.1.0]hexane            Reaction Energy                 
MADNESS     -441512.53012                      -1453022.94990                       -1896978.02910                     -2442.54908                     
NWChem      -441469.67028                      -1452874.16524                       -1896797.82376                     -2453.98824                     
Diff. (M-N) -21.42992                          -4.95949                             -13.86195                          11.43916                        

5156        1 nonane                         + 28 vinyl_radical                 --> 13 spiropentane                    Reaction Energy                 
MADNESS     -220756.26506                      -1356154.75324                       -1578747.96170                     -1836.94340                     
NWChem      -220734.83514                      -1356015.88756                       -1578599.53837                     -1848.81567                     
Diff. (M-N) -21.42992                          -4.95949                             -11.41718                          11.87227                        

5189        4 ozone                          + 3 phosphorus_tetramer            --> 12 phosphorus_monoxide             Reaction Energy                 
MADNESS     -562196.20244                      -2561377.28227                       -3123686.01110                     -112.52639                      
NWChem      -562143.62270                      -2561326.76645                       -3123572.87699                     -102.48785                      
Diff. (M-N) -13.14494                          -16.83860                            -9.42784                           -10.03855                       

5272        1 octane                         + 26 vinyl_radical                 --> 12 spiropentane                    Reaction Energy                 
MADNESS     -196308.11783                      -1259286.55658                       -1457305.81080                     -1711.13638                     
NWChem      -196289.02480                      -1259157.60988                       -1457168.80465                     -1722.16998                     
Diff. (M-N) -19.09304                          -4.95949                             -11.41718                          11.03359                        

5366        2 pentane                        + 22 vinyl_radical                 --> 9 bicyclo[3.1.0]hexane             Reaction Energy                 
MADNESS     -245930.80996                      -1065550.16326                       -1313292.48169                     -1811.50846                     
NWChem      -245906.55230                      -1065441.05451                       -1313167.72414                     -1820.11733                     
Diff. (M-N) -12.12883                          -4.95949                             -13.86195                          8.60887                         

5367        1 pentane                        + 20 vinyl_radical                 --> 9 spiropentane                     Reaction Energy                 
MADNESS     -122965.40498                      -968681.96660                        -1092979.35810                     -1331.98652                     
NWChem      -122953.27615                      -968582.77683                        -1092876.60349                     -1340.55051                     
Diff. (M-N) -12.12883                          -4.95949                             -11.41718                          8.56399                         

5694        5 sulfur_diatomic                + 2 sulfur_hexafluoride            --> 12 monosulfur_monofluoride         Reaction Energy                 
MADNESS     -2490287.17311                     -1245573.54658                       -3735299.89337                     560.82632                       
NWChem      -2490239.96593                     -1245484.75250                       -3735174.03115                     550.68728                       
Diff. (M-N) -9.44144                           -44.39704                            -10.48852                          10.13903                        

Basis: unc-aug-pcseg-2
Energy in kcal/mol
28          1 acetic_acid                    + 2 methanol                       --> 4 methanol_radical                 Reaction Energy                 
MADNESS     -142705.31061                      -144139.00568                        -286588.88343                      255.43286                       
NWChem      -142698.39955                      -144131.87706                        -286561.69682                      268.57980                       
Diff. (M-N) -6.91106                           -3.56431                             -6.79665                           -13.14694                       

1343        1 carbon_monoxide                + 3 methanol                       --> 4 methanol_radical                 Reaction Energy                 
MADNESS     -70581.06988                       -216208.50852                        -286588.88343                      200.69497                       
NWChem      -70577.57066                       -216197.81559                        -286561.69682                      213.68943                       
Diff. (M-N) -3.49923                           -3.56431                             -6.79665                           -12.99446                       

1345        2 carbon_monoxide                + 4 methanol_radical               --> 3 acetic_acid                      Reaction Energy                 
MADNESS     -141162.13977                      -286588.88343                        -428115.93184                      -364.90864                      
NWChem      -141155.14131                      -286561.69682                        -428095.19865                      -378.36052                      
Diff. (M-N) -3.49923                           -6.79665                             -6.91106                           13.45188                        

1346        2 carbon_monoxide                + 4 methanol_radical               --> 3 methyl_formate                   Reaction Energy                 
MADNESS     -141162.13977                      -286588.88343                        -428061.66281                      -310.63961                      
NWChem      -141155.14131                      -286561.69682                        -428040.75008                      -323.91196                      
Diff. (M-N) -3.49923                           -6.79665                             -6.97091                           13.27235                        

3176        2 ethanol                        + 1 oxygen_diatomic                --> 4 methanol_radical                 Reaction Energy                 
MADNESS     -193039.82855                      -93713.18374                         -286588.88343                      164.12886                       
NWChem      -193030.41336                      -93708.36282                         -286561.69682                      177.07937                       
Diff. (M-N) -4.70759                           -4.82092                             -6.79665                           -12.95051                       

3178        3 ethanol                        + 1 ozone                          --> 6 methanol_radical                 Reaction Energy                 
MADNESS     -289559.74282                      -140549.05061                        -429883.32515                      225.46828                       
NWChem      -289545.62004                      -140541.88406                        -429842.54522                      244.95888                       
Diff. (M-N) -4.70759                           -7.16655                             -6.79665                           -19.49060                       

3184        2 ethanol_radical                + 1 hydrogen_peroxide              --> 4 methanol_radical                 Reaction Energy                 
MADNESS     -192194.20829                      -94472.18217                         -286588.88343                      77.50703                        
NWChem      -192184.96005                      -94467.48948                         -286561.69682                      90.75271                        
Diff. (M-N) -4.62412                           -4.69269                             -6.79665                           -13.24568                       

4320        3 magnesium_diatomic             + 2 ozone                          --> 6 magnesium_oxide                  Reaction Energy                 
MADNESS     -749782.34170                      -281098.10122                        -1030905.30107                     -24.85815                       
NWChem      -749773.32193                      -281083.76812                        -1030870.76080                     -13.67075                       
Diff. (M-N) -3.00659                           -7.16655                             -5.75671                           -11.18740                       

4783        2 methanol                       + 1 methyl_formate                 --> 4 methanol_radical                 Reaction Energy                 
MADNESS     -144139.00568                      -142687.22094                        -286588.88343                      237.34318                       
NWChem      -144131.87706                      -142680.25003                        -286561.69682                      250.43027                       
Diff. (M-N) -3.56431                           -6.97091                             -6.79665                           -13.08709                       

4790        3 methanol                       + 1 acetate_anion                  --> 5 methanol_radical                 Reaction Energy                 
MADNESS     -216208.50852                      -142358.16694                        -358236.10429                      330.57116                       
NWChem      -216197.81559                      -142351.38706                        -358202.12102                      347.08163                       
Diff. (M-N) -3.56431                           -6.77988                             -6.79665                           -16.51047                       

4795        3 methanol_radical               + 1 propene                        --> 3 ethanol_radical                  Reaction Energy                 
MADNESS     -214941.66258                      -73311.77767                         -288291.31243                      -37.87219                       
NWChem      -214921.27261                      -73308.33135                         -288277.44007                      -47.83611                       
Diff. (M-N) -6.79665                           -3.44632                             -4.62412                           9.96392                         

4798        5 methanol_radical               + 1 cyclopentane                   --> 5 ethanol_radical                  Reaction Energy                 
MADNESS     -358236.10429                      -122227.45877                        -480485.52072                      -21.95766                       
NWChem      -358202.12102                      -122221.72449                        -480462.40012                      -38.55461                       
Diff. (M-N) -6.79665                           -5.73428                             -4.62412                           16.59694                        

5189        4 ozone                          + 3 phosphorus_tetramer            --> 12 phosphorus_monoxide             Reaction Energy                 
MADNESS     -562196.20244                      -2561377.28227                       -3123686.01110                     -112.52639                      
NWChem      -562167.53624                      -2561347.90251                       -3123619.40795                     -103.96920                      
Diff. (M-N) -7.16655                           -9.79325                             -5.55026                           -8.55720                        

5694        5 sulfur_diatomic                + 2 sulfur_hexafluoride            --> 12 monosulfur_monofluoride         Reaction Energy                 
MADNESS     -2490287.17311                     -1245573.54658                       -3735299.89337                     560.82632                       
NWChem      -2490259.19854                     -1245516.31946                       -3735225.15584                     550.36216                       
Diff. (M-N) -5.59491                           -28.61356                            -6.22813                           10.46416                        

