import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc
import reaction_tools as rtools

# Some matplotlib stuff
rc('text', usetex=True)
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})

basis_sets =   ['aug-cc-pvtz','aug-cc-pvqz','cc-pvtz','cc-pvqz'] 
#basis_sets =   ['aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz'] 

basis_markers = { '3-21g':'tab:red', '6-31g*':'tab:blue', 'cc-pvdz':'tab:green', 'cc-pvtz':'tab:purple', 
                 'cc-pvqz':'tab:orange', 'aug-cc-pvdz':'tab:cyan', 
                  'aug-cc-pvtz':'tab:brown', 'aug-cc-pvqz':'tab:olive'}

basis_styles = { '3-21g':'v', '6-31g*':'v', 'cc-pvdz':'x', 'cc-pvtz':'x', 
                 'cc-pvqz':'x', 'aug-cc-pvdz':'o', 
                  'aug-cc-pvtz':'o', 'aug-cc-pvqz':'o' }

periodic_table = {'H':1,'He':2,'Li':3,'Be':4,'B':5,'C':6,'N':7,'O':8,'F':9,'Ne':10,'Na':11,'Mg':12,
  'Al':13,'Si':14,'P':15,'S':16,'Cl':17,'Ar':18,'K':19,'Ca':20,'Br':35}

##########  So that we can plot against system size later  #############################
molecules = {}

with open("../molecule_database.txt","r") as moldb:
    for line in moldb:
        if line.startswith("#"):
            continue
        else:
            molecules[line.strip().split()[0]] = {}

# Now store the elements and their associated counts in a dictionary for each molecule in the list
for molecule in molecules.keys():
    with open("geometries/{}".format(molecule),"r") as geom:
        for line in geom:
            clean_line = line.strip().split()
            element = clean_line[0]

            #Increment element count if already in the dictionary. Add it otherwise.
            try:
                molecules[molecule][element] += 1
            except KeyError:
                molecules[molecule][element] = 1
########################################################################################

def energies_from_rxnfile(infile):

    natom = {}
    madness_rxnenergies = {}
    nwchem_rxnenergies = {}
    rxn_names = {} 

    with open(infile,"r") as fname:
        f = [line.split() for line in fname.readlines()]

    i = 0

    while i < len(f):

        if f[i][0] == "Basis:":
            basis = f[i][1]
            # Next line is a header and doesn't need to be read, so skip 2 lines ahead
            i += 2
        else:
            rxn_num = f[i][0]
            try:
                natom[rxn_num] = int(f[i][1])
                nw = float(f[i][-1])
                mad = float(f[i][-2])
                # Force the energy to be in the forward direction. If one is positive
                # and the other negative, don't negate so that we preserved the direction.
                if nw > 0 or mad > 0:
                    nwchem_rxnenergies[rxn_num][basis] = -1*nw
                    madness_rxnenergies[rxn_num] = -1*mad
                else:
                    nwchem_rxnenergies[rxn_num][basis] = nw
                    madness_rxnenergies[rxn_num] = mad

            except KeyError:
                try:
                    natom[rxn_num] = int(f[i][1])
                    nwchem_rxnenergies[rxn_num] = {}
                    nw = float(f[i][-1])
                    mad = float(f[i][-2])
                    # Force the energy to be in the forward direction. If one is positive
                    # and the other negative, don't negate so that we preserved the direction.
                    if nw > 0 or mad >0:
                        nwchem_rxnenergies[rxn_num][basis] = -1*nw
                        madness_rxnenergies[rxn_num] = -1*mad
                    else:
                        nwchem_rxnenergies[rxn_num][basis] = nw
                        madness_rxnenergies[rxn_num] = mad
                except KeyError:
                    print("Error!")

            # Save reaction in a string
            rxn_names[rxn_num] = "{:2s} {} + {:2s} {} --> {:2s} {}".format(f[i][2], f[i][3], f[i][4], f[i][5], f[i][6], f[i][7])

            i += 1

    return natom, madness_rxnenergies, nwchem_rxnenergies, rxn_names

def largedevs_from_rxnfile(infile):

    natom = {}
    madness_rxnenergies = {}
    nwchem_rxnenergies = {}
    rxn_names = {} 

    large_devs = {}

    with open(infile,"r") as fname:
        f = [line.split() for line in fname.readlines()]

    i = 0

    while i < len(f):

        if f[i][0] == "Basis:":
            basis = f[i][1]
            # Next line is a header and doesn't need to be read, so skip 2 lines ahead
            i += 2
        else:
            rxn_num = f[i][0]
            try:
                natom[rxn_num] = int(f[i][1])
                nw = float(f[i][-1])
                mad = float(f[i][-2])
                # Force the energy to be in the forward direction. If one is positive
                # and the other negative, don't negate so that we preserved the direction.
                if nw > 0 or mad > 0:
                    nwchem_rxnenergies[rxn_num][basis] = -1*nw
                    madness_rxnenergies[rxn_num] = -1*mad
                else:
                    nwchem_rxnenergies[rxn_num][basis] = nw
                    madness_rxnenergies[rxn_num] = mad

            except KeyError:
                try:
                    natom[rxn_num] = int(f[i][1])
                    nwchem_rxnenergies[rxn_num] = {}
                    nw = float(f[i][-1])
                    mad = float(f[i][-2])
                    # Force the energy to be in the forward direction. If one is positive
                    # and the other negative, don't negate so that we preserved the direction.
                    if nw > 0 or mad >0:
                        nwchem_rxnenergies[rxn_num][basis] = -1*nw
                        madness_rxnenergies[rxn_num] = -1*mad
                    else:
                        nwchem_rxnenergies[rxn_num][basis] = nw
                        madness_rxnenergies[rxn_num] = mad
                except KeyError:
                    print("Error!")

            # Save reaction in a string
            rxn_names[rxn_num] = "{:2s} {} + {:2s} {} --> {:2s} {}".format(f[i][2], f[i][3], f[i][4], f[i][5], f[i][6], f[i][7])

            i += 1

    return natom, madness_rxnenergies, nwchem_rxnenergies, rxn_names

def plot_madvnw(madness_rxnenergies,nwchem_rxnenergies):

    fig = plt.figure()
    fig.suptitle("aA + bB $\longrightarrow$ cC",size='x-large')
    fig.text(0.50,0.04,"$\Delta _{\mathrm{rxn}}\mathrm{E~(kcal/mol)} (NWChem)$",ha='center',size='x-large')
    fig.text(0.04,0.50,"$\Delta _{\mathrm{rxn}}\mathrm{E~(kcal/mol)} (MADNESS)$",va='center',rotation='vertical',size='x-large')

    mad_energies = [madness_rxnenergies[rxn_num] for rxn_num in madness_rxnenergies.keys()]
    x = np.linspace(min(mad_energies),max(mad_energies),10)

    fnum = 1

    for basis in basis_sets:
        ax = fig.add_subplot(2,2,fnum)
        ax.scatter(mad_energies,
          [nwchem_rxnenergies[rxn_num][basis] for rxn_num in nwchem_rxnenergies.keys()],
          label=basis,marker=basis_styles[basis],color=basis_markers[basis])
        ax.plot(x,x,color='black')
        ax.set_title(basis)

        fnum += 1

    plt.show()

def plot_diff(natom,madness_rxnenergies,nwchem_rxnenergies,names):

    fig = plt.figure()
    fig.suptitle("aA + bB $\longrightarrow$ cC",size='x-large')
    fig.text(0.50,0.04,"$\mathrm{Number~of~Atoms}$",ha='center',size='x-large')
    fig.text(0.04,0.50,"$\Delta _{\mathrm{rxn}}\mathrm{E} \mathrm{~~~~(MADNESS-NWChem)~~(kcal/mol)}$",va='center',rotation='vertical',size='x-large')

    energy_diff = {}

    n = [natom[rxn_num] for rxn_num in natom.keys()]
    zero = [0 for natom in n]
 
    for rxn_num in nwchem_rxnenergies.keys():
        energy_diff[rxn_num] = {}
        mad_enrg = madness_rxnenergies[rxn_num]
        for basis in nwchem_rxnenergies[rxn_num].keys():
            energy_diff[rxn_num][basis] = mad_enrg-nwchem_rxnenergies[rxn_num][basis]

    # Figure number
    fnum = 1
    for basis in basis_sets:

        # Label points with difference greater than this value (kcal/mol)
        if basis == "cc-pvtz":
            tolerance = 20.0
        elif basis == "cc-pvqz" or basis == "aug-cc-pvtz":
            tolerance = 15.0
        elif basis == "aug-cc-pvqz":
            tolerance = 10.0
        tolerance = 20.0 
        ax = fig.add_subplot(2,2,fnum)
        ax.scatter(n,
         [energy_diff[rxn_num][basis] for rxn_num in nwchem_rxnenergies.keys()],
         label=basis,marker=basis_styles[basis],color=basis_markers[basis],s=0.5)
        ax.plot(n,zero,color='black')
        ax.set_title(basis) 

        fnum += 1

        mod = 1
        
        for key in nwchem_rxnenergies.keys():
        
            if abs(energy_diff[key][basis]) > tolerance:
                if mod % 2 == 0:
                    xshift = -40
                    yshift = -15
                else:
                    xshift = 40
                    yshift = 15

                ax.annotate(key,xy=(natom[key],energy_diff[key][basis]),xytext=(xshift, yshift),
                      textcoords='offset points', ha='right', va='bottom', 
                      bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
                      arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))
              
                print(key, "  ", names[key])
     
                mod += 1

    plt.show()

def summary_stat(mad,nw):

    energy_diff = {}

    for rxn_num in nw.keys():
        energy_diff[rxn_num] = {}
        mad_enrg = mad[rxn_num]
        for basis in nw[rxn_num].keys():
            energy_diff[rxn_num][basis] = mad_enrg-nw[rxn_num][basis]

    mean = {basis: np.mean([energy_diff[rxn_num][basis] for rxn_num in energy_diff.keys()]) for basis
      in basis_sets}
    stdev = {basis: np.std([energy_diff[rxn_num][basis] for rxn_num in energy_diff.keys()]) for basis
      in basis_sets}
    median = {basis: np.median([energy_diff[rxn_num][basis] for rxn_num in energy_diff.keys()]) for 
      basis in basis_sets}
    twentyfive = {basis: np.percentile([energy_diff[rxn_num][basis] for rxn_num in energy_diff.keys()],25) for 
      basis in basis_sets}
    seventyfive = {basis: np.percentile([energy_diff[rxn_num][basis] for rxn_num in energy_diff.keys()],75) for 
      basis in basis_sets}
    max_dev = {basis: max([energy_diff[rxn_num][basis] for rxn_num in energy_diff.keys()]) for 
      basis in basis_sets}

    with open("abtoc_summary_stats_hf.txt","w") as out:
        for basis in basis_sets:
            out.write("{}\nMean:{}\n25: {}\nMedian: {}\n75: {}\nStdev: {}\nMax Dev: {}\n\n".format(basis,mean[basis],
                twentyfive[basis],median[basis],seventyfive[basis],stdev[basis],max_dev[basis]))

natom, mad, nw, names = energies_from_rxnfile("abtoc_reactions_hf.txt")
#plot_madvnw(mad,nw)
plot_diff(natom,mad,nw, names)
#summary_stat(mad,nw)
