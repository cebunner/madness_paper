efile = "../MAD_Data/CONTRACTION/mad_hf.csv"

e_molecules = []
with open(efile,"r") as f:
    for line in f:
        if not line.startswith(",") or not line.startswith("#"):
            e_molecules.append(line.strip().split(",")[0])

molecules = []
with open("../molecule_database_modified.txt","r") as moldb:
    for line in moldb:
        if line.startswith("#"):
            continue
        else:
            molecules.append(line.strip().split()[0])

missing = [molecule for molecule in molecules if molecule not in e_molecules]
print(missing)
