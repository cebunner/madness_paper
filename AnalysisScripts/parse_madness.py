#
#   This python script will parse all the files ending with .out in a given directory,
#   assuming they are MADNESS output files, and will create a .csv of the relevant 
#   relevant information. 
#
#    Information pulled out: 
#       Energy          - total energy
#       Iterations      - number of iterations for completion (not really relevant here but being consistent.)
#       Dipole moment   - dipole moment tensor
#       Eigenvalues     - orbital energies (only pulling out alpha values)
#       Derivatives     - tensor of derivative of energy gradients 
#       Derivative time - time spent calculating derivatives
#       Wall time       - total wall time
#
#
#    To use: python parse_madness.py directory_with_output_files/ 
#
#    NOTE: Relative or absolute paths seem ok after minimal testing
#    NOTE: Output will be printed to terminal, so you must pipe it to a file to save it.
#


import os
import sys
import math

# Get directory that houses all output files to parse from command line
mydir = sys.argv[1]

# Print an output header
print("{},{},{},{},{},{},{},{}".format("", "Energy", "Iterations", "Dipole Moment", "Eigens", "Derivatives", "Derivative Time", "Wall Time"))


# For each file in that directory
for file in os.listdir(mydir):

   # Open the file
   inFile = open(mydir + "/" + file, "r")

   # Initialize variables with a value that will be painfull obvious if
   # its nto found during parsing
   total_energy = -10000
   iterations = 0
   dipole_moment = -10000
   eigenvalues = []
   derivatives = []
   derivative_time = -1
   wall_time = -1
   
   # Go through each line in the MADNESS ouput file 
   while True:
  
      line = inFile.readline()
      if not line: break 
   
      # Split line on white space
      line = line.split()       
      
      # Only do stuff if line isn't empty
      if line:
	
         # Determine if line contains something we want 
         if line[0] ==  "total" and line[1] != "charge":
            total_energy = float(line[1])	
         elif line[0] == 'alpha' and line[1] == 'eigenvalues':
            line = inFile.readline().split()	
            eigenvalues = line[1:] # Getting rid of [*]
            for i in range(len(eigenvalues)):
               eigenvalues[i] = float(eigenvalues[i])
         elif line[0] == "Total" and line[1] == "Dipole":	
            dipole_moment = float(line[3])
         elif line[0] == 'timer:' and line[1] == 'derivatives':
            derivative_time = float(line[2][:-1]) # The [:-1] gets rid of "s" on the end	
         elif line[0] == 'atom' and line[1] == "x":
            line = inFile.readline()
            line = inFile.readline().split()
            while line[0] != 'timer:':
               derivatives.append(float(line[4])) # x	
               derivatives.append(float(line[5])) # y
               derivatives.append(float(line[6])) # z
               line = inFile.readline().split()
         elif line[0] == "Total" and line[1] == 'wall':
            wall_time = float(line[3][:-1]) # The [:-1] gets rid of the "s" on the end         
         elif line[0] == "Iteration":
            iterations += 1			
   
   # Always clean up
   inFile.close()

   # Output all the information
   print("{},{},{},{},\"{}\",\"{}\",{},{}".format(file[:-4], total_energy, iterations, dipole_moment, eigenvalues, derivatives, derivative_time, wall_time))
