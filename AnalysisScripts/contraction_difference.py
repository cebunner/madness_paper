#
#  Prints the 10 largest differences in contracted versus uncontracted
#
import operator
import os


nw_cont = {}
nw_uncont = {}
nw_diff = {}

basis_sets = basis_sets = ['aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz']

# Load contracted data
# Parser for uncontracted LDA NW
# Parsing all the .csv files in ../NWChem_Data
# (Should be 8, one per basis set)
for data_file in os.listdir("../NWChem_Data/"):
    if not data_file.startswith("hf_unc") and data_file.startswith("hf") and data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nw:
            key = data_file[3:-4] # for HF
            #key = data_file[4:-4] # for LDA 
            for line in nw:
               if not line.startswith(","):
                    molname = line.strip().split(",")[0]
                    if molname not in nw_cont.keys():
                        nw_cont[molname] = {}
                    nw_cont[molname][key] = float(line.strip().split(",")[1])*627.50956

# Parser for uncontracted LDA NW
# Parsing all the .csv files in ../NWChem_Data
# (Should be 3, one per basis set)
for data_file in os.listdir("../NWChem_Data/"):
    if data_file.startswith("hf_unc") and data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nw:
            key = data_file[7:-4] # for HF
            #key = data_file[8:-4] # for LDA
            for line in nw:
               if not line.startswith(","):
                    molname = line.strip().split(",")[0]
                    if molname not in nw_uncont.keys():
                        nw_uncont[molname] = {}
                    nw_uncont[molname][key] = float(line.strip().split(",")[1])*627.50956

# Find differences
for basis in basis_sets:
   nw_diff[basis] = {}
   for molecule in nw_cont.keys():
      # Flipped the order of keys here on purpose.
      if molecule != "decane":
         nw_diff[basis][molecule] = nw_cont[molecule][basis] - nw_uncont[molecule][basis]

# Output in a smart way
for basis in basis_sets:
   # Convert this dictionary to a list
   basis_list = sorted(nw_diff[basis].items(), key=operator.itemgetter(1))
 
   print("\nBasis: ", basis)
   for i in range(10):
      #print("   {}".format(basis_list[-(i+1)]))
      # Get the info 
      molecule = basis_list[-(i+1)][0]
      diff = basis_list[-(i+1)][1]

      print(molecule, "&", nw_cont[molecule][basis], "&", nw_uncont[molecule][basis], "&", diff)


