#
#  Script to check if MADNESS and NWChem converged to the same state.
#
#  The script will basically just pull out all relevant information and print
#  it side by side for an easy comparison.
#
#  MADNESS and NWChem data must have the format: 
#       molecule_name.out 
#
#  To use:
#       python same_state.py path_to_nwchem_outputs path_to_madness_outputs
#

import os
import sys

# Main function that does everything
def answer(molecule, nw_path, mad_path):

   # Make sure paths end with a slash
   if mad_path[-1] != "/":
      mad_path += "/"
   if nw_path[-1] != "/":
      nw_path += "/"

   # Variables used in comparison stored here
   mad_info = {}
   nw_info = {}
 
   # Just needs to be here 
   alpha_beta = "alpha"
 
   # Open the madness file
   try:
      madFile = open(mad_path + molecule + ".out", "r")
   except:
      print("\nCould not find a madness file with name:", mad_path + molecule + ".out\n")
      return   

   # Parse MADNESS
   for line in madFile:
      # Commented out because nwchem doesn't report this explicitly
      #if line.strip().startswith("kinetic"):
      #   mad_info["kinetic"] = line.split()[-1]
      # Commented out because nwchem doesn't report this explicitly
      #elif line.strip().startswith("nuclear attraction"):
      #   mad_info["nuclear attraction"] = line.split()[-1]
      if line.strip().startswith("coulomb"):
         mad_info["coulomb"] = line.split()[-1]
      elif line.strip().startswith("exchange-correlation"):
         mad_info["exchange-correlation"] = line.split()[-1]
      elif line.strip().startswith("nuclear-repulsion"):
         mad_info["nuclear-repulsion"] = line.split()[-1]
      elif line.strip().startswith("total"):
         mad_info["total"] = line.split()[-1]
      elif line.strip().startswith("Analysis of"):
         # Could be alpha or beta analysis, next word indicates which
         alpha_beta = line.split()[2]
      elif line.strip().startswith("MO"):
         # Get the MO index for key as well
         index = line.split()[1]
         mad_info[alpha_beta + " MO " + index] = line.split("=")[2].split()[0]
   
   # Done with MADNESS
   madFile.close()
   
   # Open nwchem file
   try:
      nwFile = open(nw_path + molecule + ".out", "r")
   except:
      print("Could not find an nwchem file with name:", molecule + ".out\n")
      return

   # Parse NWChem
   for line in nwFile:
      if line.strip().startswith("Total DFT") or line.strip().startswith("Total SCF"):
         nw_info["total"] = line.split()[-1]
      elif line.strip().startswith("Coulomb"):
         nw_info["coulomb"] = line.split()[-1]
      elif line.strip().startswith("Exchange-Corr."):
         nw_info["exchange-correlation"] = line.split()[-1]
      elif line.strip().startswith("Effective nuclear"):
         nw_info["nuclear-repulsion"] = line.split()[-1]
      elif line.strip().startswith("DFT Final") or line.strip().startswith("SCF Final") or line.strip().startswith("UHF Final"):
         # Could be alpha or beta analysis, next word indicates which
         if line.split()[2].lower() == "molecular":
            # Closed shell in madness corresponds to alpha
            alpha_beta = "alpha"
         else:
            alpha_beta = line.split()[2].lower()
      elif line.strip().startswith("Vector"):
         # Only look at occupied orbitals
         if line.split("=")[1].startswith("1.0") or line.split("=")[1].startswith("2.0"):
            # Get index of MO, subtract one to match MADNESS indexing
            index = int(line.split()[1]) - 1
            nw_info[alpha_beta + " MO " + str(index)] = line.split("=")[-1][:-1].replace("D","E") # Getting rid of newline character
            
   # Done with NWChem
   nwFile.close()
   
   # Print a header
   print("\nMolecule:", molecule, "\n")
   print("{:>20}  {:<20}  {:<20}  {:<20}".format("Value", "NWChem", "MADNESS", " |Difference|"))
   print("-"*90)
   for key in nw_info.keys():
      print("{:>20}: {:< 20}  {:< 20}  {:< 20}".format(key, float(nw_info[key]), float(mad_info[key]), abs(float(nw_info[key]) - float(mad_info[key]))))
   print("")

# Get paths from command line
nw_path = sys.argv[1]
mad_path = sys.argv[2]

# Run function over all files in directory
for f in os.listdir(nw_path):
   if f.endswith(".out"):
      answer(f[:-4], nw_path, mad_path)
   
