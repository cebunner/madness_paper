import os

# Runs all .py files not named 'reaction_tools.py' in current directory
for f in os.listdir("./"):
   if f.startswith("plot_pbe0_pcvNz") and f.endswith(".py") and f != 'reaction_tools.py' and f != 'run_all.py':
      print("Starting", f)
      os.system("python {}".format(f))
      print("Finished", f, "\n")
print("\n\nFinished everything.")
