import os

basis_sets = ['pcseg-1', 'pcseg-2', 'pcseg-3', 'aug-pcseg-1', 'aug-pcseg-2', 'unc-aug-pcseg-2']
functionals = ['hf', 'lda', 'b3lyp', 'pbe0', 'pbe96']

nw = {}
mad = {}

# Parser for all NW
# Parsing all the .csv files in ../NWChem_Data
for data_file in os.listdir("../NWChem_Data/"):
    if data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nwfile:
            data = data_file[:-4].split("_")
            func = data[0]
            basis = data[1]
            if func not in nw.keys() and func in functionals:
               nw[func] = {}
            if basis in basis_sets:
               nw[func][basis] = {}
               for line in nwfile:
                  if not line.startswith(","):
                       molname = line.strip().split(",")[0]
                       nw[func][basis][molname] = float(line.strip().split(",")[1])

# Generic MADNESS parser
for data_file in os.listdir("../MAD_Data/"):
   if data_file.endswith(".csv"):   
      with open("../MAD_Data/" + data_file, "r") as madfile:
          data = data_file[:-4].split("_")

          if data[-1] == 'new':
             func = data[1]
             if func not in mad.keys():
                mad[func] = {}
             for line in madfile:
                 if not line.startswith(","):
                     mad[func][line.strip().split(",")[0]] = float(line.strip().split(",")[1])

# Get a set of keys in all three basis sets
# and print out those that aren't
key_set = {}
for func in functionals:
   print(func)
   for basis in basis_sets:
      print(basis)
      for mol in mad[func].keys(): # MADNESS has all molecules
         if (mol not in nw[func][basis].keys()):
            print(mol)
         else:
            key_set[mol] = 1.0
      print("\n")
   print("\n\n\n")
