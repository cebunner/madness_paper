import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib import rc
import os
from scipy.stats import linregress

# Matplotlib formating things
rc('text', usetex=True)
basis_sets = ['3-21g','6-31g*','cc-pvdz','cc-pvtz','cc-pvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz']
basis_markers = { '3-21g':'tab:red', '6-31g*':'tab:blue', 'cc-pvdz':'tab:green', 'cc-pvtz':'tab:purple', 
                 'cc-pvqz':'tab:orange', 'aug-cc-pvdz':'tab:cyan', 
                  'aug-cc-pvtz':'tab:brown', 'aug-cc-pvqz':'tab:olive', 'MADNESS':'tab:red',
                  'unc-aug-cc-pvtz':'tab:brown', 'unc-aug-cc-pvqz':'tab:olive'}

basis_styles = { '3-21g':'v', '6-31g*':'v', 'cc-pvdz':'x', 'cc-pvtz':'x', 
                 'cc-pvqz':'x', 'aug-cc-pvdz':'o', 
                  'aug-cc-pvtz':'o', 'aug-cc-pvqz':'o', 'MADNESS':'*',
                  'unc-aug-cc-pvtz':'o', 'unc-aug-cc-pvqz':'o'}

red_triangle = mlines.Line2D([],[],color='tab:red',marker='v',ls='None')
blue_triangle = mlines.Line2D([],[],color='tab:blue',marker='v',ls='None')
green_x = mlines.Line2D([],[],color='tab:green',marker='x',ls='None')
purple_x = mlines.Line2D([],[],color='tab:purple',marker='x',ls='None')
orange_x = mlines.Line2D([],[],color='tab:orange',marker='x',ls='None')
cyan_o = mlines.Line2D([],[],color='tab:cyan',marker='o',ls='None')
brown_o = mlines.Line2D([],[],color='tab:brown',marker='o',ls='None')
olive_o = mlines.Line2D([],[],color='tab:olive',marker='o',ls='None')
brown_s = mlines.Line2D([],[],color='tab:brown',marker='s',ls='None')
olive_s = mlines.Line2D([],[],color='tab:olive',marker='s',ls='None')

default_handles = [red_triangle, blue_triangle,green_x,purple_x,orange_x,cyan_o,brown_o,olive_o,brown_s,olive_s]
default_labels = ['3-21g','6-31g*','cc-pvdz','cc-pvtz','cc-pvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz','unc-aug-cc-pvtz','unc-aug-cc-pvqz']

# All csv's have this format
# molecule, energy, iterations, dipole moment, eigenvalues, derivatives, derivative times, wall time

nw = {}
mad = {}

# Parser for contracted LDA NW
# Parsing all the .csv files in ../NWChem_Data
for data_file in os.listdir("../NWChem_Data/"):
    if data_file.startswith("b3lyp") and data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nwfile:
            key = data_file[6:-4]
            nw[key] = {}
            for line in nwfile:
               if not line.startswith(","):
                    molname = line.strip().split(",")[0]
                    if molname != 'decane':
                       nw[key][molname] = float(line.strip().split(",")[-1])

# Generic MADNESS parser
with open("../MAD_Data/mad_b3lyp_pm_new.csv", "r") as madfile:
    for line in madfile:
        if not line.startswith(","):
            mad[line.strip().split(",")[0]] = float(line.strip().split(",")[-1])

# Reads in the number of electrons each molecule possesses
num_electrons = {}
with open("../NWChem_Data/num_electrons.txt","r") as input_file:
    for line in input_file:
        num_electrons[line.split()[0]] = int(line.split()[1])

# Reads in the number of basis functions each molecule possesses
# First key is basis set, second key is molecule
with open("../NWChem_Data/number_bfunctions.txt","r") as input_file:
    for i,line in enumerate([line.strip().replace("\n","") for line in input_file]):
        # Header
        if i == 0:
            continue
        # List of basis sets
        elif i == 1:
            bsets = line.split()
            num_bfuncs = {bset:{} for bset in bsets}
            # Store basis set column #
            # +1 accounts for molecule name column being empty
            colnums = {bsets[i]:i+1 for i in range(len(bsets))}
        else:
            sl = line.split()
            molname = sl[0]
            for bset in num_bfuncs.keys():
                num_bfuncs[bset][molname] = int(sl[colnums[bset]])

# Plot energy differences
fig, ax = plt.subplots(2,4,sharey=False, sharex=True, figsize=(9,6))

# To get axis right
a = fig.add_subplot(111, frameon=False)
plt.tick_params(labelcolor='none',top=False, bottom=False, left=False, right=False)
plt.grid(False)
plt.xlabel(r"$\log_{10}(N)$",size='x-large')
plt.ylabel(r"$\mathrm{Wall~~Time}~~[\log_{10}(s)]$",size='x-large')
 
for j, basis in enumerate(['6-31g*','cc-pvdz','cc-pvtz','cc-pvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz','MADNESS']):
        
    times = [] 

    if basis != "MADNESS":
       bfuncs = []
       for key in nw[basis].keys():
           times.append(np.log10(nw[basis][key]))
           bfuncs.append(np.log10(num_bfuncs[basis][key]))
       med_bf = np.percentile(bfuncs,75)
       top75 = {bfuncs[i]:times[i] for i in range(len(bfuncs)) if bfuncs[i] >= med_bf}
    else:
        elecs = [] 
        for key in mad.keys():
            if(mad[key] < 0):
               print(key, mad[key])
               continue
            times.append(np.log10(mad[key]))
            elecs.append(np.log10(num_electrons[key]))
        med_elec = np.percentile(elecs,75)
        top75 = {elecs[i]:times[i] for i in range(len(elecs)) if elecs[i] >= med_elec}

    if j <= 3:
        z = 0
    else:
        z = 1

    ax[z,j%4].set_title(basis)
    
    # Fit linear regression to largest 75% of the calculations
    x = [key for key in top75.keys()]
    y = [top75[key] for key in top75.keys()]

    slope, intercept, rvalue, pvalue, stderr = linregress(x,y)

    if basis != "MADNESS":

        fit_x = np.arange(min(bfuncs),max(bfuncs),0.01)
        fit_y = fit_x*slope + intercept

        ax[z,j%4].errorbar(bfuncs, times,
                color=basis_markers[basis],marker=basis_styles[basis],ls='None')

        ax[z,j%4].errorbar(fit_x, fit_y,
                color='black',label=r"$\frac{dy}{dx} = $"+r"$~{:<3.1f}$".format(slope),marker='None',ls='-')

        ax[z,j%4].set_ylim([-1,6])
        ax[z,j%4].legend()

    else:

        fit_x = np.arange(min(elecs),max(elecs),0.01)
        fit_y = fit_x*slope + intercept

        ax[z,j%4].errorbar(elecs, times,
                color=basis_markers[basis],marker=basis_styles[basis],ls='None')

        ax[z,j%4].errorbar(fit_x, fit_y,
                color='black',label=r"$\frac{dy}{dx} = $"+r"$~{:<3.1f}$".format(slope),marker='None',ls='-')

        ax[z,j%4].set_ylim([-1,6])
        ax[z,j%4].legend()
   
plt.tight_layout()
plt.savefig("b3lyp_times.pdf")
#plt.show() 

