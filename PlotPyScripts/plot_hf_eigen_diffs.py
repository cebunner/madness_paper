import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib import rc
import os
import operator

# Matplotlib formating things
rc('text', usetex=True)
basis_sets = ['cc-pvdz','cc-pvtz','cc-pvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz', 'unc-aug-cc-pvdz', 'unc-aug-cc-pvtz', 'unc-aug-cc-pvqz']
basis_markers = { '3-21g':'tab:red', '6-31g*':'tab:blue', 'cc-pvdz':'tab:green', 'cc-pvtz':'tab:purple', 
                 'cc-pvqz':'tab:orange', 'aug-cc-pvdz':'tab:cyan', 
                  'aug-cc-pvtz':'tab:brown', 'aug-cc-pvqz':'tab:olive', 'MADNESS':'tab:red',
                  'unc-aug-cc-pvdz':'tab:blue','unc-aug-cc-pvtz':'tab:pink', 'unc-aug-cc-pvqz':'tab:grey'}

basis_styles = { '3-21g':'v', '6-31g*':'v', 'cc-pvdz':'x', 'cc-pvtz':'x', 
                 'cc-pvqz':'x', 'aug-cc-pvdz':'o', 
                  'aug-cc-pvtz':'o', 'aug-cc-pvqz':'o', 'MADNESS':'*', 'unc-aug-cc-pvdz':'o',
                  'unc-aug-cc-pvtz':'o', 'unc-aug-cc-pvqz':'o'}

red_triangle = mlines.Line2D([],[],color='tab:red',marker='v',ls='None')
blue_triangle = mlines.Line2D([],[],color='tab:blue',marker='v',ls='None')
green_x = mlines.Line2D([],[],color='tab:green',marker='x',ls='None')
purple_x = mlines.Line2D([],[],color='tab:purple',marker='x',ls='None')
orange_x = mlines.Line2D([],[],color='tab:orange',marker='x',ls='None')
cyan_o = mlines.Line2D([],[],color='tab:cyan',marker='o',ls='None')
brown_o = mlines.Line2D([],[],color='tab:brown',marker='o',ls='None')
olive_o = mlines.Line2D([],[],color='tab:olive',marker='o',ls='None')
brown_s = mlines.Line2D([],[],color='tab:brown',marker='s',ls='None')
olive_s = mlines.Line2D([],[],color='tab:olive',marker='s',ls='None')

default_handles = [red_triangle, blue_triangle,green_x,purple_x,orange_x,cyan_o,brown_o,olive_o,brown_s,olive_s]
default_labels = ['3-21g','6-31g*','cc-pvdz','cc-pvtz','cc-pvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz','unc-aug-cc-pvtz','unc-aug-cc-pvqz']


nw = {}
mad = {}

# Parser for contracted HF NW
# Parsing all the .csv files in ../NWChem_Data
# (Should be 3, one per basis set)
for data_file in os.listdir("../NWChem_Data/"):
    if data_file.startswith("hf") and data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nwfile:
            key = data_file[3:-4]
            if key in basis_sets:
               nw[key] = {}
               for line in nwfile:
                  if not line.startswith(","):
                       molname = line.strip().split(",")[0]
                       nw[key][molname] = []
                       eigs = line[len(molname)+1:].strip().split("[")[1][:-4].split(",")
                       for e in eigs:
                          nw[key][molname].append(float(e))
                  
                   

# Generic MADNESS parser
with open("../MAD_Data/mad_hf_pm_new.csv", "r") as madfile:
    for line in madfile:
        if not line.startswith(","):
            molname = line.strip().split(",")[0]
            mad[molname] = []
            eigs = line[len(molname)+1:].strip().split("[")[1][:-4].split(",")
            for e in eigs:
               mad[molname].append(float(e))


# Want same ylim on each plot in each column
ylims = []

# Plot energy differences
fig, ax = plt.subplots(3,3,sharey=False, sharex=True, figsize=(9,6))

# To get axis right
a = fig.add_subplot(111, frameon=False)
plt.tick_params(labelcolor='none',top=False, bottom=False, left=False, right=False)
plt.grid(False)
plt.xlabel("MADNESS HOMO Energy (kcal/mol)",size='x-large')
plt.ylabel(r'$|MAD - NW|$ HOMO Energy (kcal/mol)',size='x-large')

energy_diff = {}
 
for j, basis in enumerate(basis_sets):
        
    x = []
    y = []


    print("\n\nBasis:", basis)
    print("{:<24s} {:>12s}".format("Molecule", "|Difference|"))
    for i, molecule in enumerate(mad.keys()):
        y.append(abs(mad[molecule][-1] -nw[basis][molecule][-1])*627.50956)
        x.append(mad[molecule][-1]*627.50956)
        if(y[-1] > 1):
           print("{:<24s} {:>12.6f}".format(molecule, y[-1]))

    if j <= 2:
        z = 0
    elif j <= 5:
        z = 1
    else:
        z = 2
    ax[z,j%3].set_title(basis)
    #ax[z,j%3].axhline(0,color='k')
    ax[z,j%3].errorbar(x,y,color=basis_markers[basis],label=basis,marker=basis_styles[basis],ls='None')
    
    # Making axes match inside each column
    if j <= 2:
       ylims.append(ax[z,j%3].get_ylim()) 
    if j >2:
       # Check if current min lim is smaller
       if ax[z,j%3].get_ylim()[0] < ylims[j%3][0]:
          # Current is smaller, so save it 
          ylims[j%3] = [ax[z,j%3].get_ylim()[0], ylims[j%3][1]]
       # Check if current max lim is larger
       if ax[z,j%3].get_ylim()[1] > ylims[j%3][1]:
          # Current is smaller, so save it
          ylims[j%3] = [ylims[j%3][0], ax[z,j%3].get_ylim()[1]]

for i in range(3):
    # And set current ylims to what they should be
    ax[0,i%3].set_ylim([ylims[i%3][0]*1.1, ylims[i%3][1]*1.1])
    ax[1,i%3].set_ylim([ylims[i%3][0]*1.1, ylims[i%3][1]*1.1])
    ax[2,i%3].set_ylim([ylims[i%3][0]*1.1, ylims[i%3][1]*1.1])
 

plt.tight_layout()
plt.savefig("hf_eigens_diffs.pdf")
#plt.show() 

