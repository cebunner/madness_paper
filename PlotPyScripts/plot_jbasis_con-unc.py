import numpy as np
import reaction_tools as rtools
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib import rc
import os

# Matplotlib formating things
rc('text', usetex=True)
basis_sets = ['pcseg-1', 'pcseg-2', 'pcseg-3', 'aug-pcseg-1', 'aug-pcseg-2', 'unc-aug-pcseg-2']
basis_markers_hf = { 'pcseg-1':'tab:red', 'pcseg-2':'tab:blue', 'pcseg-3':'tab:green', 'aug-pcseg-1':'tab:purple', 
                 'aug-pcseg-2':'tab:orange', 'unc-aug-pcseg-2':'tab:cyan', 'MADNESS':'tab:red', 'diff':'b'}
basis_markers_pb = { 'pcseg-1':'tab:red', 'pcseg-2':'tab:blue', 'pcseg-3':'tab:green', 'aug-pcseg-1':'tab:purple', 
                 'aug-pcseg-2':'tab:orange', 'unc-aug-pcseg-2':'tab:cyan', 'MADNESS':'tab:red', 'diff':'r'}

basis_styles = { 'pcseg-1':'v', 'pcseg-2':'v', 'pcseg-3':'x', 'aug-pcseg-1':'x', 
                 'aug-pcseg-2':'x', 'unc-aug-pcseg-2':'o', 'MADNESS':'*', 'diff':'+'}


red_triangle = mlines.Line2D([],[],color='tab:red',marker='v',ls='None')
blue_triangle = mlines.Line2D([],[],color='tab:blue',marker='v',ls='None')
green_x = mlines.Line2D([],[],color='tab:green',marker='x',ls='None')
purple_x = mlines.Line2D([],[],color='tab:purple',marker='x',ls='None')
orange_x = mlines.Line2D([],[],color='tab:orange',marker='x',ls='None')
cyan_o = mlines.Line2D([],[],color='tab:cyan',marker='o',ls='None')
brown_o = mlines.Line2D([],[],color='tab:brown',marker='o',ls='None')
olive_o = mlines.Line2D([],[],color='tab:olive',marker='o',ls='None')
brown_s = mlines.Line2D([],[],color='tab:brown',marker='s',ls='None')
olive_s = mlines.Line2D([],[],color='tab:olive',marker='s',ls='None')

default_handles = [red_triangle, blue_triangle,green_x,purple_x,orange_x,cyan_o,brown_o,olive_o,brown_s,olive_s]
default_labels = ['pcseg-1', 'pcseg-2', 'pcseg-3', 'aug-pcseg-1', 'aug-pcseg-2', 'unc-aug-pcseg-2', 'diff']

nw_hf = {}
nw_pb = {}
mad_hf = {}
mad_pb = {}

# Parser for HF NW
# Parsing all the .csv files in ../NWChem_Data
for data_file in os.listdir("../NWChem_Data/"):
    if data_file.startswith("hf") and data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nwfile:
            key = data_file[3:-4]
            nw_hf[key] = {}
            if key in basis_sets:
               for line in nwfile:
                  if not line.startswith(","):
                       molname = line.strip().split(",")[0]
                       nw_hf[key][molname] = float(line.strip().split(",")[1])

# Parser for PBE0 NW
# Parsing all the .csv files in ../NWChem_Data
for data_file in os.listdir("../NWChem_Data/"):
    if data_file.startswith("pbe0") and data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nwfile:
            key = data_file[5:-4]
            nw_pb[key] = {}
            if key in basis_sets:
               for line in nwfile:
                  if not line.startswith(","):
                       molname = line.strip().split(",")[0]
                       nw_pb[key][molname] = float(line.strip().split(",")[1])



# Generic MADNESS parser
with open("../MAD_Data/mad_hf_pm_new.csv", "r") as madfile:
    for line in madfile:
        if not line.startswith(","):
            mad_hf[line.strip().split(",")[0]] = float(line.strip().split(",")[1])

# Generic MADNESS parser
with open("../MAD_Data/mad_pbe0_pm_new.csv", "r") as madfile:
    for line in madfile:
        if not line.startswith(","):
            mad_pb[line.strip().split(",")[0]] = float(line.strip().split(",")[1])



# Reads in the number of electrons each molecule possesses
num_electrons = {}
with open("../NWChem_Data/num_electrons.txt","r") as input_file:
    for line in input_file:
        num_electrons[line.split()[0]] = int(line.split()[1])

# Want ylims to be same across a row
ylims = []

# Plot energy differences
fig, ax = plt.subplots(2,3,sharey=False, sharex=True, figsize=(9,6))

# To get axis right
a = fig.add_subplot(111, frameon=False)
plt.tick_params(labelcolor='none',top=False, bottom=False, left=False, right=False)
plt.grid(False)
plt.xlabel("$\mathrm{Number~of~Electrons}$")
plt.ylabel("$\Delta _{\mathrm{rxn}}\mathrm{E}~~ \mathrm{(kcal/mol)}$")

energy_diff = {}
for i in range(2):
  for j in range(3):

    if j == 0:
        basis = 'aug-pcseg-2'
        basis_label = basis
    elif j == 1:
        basis = 'unc-aug-pcseg-2'
        basis_label = basis
    elif j == 2:
        basis = 'Difference' 
        basis_label = 'diff' 
    
    energy_diff[basis] = {}    
    if i == 1:
       for molecule in mad_hf.keys():
           if basis in basis_sets:
               energy_diff[basis][molecule] = (mad_hf[molecule] - nw_hf[basis][molecule])*-627.50956
               if(energy_diff[basis][molecule] < 0.0):
                   print("{} {} {}: {} {} {}".format(basis, "hf", molecule, mad_hf[molecule], nw_hf[basis][molecule], energy_diff[basis][molecule]/627.50956))
           elif basis == 'Difference' and j == 2:
               energy_diff[basis][molecule] = energy_diff['aug-pcseg-2'][molecule] -  energy_diff['unc-aug-pcseg-2'][molecule]
               if(energy_diff[basis][molecule] < 0.0 or (energy_diff[basis][molecule] > 60)):
                   print("{} {} {}: {} {} {}".format(basis, "hf", molecule, nw_hf['aug-pcseg-2'][molecule], nw_hf['unc-aug-pcseg-2'][molecule], energy_diff[basis][molecule]))
    else:
       for molecule in mad_pb.keys():
           if basis in basis_sets:
               energy_diff[basis][molecule] = (mad_pb[molecule] - nw_pb[basis][molecule])*-627.50956
               if(energy_diff[basis][molecule] < 0.0):
                   print("{} {} {}: {} {} {}".format(basis, "pbe0", molecule, mad_pb[molecule], nw_pb[basis][molecule], energy_diff[basis][molecule]/627.50956))
           elif basis == 'Difference' and j == 2:
               energy_diff[basis][molecule] = energy_diff['aug-pcseg-2'][molecule] -  energy_diff['unc-aug-pcseg-2'][molecule]
               if(energy_diff[basis][molecule] < 0.0 or (energy_diff[basis][molecule] > 60)):
                   print("{} {} {}: {} {} {}".format(basis, "pbe0", molecule, nw_pb['aug-pcseg-2'][molecule], nw_pb['unc-aug-pcseg-2'][molecule], energy_diff[basis][molecule]))
 
    electrons = [num_electrons[molecule] for molecule in energy_diff[basis].keys()]


    title = basis 
    if i == 0 and j == 1:
       title = 'Hartree-Fock \n {}'.format(basis)
    elif i == 1 and j == 1:
       title = 'DFT-PBE0 \n {}'.format(basis)

    if i == 0:
       ax[i,j%3].set_title(title)
       ax[i,j%3].plot(electrons, np.zeros(len(electrons)),color='black',ls='-')   
       ax[i,j%3].errorbar(electrons, [energy_diff[basis][molecule] for molecule in energy_diff[basis].keys()],
                   color=basis_markers_hf[basis_label],label=title,marker=basis_styles[basis_label],ls='None')
    else:
       ax[i,j%3].set_title(title)
       ax[i,j%3].plot(electrons, np.zeros(len(electrons)),color='black',ls='-')   
       ax[i,j%3].errorbar(electrons, [energy_diff[basis][molecule] for molecule in energy_diff[basis].keys()],
                   color=basis_markers_pb[basis_label],label=title,marker=basis_styles[basis_label],ls='None')

    # Making axes match inside each row 
    if j == 0:
       ylims = ax[i,j%3].get_ylim()
    else:
       # Check if current min lim is smaller
       if ax[i,j%3].get_ylim()[0] < ylims[0]:
          # Current is smaller, so save it 
          ylims = [ax[i,j%3].get_ylim()[0], ylims[1]]
       # Check if current max lim is larger
       if ax[i,j%3].get_ylim()[1] > ylims[1]:
          # Current is smaller, so save it
          ylims = [ylims[0], ax[i,j%3].get_ylim()[1]]

       # And set current ylims to what they should be
       ax[i,0].set_ylim([ylims[0]*1.1, ylims[1]*1.1])
       ax[i,1].set_ylim([ylims[0]*1.1, ylims[1]*1.1])
       ax[i,2].set_ylim([ylims[0]*1.1, ylims[1]*1.1])
 

plt.tight_layout()
plt.savefig("jbasis_con-unc.pdf")
#plt.show() 

