import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib import rc
import os
import operator

# Matplotlib formating things
rc('text', usetex=True)
basis_sets =   ['cc-pvdz', 'cc-pvtz','cc-pvqz', 'aug-cc-pvdz', 'aug-cc-pvtz','aug-cc-pvqz', 'unc-aug-cc-pvdz', 'unc-aug-cc-pvtz', 'unc-aug-cc-pvqz'] 

basis_markers = { '3-21g':'tab:red', '6-31g*':'tab:blue', 'cc-pvdz':'tab:green', 'cc-pvtz':'tab:purple', 
                 'cc-pvqz':'tab:orange', 'aug-cc-pvdz':'tab:cyan', 
                  'aug-cc-pvtz':'tab:brown', 'aug-cc-pvqz':'tab:olive',
                  'unc-aug-cc-pvdz':'tab:red', 'unc-aug-cc-pvtz':'tab:pink',
                  'unc-aug-cc-pvqz':'tab:grey'}

basis_styles = { '3-21g':'v', '6-31g*':'v', 'cc-pvdz':'x', 'cc-pvtz':'x', 
                 'cc-pvqz':'x', 'aug-cc-pvdz':'o', 
                  'aug-cc-pvtz':'o', 'aug-cc-pvqz':'o', 'unc-aug-cc-pvdz':'d',
                  'unc-aug-cc-pvtz':'d', 'unc-aug-cc-pvqz':'d'} 



#Directory with the geometry files
geom_dir = "../SI/geometries"

#Dictionaries for the total energy
nw_tot = {}
mad_tot = {}
atomic_mad = {}

#Dictionary for the atomization energies
nw_atom = {}
mad_atom = {}

# Parser for HF NW
# Parsing all the .csv files in ../NWChem_Data
# (Should be 8, one per basis set)
for data_file in os.listdir("../NWChem_Data/"):
    if data_file.startswith("hf") and data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nw:
            key = data_file[3:-4]
            for line in nw:
               if not line.startswith(","):
                    molname = line.strip().split(",")[0]
                    if molname not in nw_tot.keys():
                        nw_tot[molname] = {}
                    nw_tot[molname][key] = float(line.strip().split(",")[1])

# Generic MADNESS parser
with open("../MAD_Data/mad_hf_pm_new.csv", "r") as madfile:
    for line in madfile:
        if not line.startswith(","):
            mad_tot[line.strip().split(",")[0]] = float(line.strip().split(",")[1])

# Reads in the number of electrons each molecule possesses
num_electrons = {}
with open("../NWChem_Data/num_electrons.txt","r") as input_file:
    for line in input_file:
        num_electrons[line.split()[0]] = int(line.split()[1])

# Read pre-computed NWChem atomization energies
with open("../NWChem_Data/hf_atomization_energies.txt","r") as input_file:
    for i,line in enumerate([line.strip().replace("\n","") for line in input_file]):
        # Header
        if i == 0:
            continue
        # List of basis sets
        elif i == 1:
            bsets = line.split()
            nw_atom = {bset:{} for bset in bsets}
            # Store basis set column #
            # +1 accounts for molecule name column being empty
            colnums = {bsets[i]:i+1 for i in range(len(bsets))}
        else:
            sl = line.split()
            molname = sl[0]
            for bset in nw_atom.keys():
                nw_atom[bset][molname] = float(sl[colnums[bset]])

# Read in MADNESS atomic energies
with open("../MAD_Data/mad_hf_atomic.txt","r") as input_file:
    for i,line in enumerate([line.strip().replace("\n","") for line in input_file]):
        atomic_mad[line.strip().split()[0]] = float(line.strip().split()[1]) 


# Calculate MADNESS atomic energies
for i, molecule in enumerate(mad_tot.keys()):
    ae = 0.0
    with open("{}/{}".format(geom_dir,molecule)) as f:
        for line in f:
            ae += atomic_mad[line.strip().split()[0]]
    mad_atom[molecule] = (ae - mad_tot[molecule])*627.50956
#    print("{:<24s} {:<20.12f}".format(molecule,mad_atom[molecule]))

# Want same ylim on each plot in each column
ylims = []

# Plot energy differences
fig, ax = plt.subplots(3,3,sharey=False, sharex=True, figsize=(9,6))

# To get axis right
a = fig.add_subplot(111, frameon=False)
plt.tick_params(labelcolor='none',top=False, bottom=False, left=False, right=False)
plt.grid(False)
plt.xlabel("$\mathrm{Number~of~Electrons}$",size='x-large')
plt.ylabel("$\Delta \mathrm{E}_{\mathrm{atomization}}~~ \mathrm{(kcal/mol)}$",size='x-large')

atomization_diff = {}

for j, basis in enumerate(basis_sets):
        
    electrons = [num_electrons[molecule] for molecule in mad_tot.keys()]

    for i, molecule in enumerate(mad_tot.keys()):
        atomization_diff[molecule] = (mad_atom[molecule] - nw_atom[basis][molecule])
        if(abs(atomization_diff[molecule]) > 5 and basis == 'unc-aug-cc-pvqz'):
            print("{} {} {}".format(molecule, basis, atomization_diff[molecule]))

    # Print 10 biggest differences if basis = aug-cc-pvqz
    if(basis == "aug-cc-pvqz"):       
       print("10 biggest outliers for aug-cc-pvqz:\n")
       sorted_e = sorted(atomization_diff.items(), key=operator.itemgetter(1))
       for i in range(-1,-11,-1):
          print("   ", sorted_e[i])


    if j < 3:
        z = 0
    elif j < 6:
        z = 1
    else:
        z = 2
    ax[z,j%3].set_title(basis)
    ax[z,j%3].plot(electrons, np.zeros(len(electrons)),color='black',ls='-')   
    ax[z,j%3].errorbar(electrons, [atomization_diff[molecule] for molecule in atomization_diff.keys()],
                color=basis_markers[basis],label=basis,marker=basis_styles[basis],ls='None')
    
    # Making axes match inside each column
    if j <= 3:
       ylims.append(ax[z,j%3].get_ylim()) 
    if j > 3:
       # Check if current min lim is smaller
       if ax[z,j%3].get_ylim()[0] < ylims[j%3][0]:
          # Current is smaller, so save it 
          ylims[j%3] = [ax[z,j%3].get_ylim()[0], ylims[j%3][1]]
       # Check if current max lim is larger
       if ax[z,j%3].get_ylim()[1] > ylims[j%3][1]:
          # Current is smaller, so save it
          ylims[j%3] = [ylims[j%3][0], ax[z,j%3].get_ylim()[1]]

# And set current ylims to what they should be
for j in range(3):
   ax[0,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
   ax[1,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
   ax[2,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
 

plt.tight_layout()
plt.savefig("hf_atomization.pdf")
#plt.show() 

