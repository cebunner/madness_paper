import os

# Runs all .py files starting with "plot" 
for f in os.listdir("./"):
   if f.startswith("plot") and f.endswith(".py"):
      print("Starting", f)
      os.system("python {}".format(f))
      print("Finished", f, "\n")
print("\n\nFinished everything.")
