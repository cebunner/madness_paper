import operator
import numpy as np
import reaction_tools as rtools
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib import rc
import os

# For uncontracted LDA
basis_sets = ['aug-cc-pvtz','aug-cc-pvqz', 'unc-aug-cc-pvtz','unc-aug-cc-pvqz']
basis_colors = dict(zip(basis_sets, ['teal','brown','gold']))

# Matplotlib formating things
rc('text', usetex=True)
basis_markers = { '3-21g':'tab:red', '6-31g*':'tab:blue', 'cc-pvdz':'tab:green', 'cc-pvtz':'tab:purple', 
                 'cc-pvqz':'tab:orange', 'aug-cc-pvdz':'tab:cyan', 
                  'aug-cc-pvtz':'tab:brown', 'aug-cc-pvqz':'tab:olive', 'MADNESS':'tab:red',
                  'unc-aug-cc-pvtz':'tab:pink', 'unc-aug-cc-pvqz':'tab:grey',
                  'tz diff':'r', 'qz diff':'b'}

basis_styles = { '3-21g':'v', '6-31g*':'v', 'cc-pvdz':'x', 'cc-pvtz':'x', 
                 'cc-pvqz':'x', 'aug-cc-pvdz':'o', 
                  'aug-cc-pvtz':'o', 'aug-cc-pvqz':'o', 'MADNESS':'*',
                  'unc-aug-cc-pvtz':'d', 'unc-aug-cc-pvqz':'d',
                  'tz diff':'+', 'qz diff':'+'}

red_triangle = mlines.Line2D([],[],color='tab:red',marker='v',ls='None')
blue_triangle = mlines.Line2D([],[],color='tab:blue',marker='v',ls='None')
green_x = mlines.Line2D([],[],color='tab:green',marker='x',ls='None')
purple_x = mlines.Line2D([],[],color='tab:purple',marker='x',ls='None')
orange_x = mlines.Line2D([],[],color='tab:orange',marker='x',ls='None')
cyan_o = mlines.Line2D([],[],color='tab:cyan',marker='o',ls='None')
brown_o = mlines.Line2D([],[],color='tab:brown',marker='o',ls='None')
olive_o = mlines.Line2D([],[],color='tab:olive',marker='o',ls='None')
brown_s = mlines.Line2D([],[],color='tab:brown',marker='s',ls='None')
olive_s = mlines.Line2D([],[],color='tab:olive',marker='s',ls='None')

default_handles = [red_triangle, blue_triangle,green_x,purple_x,orange_x,cyan_o,brown_o,olive_o,brown_s,olive_s]
default_labels = ['3-21g','6-31g*','cc-pvdz','cc-pvtz','cc-pvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz','unc-aug-cc-pvtz','unc-aug-cc-pvqz']


nw = {}
mad = {}

# Parser for contracted LDA NW
# Parsing all the .csv files in ../NWChem_Data
# (Should be 3, one per basis set)
for data_file in os.listdir("../NWChem_Data/"):
    if not data_file.startswith("lda_unc") and data_file.startswith("lda") and data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nwfile:
            key = data_file[4:-4]
            nw[key] = {}
            for line in nwfile:
               if not line.startswith(","):
                    molname = line.strip().split(",")[0]
                    nw[key][molname] = float(line.strip().split(",")[1])


# Parser for uncontracted HF NW
# Parsing all the .csv files in ../NWChem_Data
# (Should be 3, one per basis set)
for data_file in os.listdir("../NWChem_Data/"):
    if data_file.startswith("lda_unc") and data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nwfile:
            key = data_file[4:-4]
            key = key.replace('_','-') # changing underscore in name to hyphen
            nw[key] = {}
            for line in nwfile:
               if not line.startswith(","):
                    molname = line.strip().split(",")[0]
                    nw[key][molname] = float(line.strip().split(",")[1])

# Generic MADNESS parser
with open("../MAD_Data/mad_lda_pm_new.csv", "r") as madfile:
    for line in madfile:
        if not line.startswith(","):
            mad[line.strip().split(",")[0]] = float(line.strip().split(",")[1])

# Reads in the number of electrons each molecule possesses
num_electrons = {}
with open("../NWChem_Data/num_electrons.txt","r") as input_file:
    for line in input_file:
        num_electrons[line.split()[0]] = int(line.split()[1])

# Want y axis to be the same in each row
ylims = 0.0

# Plot energy differences
fig, ax = plt.subplots(2,3,sharey=False, sharex=True, figsize=(9,6))

# To get axis right
a = fig.add_subplot(111, frameon=False)
plt.tick_params(labelcolor='none',top=False, bottom=False, left=False, right=False)
plt.grid(False)
plt.xlabel("$\mathrm{Number~of~Electrons}$")
plt.ylabel("$\Delta _{\mathrm{rxn}}\mathrm{E}~~ \mathrm{(kcal/mol)}$")

energy_diff = {}
for j in range(6):

    if j == 0:
        basis = 'aug-cc-pvtz'
        basis_label = basis
    elif j == 1:
        basis = 'unc-aug-cc-pvtz'
        basis_label = basis
    elif j == 2:
        basis = 'Difference' 
        basis_label = 'tz diff' 
    elif j == 3:
        basis = 'aug-cc-pvqz'
        basis_label = basis
    elif j == 4:
        basis = 'unc-aug-cc-pvqz'
        basis_label = basis
    elif j == 5:
        basis = 'Difference'
        basis_label = 'qz diff' 
    
    energy_diff[basis] = {}    
    electrons = [num_electrons[molecule] for molecule in mad.keys()]

    for i, molecule in enumerate(mad.keys()):
        if basis in basis_sets:
            energy_diff[basis][molecule] = (mad[molecule] - nw[basis][molecule])*-627.50956
            if(energy_diff[basis][molecule] < 0.0):
                print("First {} {}: {} {} {}".format(basis, molecule, mad[molecule], nw[basis][molecule], energy_diff[basis][molecule]/627.50956))
        elif basis == 'Difference' and j == 2:
            energy_diff[basis][molecule] = energy_diff['aug-cc-pvtz'][molecule] -  energy_diff['unc-aug-cc-pvtz'][molecule]
            if(energy_diff[basis][molecule] < 0.0):
                print("Second {} {}: {} {} {}".format(basis, molecule, nw['aug-cc-pvtz'][molecule], nw['unc-aug-cc-pvtz'][molecule], energy_diff[basis][molecule]))
        elif basis == 'Difference' and j == 5:
            energy_diff[basis][molecule] = energy_diff['aug-cc-pvqz'][molecule] -  energy_diff['unc-aug-cc-pvqz'][molecule]
            if(energy_diff[basis][molecule] < 0.0):
                print("Third {} {}: {} {} {}".format(basis, molecule, nw['aug-cc-pvqz'][molecule], nw['unc-aug-cc-pvqz'][molecule], energy_diff[basis][molecule]))

    # Print 5 biggest differences if basis = unc-aug-cc-pvqz
    if(basis == "unc-aug-cc-pvqz"):
       sorted_e = sorted(energy_diff[basis].items(), key=operator.itemgetter(1))
       for i in range(5):
          print(sorted_e[i])

    z = 0
    title = basis
    if j > 2:
       z = 1   

    ax[z,j%3].set_title(title)
    ax[z,j%3].plot(electrons, np.zeros(len(electrons)),color='black',ls='-')   
    ax[z,j%3].errorbar(electrons, [energy_diff[basis][molecule] for molecule in energy_diff[basis].keys()],
                color=basis_markers[basis_label],label=title,marker=basis_styles[basis_label],ls='None')

    # Making axes match inside each row 
    if (z == 0 and j == 0) or (z == 1 and j == 3):
       ylims = ax[z,j%3].get_ylim()
    else:
       # Check if current min lim is smaller
       if ax[z,j%3].get_ylim()[0] < ylims[0]:
          # Current is smaller, so save it 
          ylims = [ax[z,j%3].get_ylim()[0], ylims[1]]
       # Check if current max lim is larger
       if ax[z,j%3].get_ylim()[1] > ylims[1]:
          # Current is smaller, so save it
          ylims = [ylims[0], ax[z,j%3].get_ylim()[1]]

       # And set current ylims to what they should be
       ax[z,0].set_ylim([ylims[0]*1.1, ylims[1]*1.1])
       ax[z,1].set_ylim([ylims[0]*1.1, ylims[1]*1.1])
       ax[z,2].set_ylim([ylims[0]*1.1, ylims[1]*1.1])
 

plt.tight_layout()
plt.savefig("lda_con-unc.pdf")
#plt.show() 

