import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib import rc
import os

# For uncontracted HF 
basis_sets = ['aug-cc-pvtz','aug-cc-pvqz', 'unc-aug-cc-pvtz','unc-aug-cc-pvqz']
basis_colors = dict(zip(basis_sets, ['teal','brown','gold']))

# Matplotlib formating things
rc('text', usetex=True)
basis_markers = { '3-21g':'tab:red', '6-31g*':'tab:blue', 'cc-pvdz':'tab:green', 'cc-pvtz':'tab:purple', 
                 'cc-pvqz':'tab:orange', 'aug-cc-pvdz':'tab:cyan', 
                  'aug-cc-pvtz':'tab:brown', 'aug-cc-pvqz':'tab:olive', 'MADNESS':'tab:red', 'unc-aug-cc-pvdz':'tab:red',
                  'unc-aug-cc-pvtz':'tab:pink', 'unc-aug-cc-pvqz':'tab:grey'}

basis_styles = { '3-21g':'v', '6-31g*':'v', 'cc-pvdz':'x', 'cc-pvtz':'x', 
                 'cc-pvqz':'x', 'aug-cc-pvdz':'o', 
                  'aug-cc-pvtz':'o', 'aug-cc-pvqz':'o', 'MADNESS':'*', 'unc-aug-cc-pvdz':'d',
                  'unc-aug-cc-pvtz':'d', 'unc-aug-cc-pvqz':'d'}



red_triangle = mlines.Line2D([],[],color='tab:red',marker='v',ls='None')
blue_triangle = mlines.Line2D([],[],color='tab:blue',marker='v',ls='None')
green_x = mlines.Line2D([],[],color='tab:green',marker='x',ls='None')
purple_x = mlines.Line2D([],[],color='tab:purple',marker='x',ls='None')
orange_x = mlines.Line2D([],[],color='tab:orange',marker='x',ls='None')
cyan_o = mlines.Line2D([],[],color='tab:cyan',marker='o',ls='None')
brown_o = mlines.Line2D([],[],color='tab:brown',marker='o',ls='None')
olive_o = mlines.Line2D([],[],color='tab:olive',marker='o',ls='None')
brown_s = mlines.Line2D([],[],color='tab:brown',marker='s',ls='None')
olive_s = mlines.Line2D([],[],color='tab:olive',marker='s',ls='None')

default_handles = [red_triangle, blue_triangle,green_x,purple_x,orange_x,cyan_o,brown_o,olive_o,brown_s,olive_s]
default_labels = ['3-21g','6-31g*','cc-pvdz','cc-pvtz','cc-pvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz','unc-aug-cc-pvtz','unc-aug-cc-pvqz']


nw = {}
mad = {}

# All csv's have this format
# molecule, energy, iterations, dipole moment, eigenvalues, derivatives, derivative times, wall time

# Parser for contracted HF NW
# Parsing all the .csv files in ../NWChem_Data
# (Should be 3, one per basis set)
for data_file in os.listdir("../NWChem_Data/"):
    if not data_file.startswith("hf_unc") and data_file.startswith("hf") and data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nwfile:
            key = data_file[3:-4]
            nw[key] = {}
            for line in nwfile:
               if not line.startswith(","):
                    molname = line.strip().split(",")[0]
                    nw[key][molname] = 0.0
                    forces = line.strip().split("\"")[3].strip("[").strip("]").split(",")
                    for force in forces:
                       if force == '':
                          continue
                       nw[key][molname] += float(force)**2


# Parser for uncontracted HF NW
# Parsing all the .csv files in ../NWChem_Data
# (Should be 3, one per basis set)
for data_file in os.listdir("../NWChem_Data/"):
    if data_file.startswith("hf_unc") and data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nwfile:
            key = data_file[3:-4]
            key = key.replace('_','-') # changing underscore in name to hyphen
            nw[key] = {}
            for line in nwfile:
               if not line.startswith(","):
                    molname = line.strip().split(",")[0]
                    nw[key][molname] = 0.0
                    forces = line.strip().split("\"")[3].strip("[").strip("]").split(",")
                    for force in forces:
                       if force == '':
                          continue
                       nw[key][molname] += float(force)**2


# Generic MADNESS parser
with open("../MAD_Data/mad_hf_pm_new.csv", "r") as madfile:
    for line in madfile:
        if not line.startswith(","):
            forces = line.strip().split("\"")[3].strip("[").strip("]").split(",")
            molname = line.strip().split(",")[0]
            mad[molname] = 0.0
            for force in forces:
                if force == '':
                   continue
                mad[molname] += float(force)**2


##########  So that we can plot against system size later  #############################
molecules = {}

with open("../molecule_database.txt","r") as moldb:
    for line in moldb:
        if line.startswith("#"):
            continue
        else:
            molecules[line.strip().split()[0]] = {}

# Now store the elements and their associated counts in a dictionary for each molecule in the list
for molecule in molecules.keys():
    with open("../AnalysisScripts/geometries/{}".format(molecule),"r") as geom:
        for line in geom:
            clean_line = line.strip().split()
            element = clean_line[0]

            #Increment element count if already in the dictionary. Add it otherwise.
            try:
                molecules[molecule][element] += 1
            except KeyError:
                molecules[molecule][element] = 1

# Finally get a count of all atoms in each molecule
natoms = {}
for molecule in molecules.keys():
 
   natoms[molecule] = 0

   for atom in molecules[molecule]:
      natoms[molecule] += molecules[molecule][atom]

########################################################################################


# Want same ylim on each plot in each column
ylims = []

# Dipoles
fig, ax = plt.subplots(3,3,sharey=False, sharex=True, figsize=(9,6))
fig.text(0.35,0.02,r'Number of Atoms',fontsize='xx-large',va='bottom')
fig.text(0.05,0.75,r'Force Difference per Atom',fontsize='xx-large',rotation='vertical')

for j, basis in enumerate(['cc-pvdz','cc-pvtz','cc-pvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz',
                           'unc-aug-cc-pvdz', 'unc-aug-cc-pvtz', 'unc-aug-cc-pvqz']):
    
    force_diff = {}
    atoms = [] 
        
    for molecule in mad.keys():

        atoms.append(natoms[molecule])
        force_diff[molecule] = np.sqrt(mad[molecule] - nw[basis][molecule])/natoms[molecule]
        
        if mad[molecule] - nw[basis][molecule] < 0: 
            print("{}\t{}\t{}\t{}".format('hf', molecule,basis, mad[molecule] - nw[basis][molecule]))
            print("{:16.8f} {:16.8f}\n".format(nw[basis][molecule], mad[molecule]))
        
    if j <= 2:
        z = 0
    elif j <= 5:
        z = 1
    else:
        z = 2
        
    plt.setp(ax[z,j%3].get_yticklabels(), fontsize=6)
        
    ax[z,j%3].set_title(basis)
    ax[z,j%3].axhline(0, color='k')
    ax[z,j%3].errorbar(atoms, [force_diff[molecule] for molecule in force_diff.keys()],
                color=basis_markers[basis],label=basis,marker=basis_styles[basis],ls='None')

    # Making axes match inside each column
    if j <= 2:
       ylims.append(ax[z,j%3].get_ylim()) 
    if j >2:
       # Check if current min lim is smaller
       if ax[z,j%3].get_ylim()[0] < ylims[j%3][0]:
          # Current is smaller, so save it 
          ylims[j%3] = [ax[z,j%3].get_ylim()[0], ylims[j%3][1]]
       # Check if current max lim is larger
       if ax[z,j%3].get_ylim()[1] > ylims[j%3][1]:
          # Current is smaller, so save it
          ylims[j%3] = [ylims[j%3][0], ax[z,j%3].get_ylim()[1]]

       # And set current ylims to what they should be
       ax[0,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
       ax[1,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
       ax[2,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
    
plt.savefig("hf_forces.pdf")
#plt.show()
