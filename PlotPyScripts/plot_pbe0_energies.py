import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib import rc
import os
import operator

# Matplotlib formating things
rc('text', usetex=True)
basis_sets = ['cc-pvdz','cc-pvtz','cc-pvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz', 'unc-aug-cc-pvdz', 'unc-aug-cc-pvtz', 'unc-aug-cc-pvqz']

basis_markers = { '3-21g':'tab:red', '6-31g*':'tab:blue', 'cc-pvdz':'tab:green', 'cc-pvtz':'tab:purple', 
                 'cc-pvqz':'tab:orange', 'aug-cc-pvdz':'tab:cyan', 
                  'aug-cc-pvtz':'tab:brown', 'aug-cc-pvqz':'tab:olive',
                  'unc-aug-cc-pvdz':'tab:red', 'unc-aug-cc-pvtz':'tab:pink',
                  'unc-aug-cc-pvqz':'tab:grey'}

basis_styles = { '3-21g':'v', '6-31g*':'v', 'cc-pvdz':'x', 'cc-pvtz':'x', 
                 'cc-pvqz':'x', 'aug-cc-pvdz':'o', 
                  'aug-cc-pvtz':'o', 'aug-cc-pvqz':'o', 'unc-aug-cc-pvdz':'d',
                  'unc-aug-cc-pvtz':'d', 'unc-aug-cc-pvqz':'d'} 

nw = {}
mad = {}

# Parser for contracted B3LYP NW
# Parsing all the .csv files in ../NWChem_Data
# (Should be 3, one per basis set)
for data_file in os.listdir("../NWChem_Data/"):
    if data_file.startswith("pbe0") and data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nwfile:
            key = data_file[5:-4]
            nw[key] = {}
            for line in nwfile:
               if not line.startswith(","):
                    molname = line.strip().split(",")[0]
                    nw[key][molname] = float(line.strip().split(",")[1])

# Generic MADNESS parser
with open("../MAD_Data/mad_pbe0_pm_new.csv", "r") as madfile:
    for line in madfile:
        if not line.startswith(","):
            mad[line.strip().split(",")[0]] = float(line.strip().split(",")[1])

# Reads in the number of electrons each molecule possesses
num_electrons = {}
with open("../NWChem_Data/num_electrons.txt","r") as input_file:
    for line in input_file:
        num_electrons[line.split()[0]] = int(line.split()[1])

# Want same ylim on each plot in each column
ylims = []

# Plot energy differences
fig, ax = plt.subplots(3,3,sharey=False, sharex=True, figsize=(9,6))

# To get axis right
a = fig.add_subplot(111, frameon=False)
plt.tick_params(labelcolor='none',top=False, bottom=False, left=False, right=False)
plt.grid(False)
plt.xlabel("$\mathrm{Number~of~Electrons}$",size='x-large')
plt.ylabel("$\Delta \mathrm{E}~~ \mathrm{(kcal/mol)}$",size='x-large')

energy_diff = {}
 
for j, basis in enumerate(basis_sets):
        
    bfuncs = []
    electrons = [num_electrons[molecule] for molecule in mad.keys()]

    for i, molecule in enumerate(mad.keys()):
        energy_diff[molecule] = (mad[molecule] - nw[basis][molecule])*-627.50956

    # Print 5 biggest differences if basis = aug-cc-pvqz
    if(basis == "aug-cc-pvqz"):
       sorted_e = sorted(energy_diff.items(), key=operator.itemgetter(1))
       for i in range(1,6,1):
          print(sorted_e[-i])
    
    if j <= 2:
        z = 0
    elif j <= 5:
        z = 1
    else:
        z = 2

    ax[z,j%3].set_title(basis)
    ax[z,j%3].plot(electrons, np.zeros(len(electrons)),color='black',ls='-')   
    ax[z,j%3].errorbar(electrons, [energy_diff[molecule] for molecule in energy_diff.keys()],
                color=basis_markers[basis],label=basis,marker=basis_styles[basis],ls='None')
 
    # Making axes match inside each column
    if j <= 2:
       ylims.append(ax[z,j%3].get_ylim()) 
    if j >2:
       # Check if current min lim is smaller
       if ax[z,j%3].get_ylim()[0] < ylims[j%3][0]:
          # Current is smaller, so save it 
          ylims[j%3] = [ax[z,j%3].get_ylim()[0], ylims[j%3][1]]
       # Check if current max lim is larger
       if ax[z,j%3].get_ylim()[1] > ylims[j%3][1]:
          # Current is smaller, so save it
          ylims[j%3] = [ylims[j%3][0], ax[z,j%3].get_ylim()[1]]

# And set current ylims to what they should be
for j in range(3): 
   ax[0,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
   ax[1,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
   ax[2,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
           

plt.tight_layout()
plt.savefig("pbe0_energies.pdf")
#plt.show() 

