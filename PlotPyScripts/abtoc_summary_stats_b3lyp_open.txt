 & & Mean & Median & Stdev & Max Dev
 & cc-pvdz          &       12.001 &        6.956 &       14.128 &      148.592\\ 
 & cc-pvtz          &        1.229 &        0.684 &        1.958 &       36.329\\ 
 & cc-pvqz          &        0.710 &        0.421 &        0.933 &       13.448\\ 
 & aug-cc-pvdz      &        9.083 &        4.536 &       11.425 &      111.814\\ 
 & aug-cc-pvtz      &        1.359 &        0.882 &        1.699 &       30.215\\ 
 & aug-cc-pvqz      &        0.409 &        0.257 &        0.615 &       16.369\\ 
