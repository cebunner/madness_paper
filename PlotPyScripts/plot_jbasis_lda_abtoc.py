import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc
import reaction_tools as rtools

# Some matplotlib stuff
rc('text', usetex=True)
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})

basis_sets = ['pcseg-1', 'pcseg-2', 'pcseg-3','aug-pcseg-1', 'aug-pcseg-2', 'unc-aug-pcseg-2']

basis_markers = { 'pcseg-1':'red', 'pcseg-2':'green', 'pcseg-3':'purple',
                 'aug-pcseg-1':'orange', 'aug-pcseg-2':'teal', 'unc-aug-pcseg-2':'brown'}

basis_styles = { 'pcseg-1':'v', 'pcseg-2':'x', 'pcseg-3':'d',
                 'aug-pcseg-1':'v', 'aug-pcseg-2':'x', 'unc-aug-pcseg-2':'o'} 

periodic_table = {'H':1,'He':2,'Li':3,'Be':4,'B':5,'C':6,'N':7,'O':8,'F':9,'Ne':10,'Na':11,'Mg':12,
  'Al':13,'Si':14,'P':15,'S':16,'Cl':17,'Ar':18,'K':19,'Ca':20,'Br':35}

##########  So that we can plot against system size later  #############################
molecules = {}

with open("../molecule_database.txt","r") as moldb:
    for line in moldb:
        if line.startswith("#"):
            continue
        else:
            molecules[line.strip().split()[0]] = {}

# Now store the elements and their associated counts in a dictionary for each molecule in the list
for molecule in molecules.keys():
    with open("../AnalysisScripts/geometries/{}".format(molecule),"r") as geom:
        for line in geom:
            clean_line = line.strip().split()
            element = clean_line[0]

            #Increment element count if already in the dictionary. Add it otherwise.
            try:
                molecules[molecule][element] += 1
            except KeyError:
                molecules[molecule][element] = 1
########################################################################################

def energies_from_rxnfile(infile):

    natom = {}
    madness_rxnenergies = {}
    nwchem_rxnenergies = {}
    rxn_names = {} 

    with open(infile,"r") as fname:
        f = [line.split() for line in fname.readlines()]

    i = 0

    while i < len(f):

        if f[i][0] == "Basis:":
            basis = f[i][1]
            # Next line is a header and doesn't need to be read, so skip 2 lines ahead
            i += 2
        else:
            rxn_num = f[i][0]
            try:
                natom[rxn_num] = int(f[i][1])
                nwchem_rxnenergies[rxn_num][basis] = float(f[i][9])
                madness_rxnenergies[rxn_num] = float(f[i][8])

            except KeyError:
                try:
                    natom[rxn_num] = int(f[i][1])
                    nwchem_rxnenergies[rxn_num] = {}
                    rxn_names[rxn_num] = {}
                    nwchem_rxnenergies[rxn_num][basis] = float(f[i][9])
                    madness_rxnenergies[rxn_num] = float(f[i][8])
                except KeyError:
                    print("Error!")

            # Save reaction in a string
            rxn_names[rxn_num][basis] = "{:>3s} {:<24s} + {:>3s} {:<24s} --> {:>3s} {:<24s} {:>16.8f} {:>16.8f} {:>16.8f}".format(
                                         f[i][2], f[i][3], f[i][4], f[i][5], f[i][6], f[i][7],
                                         madness_rxnenergies[rxn_num], nwchem_rxnenergies[rxn_num][basis],
                                         abs(madness_rxnenergies[rxn_num] - nwchem_rxnenergies[rxn_num][basis]))

            i += 1

    return natom, madness_rxnenergies, nwchem_rxnenergies, rxn_names

def plot_diff(natom,madness_rxnenergies,nwchem_rxnenergies,names,labels):

    fig, ax = plt.subplots(2,3, sharey=False, sharex=True, figsize=(9,6))

    # Getting shared axes right
    a = fig.add_subplot(111, frameon=False)
    plt.tick_params(labelcolor='none',top=False, bottom=False, left=False, right=False)
    plt.grid(False)
    plt.xlabel("$\mathrm{Number~of~Atoms}$",size='x-large')
    plt.ylabel("$\Delta _{\mathrm{rxn}}\mathrm{E} \mathrm{~~~~(MRA-Gaussian)~~(kcal/mol)}$",labelpad=15,size='x-large')

    energy_diff = {}
    ylims = []
    n = [natom[rxn_num] for rxn_num in natom.keys()]
    zero = [0 for natom in n]
 
    for rxn_num in nwchem_rxnenergies.keys():
        energy_diff[rxn_num] = {}
        mad_enrg = madness_rxnenergies[rxn_num]
        for basis in nwchem_rxnenergies[rxn_num].keys():
            energy_diff[rxn_num][basis] = mad_enrg-nwchem_rxnenergies[rxn_num][basis]

    for j,basis in enumerate(basis_sets):

        if j < 3:
           z = 0
        elif j < 6:
           z = 1
        else:
           z = 2

        ax[z, j%3].scatter(n,
         [energy_diff[rxn_num][basis] for rxn_num in nwchem_rxnenergies.keys()],
         label=basis,marker=basis_styles[basis],color=basis_markers[basis],s=0.5)
        ax[z, j%3].plot(n,zero,color='black')
        ax[z, j%3].set_title(basis) 

        # Making axes match inside each column
        if j <= 3:
           ylims.append(ax[z,j%3].get_ylim()) 
        if j >3:
           # Check if current min lim is smaller
           if ax[z,j%3].get_ylim()[0] < ylims[j%3][0]:
              # Current is smaller, so save it 
              ylims[j%3] = [ax[z,j%3].get_ylim()[0], ylims[j%3][1]]
           # Check if current max lim is larger
           if ax[z,j%3].get_ylim()[1] > ylims[j%3][1]:
              # Current is smaller, so save it
              ylims[j%3] = [ylims[j%3][0], ax[z,j%3].get_ylim()[1]]

        #print("basis =", basis, "\n", ylims, "\n", ax[z,j%3].get_xlim())
        # And set current ylims to what they should be
        ax[0,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
        ax[1,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
 
    plt.savefig('jbasis_lda_abtoc.pdf', bbox_inches='tight')
    #plt.show()

def summary_stat(mad,nw):

    energy_diff = {}

    for rxn_num in nw.keys():
        energy_diff[rxn_num] = {}
        mad_enrg = mad[rxn_num]
        for basis in nw[rxn_num].keys():
            energy_diff[rxn_num][basis] = mad_enrg-nw[rxn_num][basis]

    mean = {basis: np.mean([abs(energy_diff[rxn_num][basis]) for rxn_num in energy_diff.keys()]) for basis
      in basis_sets}
    stdev = {basis: np.std([abs(energy_diff[rxn_num][basis]) for rxn_num in energy_diff.keys()]) for basis
      in basis_sets}
    median = {basis: np.median([abs(energy_diff[rxn_num][basis]) for rxn_num in energy_diff.keys()]) for 
      basis in basis_sets}
    twentyfive = {basis: np.percentile([abs(energy_diff[rxn_num][basis]) for rxn_num in energy_diff.keys()],25) for 
      basis in basis_sets}
    seventyfive = {basis: np.percentile([abs(energy_diff[rxn_num][basis]) for rxn_num in energy_diff.keys()],75) for 
      basis in basis_sets}
    max_dev = {basis: max([abs(energy_diff[rxn_num][basis]) for rxn_num in energy_diff.keys()]) for 
      basis in basis_sets}

    with open("abtoc_summary_stats_jbasis_lda.txt","w") as out:
        out.write("% & & Mean & Median & Stdev & Max Dev\n")
        for basis in basis_sets:
            out.write(" & {:16s} & {:>12.3f} & {:>12.3f} & {:>12.3f} & {:>12.3f}\\\\ \n".format(basis,mean[basis],median[basis],stdev[basis],max_dev[basis]))

natom, mad, nw, names = energies_from_rxnfile("../AnalysisScripts/abtoc_reactions_jbasis_lda.txt")
#plot_diff(natom,mad,nw,names,True) # Labels
plot_diff(natom,mad,nw,names,False) # No labels
summary_stat(mad,nw)
