import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib import rc
import os





# Matplotlib formating things
rc('text', usetex=True)
basis_sets = ['3-21g','6-31g*','cc-pvdz','cc-pvtz','cc-pvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz']
basis_markers = { '3-21g':'tab:red', '6-31g*':'tab:blue', 'cc-pvdz':'tab:green', 'cc-pvtz':'tab:purple', 
                 'cc-pvqz':'tab:orange', 'aug-cc-pvdz':'tab:cyan', 
                  'aug-cc-pvtz':'tab:brown', 'aug-cc-pvqz':'tab:olive', 'MADNESS':'tab:red',
                  'unc-aug-cc-pvtz':'tab:brown', 'unc-aug-cc-pvqz':'tab:olive',
                  'tz diff':'tab:brown', 'qz diff':'tab:olive'}

basis_styles = { '3-21g':'v', '6-31g*':'v', 'cc-pvdz':'x', 'cc-pvtz':'x', 
                 'cc-pvqz':'x', 'aug-cc-pvdz':'o', 
                  'aug-cc-pvtz':'o', 'aug-cc-pvqz':'o', 'MADNESS':'*'}

red_triangle = mlines.Line2D([],[],color='tab:red',marker='v',ls='None')
blue_triangle = mlines.Line2D([],[],color='tab:blue',marker='v',ls='None')
green_x = mlines.Line2D([],[],color='tab:green',marker='x',ls='None')
purple_x = mlines.Line2D([],[],color='tab:purple',marker='x',ls='None')
orange_x = mlines.Line2D([],[],color='tab:orange',marker='x',ls='None')
cyan_o = mlines.Line2D([],[],color='tab:cyan',marker='o',ls='None')
brown_o = mlines.Line2D([],[],color='tab:brown',marker='o',ls='None')
olive_o = mlines.Line2D([],[],color='tab:olive',marker='o',ls='None')
brown_s = mlines.Line2D([],[],color='tab:brown',marker='s',ls='None')
olive_s = mlines.Line2D([],[],color='tab:olive',marker='s',ls='None')

default_handles = [red_triangle, blue_triangle,green_x,purple_x,orange_x,cyan_o,brown_o,olive_o,brown_s,olive_s]
default_labels = ['3-21g','6-31g*','cc-pvdz','cc-pvtz','cc-pvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz','unc-aug-cc-pvtz','unc-aug-cc-pvqz']


nw = {}
mad = {}

# All csv's have this format
# molecule, energy, iterations, dipole moment, eigenvalues, derivatives, derivative times, wall time

# Parser for contracted B3LYP NW
# Parsing all the .csv files in ../NWChem_Data
# (Should be 3, one per basis set)
for data_file in os.listdir("../NWChem_Data/"):
    if data_file.startswith("b3lyp") and data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nwfile:
            key = data_file[6:-4]
            nw[key] = {}
            for line in nwfile:
               if not line.startswith(","):
                    molname = line.strip().split(",")[0]
                    nw[key][molname] = float(line.strip().split(",")[3])


# Generic MADNESS parser
with open("../MAD_Data/mad_b3lyp_pm_new.csv", "r") as madfile:
    for line in madfile:
        if not line.startswith(","):
            mad[line.strip().split(",")[0]] = float(line.strip().split(",")[3])

# Want same ylim on each plot in each column
ylims = []

# Dipoles
fig, ax = plt.subplots(2,3,sharey=False, sharex=True, figsize=(9,6))
fig.text(0.35,0.02,r'Dipole Moment (Debye)',fontsize='xx-large',va='bottom')
fig.text(0.05,0.75,r'Dipole Difference (Debye)',fontsize='xx-large',rotation='vertical')

for j, basis in enumerate(['cc-pvdz','cc-pvtz','cc-pvqz','aug-cc-pvdz','aug-cc-pvtz','aug-cc-pvqz']):
    
    dipole_diff = {}
        
    for molecule in mad.keys():

        dipole_diff[molecule] = mad[molecule] - nw[basis][molecule]
        
        if abs(dipole_diff[molecule]) > 0.05 and basis == "aug-cc-pvqz":
            print("{}\t{}\t{}\t{}".format('b3lyp', molecule,basis, dipole_diff[molecule]))
            print("{:16.8f} {:16.8f}\n".format(nw[basis][molecule], mad[molecule]))
        
    dips = np.abs([mad[molecule] for molecule in dipole_diff.keys()])
        
    if j <= 2:
        z = 0
    else:
        z = 1
        
    plt.setp(ax[z,j%3].get_yticklabels(), fontsize=6)
        
    ax[z,j%3].set_title(basis)
    ax[z,j%3].plot(dips, np.zeros(len(dips)),color='black',ls='-')   
    ax[z,j%3].errorbar(dips, [dipole_diff[molecule] for molecule in dipole_diff.keys()],
                color=basis_markers[basis],label=basis,marker=basis_styles[basis],ls='None')
 
    # Making axes match inside each column
    if j <= 2:
       ylims.append(ax[z,j%3].get_ylim()) 
    if j >2:
       # Check if current min lim is smaller
       if ax[z,j%3].get_ylim()[0] < ylims[j%3][0]:
          # Current is smaller, so save it 
          ylims[j%3] = [ax[z,j%3].get_ylim()[0], ylims[j%3][1]]
       # Check if current max lim is larger
       if ax[z,j%3].get_ylim()[1] > ylims[j%3][1]:
          # Current is smaller, so save it
          ylims[j%3] = [ylims[j%3][0], ax[z,j%3].get_ylim()[1]]

       # And set current ylims to what they should be
       ax[0,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
       ax[1,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
 
  
plt.savefig("b3lyp_dipoles.pdf")
#plt.show()
