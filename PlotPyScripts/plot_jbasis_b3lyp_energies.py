import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib import rc
import os

# Matplotlib formating things
rc('text', usetex=True)
basis_sets = ['pcseg-1', 'pcseg-2', 'pcseg-3', 'aug-pcseg-1', 'aug-pcseg-2', 'unc-aug-pcseg-2']
basis_markers = { 'pcseg-1':'tab:red', 'pcseg-2':'tab:blue', 'pcseg-3':'tab:green', 'aug-pcseg-1':'tab:purple', 
                 'aug-pcseg-2':'tab:orange', 'unc-aug-pcseg-2':'tab:cyan', 'MADNESS':'tab:red'}

basis_styles = { 'pcseg-1':'v', 'pcseg-2':'v', 'pcseg-3':'x', 'aug-pcseg-1':'x', 
                 'aug-pcseg-2':'x', 'unc-aug-pcseg-2':'o', 'MADNESS':'*'}

red_triangle = mlines.Line2D([],[],color='tab:red',marker='v',ls='None')
blue_triangle = mlines.Line2D([],[],color='tab:blue',marker='v',ls='None')
green_x = mlines.Line2D([],[],color='tab:green',marker='x',ls='None')
purple_x = mlines.Line2D([],[],color='tab:purple',marker='x',ls='None')
orange_x = mlines.Line2D([],[],color='tab:orange',marker='x',ls='None')
cyan_o = mlines.Line2D([],[],color='tab:cyan',marker='o',ls='None')
brown_o = mlines.Line2D([],[],color='tab:brown',marker='o',ls='None')
olive_o = mlines.Line2D([],[],color='tab:olive',marker='o',ls='None')
brown_s = mlines.Line2D([],[],color='tab:brown',marker='s',ls='None')
olive_s = mlines.Line2D([],[],color='tab:olive',marker='s',ls='None')

default_handles = [red_triangle, blue_triangle,green_x,purple_x,orange_x,cyan_o,brown_o,olive_o,brown_s,olive_s]
default_labels = ['pcseg-1', 'pcseg-2', 'pcseg-3', 'aug-pcseg-1', 'aug-pcseg-2', 'unc-aug-pcseg-2']

nw = {}
mad = {}

# Parser for contracted HF NW
# Parsing all the .csv files in ../NWChem_Data
# (Should be 3, one per basis set)
for data_file in os.listdir("../NWChem_Data/"):
    if data_file.startswith("b3lyp") and data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nwfile:
            key = data_file[6:-4]
            if key in basis_sets:
               nw[key] = {}
               for line in nwfile:
                  if not line.startswith(","):
                       molname = line.strip().split(",")[0]
                       nw[key][molname] = float(line.strip().split(",")[1])

# Generic MADNESS parser
with open("../MAD_Data/mad_b3lyp_pm_new.csv", "r") as madfile:
    for line in madfile:
        if not line.startswith(","):
            mad[line.strip().split(",")[0]] = float(line.strip().split(",")[1])

# Reads in the number of electrons each molecule possesses
num_electrons = {}
with open("../NWChem_Data/num_electrons.txt","r") as input_file:
    for line in input_file:
        num_electrons[line.split()[0]] = int(line.split()[1])

# Want same ylim on each plot in each column
ylims = []

# Plot energy differences
fig, ax = plt.subplots(2,3,sharey=False, sharex=True, figsize=(9,6))

# To get axis right
a = fig.add_subplot(111, frameon=False)
plt.tick_params(labelcolor='none',top=False, bottom=False, left=False, right=False)
plt.grid(False)
plt.xlabel("$\mathrm{Number~of~Electrons}$",size='x-large')
plt.ylabel("$\Delta \mathrm{E}~~ \mathrm{(kcal/mol)}$",size='x-large')

energy_diff = {}
 
for j, basis in enumerate(basis_sets):
        
    energy_diff = {}
    for i, molecule in enumerate(nw[basis].keys()):
        value = (mad[molecule] - nw[basis][molecule])*-627.50956
        if(value < 0.0 or (value > 100 and basis == 'pcseg-3')):
           print(basis, molecule, "is being excluded do to some error. Investigate")
           continue
        energy_diff[molecule] = value 

    bfuncs = []
    electrons = [num_electrons[molecule] for molecule in energy_diff.keys()]

    if j <= 2:
        z = 0
    else:
        z = 1
    ax[z,j%3].set_title(basis)
    ax[z,j%3].plot(electrons, np.zeros(len(electrons)),color='black',ls='-')   
    ax[z,j%3].errorbar(electrons, [energy_diff[molecule] for molecule in energy_diff.keys()],
                color=basis_markers[basis],label=basis,marker=basis_styles[basis],ls='None')
    
    # Making axes match inside each column
    #if j <= 2:
    #   ylims.append(ax[z,j%3].get_ylim()) 
    #if j >2:
    #   # Check if current min lim is smaller
    #   if ax[z,j%3].get_ylim()[0] < ylims[j%3][0]:
    #      # Current is smaller, so save it 
    #      ylims[j%3] = [ax[z,j%3].get_ylim()[0], ylims[j%3][1]]
    #   # Check if current max lim is larger
    #   if ax[z,j%3].get_ylim()[1] > ylims[j%3][1]:
    #      # Current is smaller, so save it
    #      ylims[j%3] = [ylims[j%3][0], ax[z,j%3].get_ylim()[1]]

    #   # And set current ylims to what they should be
    #   ax[0,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
    #   ax[1,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
 

plt.tight_layout()
plt.savefig("b3lyp_jbasis_energies.pdf")
#plt.show() 

