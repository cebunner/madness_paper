import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib import rc
import os

# For uncontracted LDA
basis_sets = ['aug-cc-pvtz','aug-cc-pvqz', 'unc-aug-cc-pvtz','unc-aug-cc-pvqz']
basis_colors = dict(zip(basis_sets, ['teal','brown','gold']))

# Matplotlib formating things
rc('text', usetex=True)
basis_sets = ['pcseg-1', 'pcseg-2', 'pcseg-3', 'aug-pcseg-1', 'aug-pcseg-2', 'unc-aug-pcseg-2']
basis_markers = { 'pcseg-1':'tab:red', 'pcseg-2':'tab:blue', 'pcseg-3':'tab:green', 'aug-pcseg-1':'tab:purple', 
                 'aug-pcseg-2':'tab:orange', 'unc-aug-pcseg-2':'tab:cyan', 'MADNESS':'tab:red'}

basis_styles = { 'pcseg-1':'v', 'pcseg-2':'v', 'pcseg-3':'x', 'aug-pcseg-1':'x', 
                 'aug-pcseg-2':'x', 'unc-aug-pcseg-2':'o', 'MADNESS':'*'}

red_triangle = mlines.Line2D([],[],color='tab:red',marker='v',ls='None')
blue_triangle = mlines.Line2D([],[],color='tab:blue',marker='v',ls='None')
green_x = mlines.Line2D([],[],color='tab:green',marker='x',ls='None')
purple_x = mlines.Line2D([],[],color='tab:purple',marker='x',ls='None')
orange_x = mlines.Line2D([],[],color='tab:orange',marker='x',ls='None')
cyan_o = mlines.Line2D([],[],color='tab:cyan',marker='o',ls='None')
brown_o = mlines.Line2D([],[],color='tab:brown',marker='o',ls='None')
olive_o = mlines.Line2D([],[],color='tab:olive',marker='o',ls='None')
brown_s = mlines.Line2D([],[],color='tab:brown',marker='s',ls='None')
olive_s = mlines.Line2D([],[],color='tab:olive',marker='s',ls='None')

default_handles = [red_triangle, blue_triangle,green_x,purple_x,orange_x,cyan_o,brown_o,olive_o,brown_s,olive_s]
default_labels = ['pcseg-1', 'pcseg-2', 'pcseg-3', 'aug-pcseg-1', 'aug-pcseg-2', 'unc-aug-pcseg-2']

nw = {}
mad = {}

# All csv's have this format
# molecule, energy, iterations, dipole moment, eigenvalues, derivatives, derivative times, wall time

# Parser for contracted HF NW
# Parsing all the .csv files in ../NWChem_Data
# (Should be 3, one per basis set)
for data_file in os.listdir("../NWChem_Data/"):
    if data_file.startswith("b3lyp") and data_file.endswith(".csv"):
        with open("../NWChem_Data/" + data_file,"r") as nwfile:
            key = data_file[6:-4]
            nw[key] = {}
            if key in basis_sets:
               for line in nwfile:
                  if not line.startswith(","):
                       molname = line.strip().split(",")[0]
                       nw[key][molname] = float(line.strip().split(",")[3])

# Generic MADNESS parser
with open("../MAD_Data/mad_b3lyp_pm_new.csv", "r") as madfile:
    for line in madfile:
        if not line.startswith(","):
            mad[line.strip().split(",")[0]] = float(line.strip().split(",")[3])

# Want same ylim on each plot in each column
ylims = []

# Dipoles
fig, ax = plt.subplots(2,3,sharey=False, sharex=True, figsize=(9,6))
fig.text(0.35,0.02,r'Dipole Moment (Debye)',fontsize='xx-large',va='bottom')
fig.text(0.05,0.75,r'Dipole Difference (Debye)',fontsize='xx-large',rotation='vertical')

for j, basis in enumerate(basis_sets):
    
    dipole_diff = {}
    
    for molecule in nw[basis].keys():

        if (nw[basis][molecule] == -10000):
           print("{:24s} {:24s} {}".format(basis, molecule, "used quadratically convergent algorithm and has no dipole value, skipping"))
           continue

        if((molecule == "heptane" and (basis == 'pcseg-3' or basis == 'unc-aug-pcseg-2')) or 
           (molecule == "magnesium_hydroxide" and basis == 'aug-pcseg-1') or
           (molecule == "bicyclo[3.1.0]hexane" and (basis == 'aug-pcseg-1' or basis == 'unc-aug-pcseg-2'))):
           print("{:24s} {:24s} {}".format(basis, molecule, "nwchem converged to a different state, skipping"))
           continue

        value = mad[molecule] - nw[basis][molecule]
        
        if abs(value) > 0.1:
            print("{}\t{}\t{}\t{}".format('b3lyp', molecule,basis, value))
            print("{:16.8f} {:16.8f}\n".format(nw[basis][molecule], mad[molecule]))
        
        dipole_diff[molecule] = value

    dips = np.abs([mad[molecule] for molecule in dipole_diff.keys()])
        
    if j <= 2:
        z = 0
    else:
        z = 1
        
    plt.setp(ax[z,j%3].get_yticklabels(), fontsize=6)
        
    ax[z,j%3].set_title(basis)
    ax[z,j%3].plot(dips, np.zeros(len(dips)),color='black',ls='-')   
    ax[z,j%3].errorbar(dips, [dipole_diff[molecule] for molecule in dipole_diff.keys()],
                color=basis_markers[basis],label=basis,marker=basis_styles[basis],ls='None')

    # Making axes match inside each column
    if j <= 2:
       ylims.append(ax[z,j%3].get_ylim()) 
    if j >2:
       # Check if current min lim is smaller
       if ax[z,j%3].get_ylim()[0] < ylims[j%3][0]:
          # Current is smaller, so save it 
          ylims[j%3] = [ax[z,j%3].get_ylim()[0], ylims[j%3][1]]
       # Check if current max lim is larger
       if ax[z,j%3].get_ylim()[1] > ylims[j%3][1]:
          # Current is smaller, so save it
          ylims[j%3] = [ylims[j%3][0], ax[z,j%3].get_ylim()[1]]

       # And set current ylims to what they should be
       ax[0,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
       ax[1,j%3].set_ylim([ylims[j%3][0]*1.1, ylims[j%3][1]*1.1])
    
plt.savefig("b3lyp_jbasis_dipoles.pdf")
#plt.show()
