#
#   Script goes through the database and finds all molecules containing 
#   only carbon and hydrogen.
#

molecule_list = open("../molecule_database.txt", "r")
output = open("hydrocarbons_list.txt", "w")

# List of atoms in molecules in set excluding carbon and hydrogen
atoms = ['Al', 'Ar', 'Be', 'B', 'Br', 'Cl', 'F', 'Li', 'Mg', 'Na', 'Ne', 
         'N', 'O', 'P', 'Si', 'S']

for line in molecule_list:
   if not line.startswith("#"):
      molecule = line.split()[2]

      flag = True

      for i in range(len(molecule)):
         # String to hold atom symbols
         entry = molecule[i]
        
         # Need to see if next letter is lowercase
         # and if it is include it in the comparison
         if i+1 < len(molecule) and molecule[i+1].islower():
            entry += molecule[i+1]
            i = i + 1

         if entry in atoms:
            flag = False 

      if flag and 'C' in molecule and 'H' in molecule:
         output.write(molecule + "\n")

molecule_list.close()
output.close()
