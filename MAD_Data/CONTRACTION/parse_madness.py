 #
 #   This python script will parse all the files ending with .out in a given directory,
 #   assuming they are MADNESS output files, and will create a .csv of the relevant 
 #   relevant information. 
 #
 #    Information pulled out: 
 #       Energy          - total energy
 #       Iterations      - number of iterations for completion (not really relevant here but being consistent.)
 #       Dipole moment   - dipole moment tensor
 #       Eigenvalues     - orbital energies (only pulling out alpha values)
 #       Derivatives     - tensor of derivative of energy gradients 
 #       Derivative time - time spent calculating derivatives
 #       Wall time       - total wall time
 #
 #
 #    To use: python parse_nwchem.py directory_with_output_files/
 #
 #    NOTE: Relative or absolute paths seem ok after minimal testing
 #    NOTE: Output will be printed to terminal, so you must pipe it to a file to save it.
 #

import os
import sys
import math

# Get directory with outputs from command line
mydir = sys.argv[1]

# Print an output header
print("{},{},{},{},{},{},{},{}".format("", "Energy", "Iterations", "Dipole Moment", "Eigens", "Derivatives", "Derivative Time", "Wall Time"))

# For each file in that directory...
for file in os.listdir(mydir):

   # Open the file
   reader = open(mydir + "/" + file, "r")

   # Initialize variables
   energy = 0.0
   dipole = ''
   wall_time = 0.0
   iterations = 0
   derivatives = []
   derivative_time = 0.0
   eigenvalues = []

   #go through each line in the MADNESS ouput file of each molecule
   for line in reader:
   
   	# Split line on white space
   	line = line.split()       
    
   	# Only do stuff if line isn't empty
   	if line:
   		
   		# get all the total values in the files
   		if line[0] ==  "total" and line[1] != "charge":
   			energy = float(line[1])	
   		elif line[0] == 'alpha' and line[1] == 'eigenvalues':
   			line = reader.readline().split()	
   			eigenvalues = line[1:] # Getting rid of [*]
   			for i in range(len(eigenvalues)):
   				eigenvalues[i] = float(eigenvalues[i])
   		elif line[0] == "Total" and line[1] == "Dipole":	
   			dipole = float(line[3])
   		elif line[0] == 'timer:' and line[1] == 'derivatives':
   			derivative_time = float(line[2][:-1]) # The [:-1] gets rid of "s" on the end	
   		elif line[0] == 'atom' and line[1] == "x":
   			line = reader.readline()
   			line = reader.readline().split()
   			while line[0] != 'timer:':	
   				derivatives.append(float(line[4])) # x	
   				derivatives.append(float(line[5])) # y
   				derivatives.append(float(line[6])) # z
   				line = reader.readline().split()
   		elif line[0] == "Total" and line[1] == 'wall':			
   			wall_time = float(line[3][:-1]) # The [:-1] gets rid of the "s" on the end
   	
   		elif line[0] == "Iteration":
   			iterations += 1			


   # Always clean up
   reader.close()

   # Print the data
   print("{},{},{},{},\"{}\",\"{}\",{},{}".format(file[:-4], energy, iterations, dipole, eigenvalues, derivatives, derivative_time, wall_time))
