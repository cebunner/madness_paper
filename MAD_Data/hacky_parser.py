import os, sys
import numpy as np
import pandas as pd

# Directories on SeaWulf
#output_directory = "/gpfs/projects/rjh/Paper/MAD_Data/outputs_LDA_boys"
#data_directory = "/gpfs/projects/rjh/Paper/MAD_Data/DATA"
#geometry_directory = "/gpfs/projects/rjh/NWCHEM_BENCHMARK/geometries"

# Directories on Colin's PC
output_directory = "/home/cbunner/madness_paper/MAD_Data/outputs_LDA_boys"
data_directory = "/home/cbunner/madness_paper/MAD_Data/DATA"
geometry_directory = "/home/cbunner/madness_paper/SI/geometries"

#This dictionary will hold all of my data dictionaries. It's kinda like locals(), but with an extra step.
temp_namespace =  {}

### Read in the number of alpha and beta electrons to a dictionary for easy access. These are needed for grabbing the HOMO and LUMO. ###
alpha_electrons = {}
beta_electrons = {}

with open("/gpfs/projects/rjh/Paper/MAD_Data/alphabeta.txt","r") as ab:
    for i, line in enumerate(ab):
        if i != 0:
            spl = line.split()
            alpha_electrons[spl[0]] = int(spl[1])
            beta_electrons[spl[0]] = int(spl[2])                       
########################################################################################################################################

### This function cleans up energies extracted from the final vector analysis so that I can get the HOMO and LUMO. ###
def vector_clean(string):
    #Get rid of E= prefix
    homo = string.replace("E=","")
    #Record index of exponent indicator 'D'
    exp_index = homo.index("D")
    #Grab the exponent and store it as an int
    exp = int(homo[exp_index+1:])
    #Remove the exponent part
    homo = float(homo[:exp_index])
    #Return the base times 10^exp
    return homo*(10**exp)
######################################################################################################################

dipole_dictionary = {}
total_energy_dictionary = {}
energy_timing_dictionary = {}
gradients_dictionary = {}
closed_homo_dictionary = {}
open_alpha_homo_dictionary = {}
open_beta_homo_dictionary = {}
iter_dictionary = {}

#Read the output for all of the molecules in the current functional/basis directory
for file_out in os.listdir(output_directory):

    #Remove the .out file extension
    molecule_name = file_out.replace(".out","")

    file_out.replace("[",r"\[").replace("]",r"\]")

    if molecule_name.startswith("decane"):
        continue

    else:
        #Make a copy of the file stored in list format with no newline characters. This allows me to grab slices of the
        #file easily when multiple lines are correlated.
        #
        #Note: fcp is the array of lines we iterate over
        with open("{}/{}".format(output_directory,file_out),'r') as fi:
            fcp = [line.rstrip('\n') for line in fi]

        #Write the geometry to an array. I only do this so I can count the number of atoms in molecule, which is necessary
        #to grab the entire gradient output
        with open("{}/{}".format(geometry_directory,file_out.replace(".out",""))) as f:
            geom = [line for line in f]
            #Number of atoms in the molecule. See above.
            natom = len(geom)

            #Number of alpha and beta electrons in the molecule
            nalpha = alpha_electrons[molecule_name]
            nbeta = beta_electrons[molecule_name]

            #Boolean determining whether or not I have to calculate alpha and beta homos separately
            if nalpha-nbeta == 0:
                open_shell = False
            else:
                open_shell = True

            #This a really ad hoc variable needed to grab the appropriate alpha and beta HOMOs/LUMOs in ODFT calculations
            iteration = 0

            #Just to confuse the situation even more, this variable will hold the total number of iterations in the calculation
            max_iter = 0 

            #Start iterating over the lines in the file to find the information we want.
            for i, line in enumerate(fcp):

                #Get the total energy. Note that this will grab the total energy every time it is printed, but since
                #the entire file is being read, the last value (which is the value we want) will be recorded.
                if line.strip().startswith("total") and fcp[i-1].strip().startswith("nuclear-repulsion"):
                    try:
                        total_energy_dictionary[molecule_name] = float(line.strip().split()[1])
                    except:
                        print(line.split()[1])

                #Get the dipoles in Debyes.
                elif line.strip().startswith("Total Dipole Moment"):
                    dipole_dictionary[molecule_name] = float(line.strip().split()[3])

                #Get the total iterative time for the DFT Energy
                elif line.strip().startswith("Total wall time"):
                    try:
                        energy_timing_dictionary[molecule_name] = float(line.split()[3].replace("s",""))
                    except:
                        print(line.split()[3].replace("s",""))
                   
                #Find the total number of iterations
                elif line.strip().startswith("Iteration"):
                     max_iter += 1


            iter_dictionary[molecule_name] = max_iter

#I just use the dataframes for the .to_string() method
dipoles = pd.DataFrame.from_dict(dipole_dictionary,orient='index')
energies = pd.DataFrame.from_dict(total_energy_dictionary,orient='index')
energy_timings = pd.DataFrame.from_dict(energy_timing_dictionary,orient='index')
iters = pd.DataFrame.from_dict(iter_dictionary,orient='index')

#Write the dipoles to a text file
with open("{}/{}_dipoles.txt".format(data_directory,"LDA_BOYS"),"w") as f:
    f.write("Dipole moment (Debye) calculated using LDA in MADNESS")
    f.write(dipoles.to_string())

#Write the total energies to a text file
with open("{}/{}_total_energies.txt".format(data_directory,"LDA_BOYS"),"w") as g:
    g.write("Total DFT energy using LDA in MADNESS")
    g.write(energies.to_string())

#Write total iterative times for the DFT energy to a text file
with open("{}/{}_dftenergy_times.txt".format(data_directory,"LDA_BOYS"),"w") as h:
    h.write("Total wall times for the calculation of total energies with MADNESS")
    h.write(energy_timings.to_string())

#Write the number of iterations to a file
with open("{}/{}_num_iterations.txt".format(data_directory,"LDA_BOYS"),'w') as f:
    f.write("Total number of iterations required to reach self-consistency with"
      "MADNESS and the LDA approximation")
    f.write(iters.to_string())

