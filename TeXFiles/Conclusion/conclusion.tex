\section{Conclusions} \label{sec:conclusion}

From our systematic comparison of converged MW and Gaussian basis set
results in several density functionals we draw several main
conclusions, most of which are already represented in the literature
but are not routinely reflected in widely-used computational
protocols.
\begin{itemize}
\item Errors in atomic and molecular total energies are signficantly
  correlated with errors in chemical energy differences due to
  imperfect cancellation of systematic errors, inadequate polarization
  of atoms in molecules, and basis set superposition error.

\item For first and second period atoms (H-Ne), widely-used families
  of basis sets are capable of systematically approaching the conveged
  numerical results for chemical reactions.  For hydrocarbons the
  cc-pvtz basis reduces the mean, median, standard deviation, and
  maximum deviation well below 1 kcal/mole.  For other atoms
  augmentation with diffuse functions is necessary.

\item For heavier atoms in widely-used basis set families, there
  remain outlier reactions with errors of 10 kcal/mol or much more that are
  not remedied by uncontracting.  This incompleteness in the
  primitive set is in part associated with inadequate polarization of the
  inner valence or outer core states as already identified by Peterson
  and collaborators \cite{Dunning2001,Nicklass1998}.  To reduce the
  errors of outliers close to 1 kcal/mol it is necessary to employ
  augmented, polarized, quadruple-zeta basis sets with core polarization (Table
  \ref{table:abtoc}).  However, such large basis sets are very rarely
  employed in DFT calculations, and commonly-used DZP-quality basis
  sets have outliers with errors over 100 kcal/mol.  

\item A corollary to the preceding point is that estimating basis set
  error for molecules with heavy atoms by extrapolating results in
  standard basis set families without core polarization (e.g., the
  aug-pvNz with N=d,t,q,$\ldots$) can suggest convergence to the basis
  set limit but in actuality produce incorrect values.

\item The importance of model chemistries defined by both DFT
  functional+basis (and any other parameters controlling error or
  reproducibility) is emphasized by the complexity and expense of
  reliably reducing errors in Gaussian basis set reaction energies to
  below chemical accuracy (1 kcal/mole), and the presence of
  significant outliers in even very large basis sets.  I.e., it is
  insufficient to characterize a calculation by just identifying the
  exchange-correlation functional used because of the size of errors
  from all but the largest Gaussian basis that include semi-core
  poloarization.  Such model chemistries are relevant to many chemical
  applications, and can extract quantative
  results through carefully engineered cancellation of error for
  instance in the prediction of trends (e.g., differences in
  reaction energies, $\Delta \Delta G$) or in application to
  homodesomtic reactions \cite{Hehre-1970,Wheeler-2012} .

\item A positive observation is the apparent transferability (for this
  set of test molecules, reactions, and functionals) of basis sets between DFT
  functionals when computing reaction energies.  Uncontracting or
  recontracting a basis for a given DFT functional can reduce error,
  but the residual error in the primitive set is typically seen to be larger
  than the contraction error.

\item The multiwavelet basis provides a practical approach to reliable
  and direct computation of numerically-converged chemistry that for
  large molecules is both faster and more accurate than conventional
  Gaussian methods.  However, Gaussian basis sets retain significant
  advantages at lower accuracy, or for smaller systems, or for
  dynamics, or for post-HF methods. This is due to more systematic
  cancellation of errors in computing chemical energy differences in
  atom-centered basis sets, and to it being hard in the multiwavelet
  basis to define a set of virtual orbitals as a basis for many-body
  models.
  
\end{itemize}

To facilitate reproducibility and use of this highly-curated data set
in future studies, the SI includes all input geometries, input and
output files, associated Python scripts that manipulated data or
produced the plots, and files with extracted data in CSV format that
is suitable for automated processing or importing into spreadsheets.
