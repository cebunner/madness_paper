
209                 27                1 acetylene                     1 heptane                       9 methylene_s                   1135.3804284        1142.1613978        6.7809693409        

387                 33                1 acetylene                     1 nonane                        11 methylene_s                  1405.8079797        1413.9907331        8.1827533972        

410                 30                1 acetylene                     1 octane                        10 methylene_s                  1270.1226059        1277.5959651        7.4733591702        

528                 30                1 acetylene                     2 tert_butyl_radical            10 methylene_s                  1182.7206675        1189.7361656        7.0154981169        

666                 27                1 allene                        1 hexane                        9 methylene_s                   1146.4684309        1153.1599315        6.6915005961        

684                 30                1 allene                        1 heptane                       10 methylene_s                  1281.7950012        1289.3714734        7.5764721525        

754                 27                1 allene                        2 isopropyl_radical             9 methylene_s                   1048.4166544        1054.6118745        6.1952201054        

831                 36                1 allene                        1 nonane                        12 methylene_s                  1552.2225525        1561.2008087        8.9782562089        

847                 33                1 allene                        1 octane                        11 methylene_s                  1416.5371788        1424.8060407        8.2688619819        

943                 33                1 allene                        2 tert_butyl_radical            11 methylene_s                  1329.1352403        1336.9462412        7.8110009286        

1077                30                1 bicyclo[1.1.0]butane          1 hexane                        10 methylene_s                  1276.8875295        1284.2814668        7.3939373333        

1090                33                1 bicyclo[1.1.0]butane          1 heptane                       11 methylene_s                  1412.2140998        1420.4930087        8.2789088898        

1144                30                1 bicyclo[1.1.0]butane          2 isopropyl_radical             10 methylene_s                  1178.8357530        1185.7334098        6.8976568426        

1207                39                1 bicyclo[1.1.0]butane          1 nonane                        13 methylene_s                  1682.6416511        1692.3223440        9.6806929460        

1222                36                1 bicyclo[1.1.0]butane          1 octane                        12 methylene_s                  1546.9562773        1555.9275760        8.9712987191        

1237                27                1 bicyclo[1.1.0]butane          1 pentane                       9 methylene_s                   1141.6359653        1148.2552807        6.6193154001        

1275                36                1 bicyclo[1.1.0]butane          2 tert_butyl_radical            12 methylene_s                  1459.5543389        1468.0677765        8.5134376658        

1567                27                1 cyclobutane                   1 cyclopentane                  9 methylene_s                   1174.6618831        1181.6008057        6.9389225361        

1644                30                1 cyclobutene                   1 hexane                        10 methylene_s                  1287.4127065        1294.9481094        7.5354028538        

1657                33                1 cyclobutene                   1 heptane                       11 methylene_s                  1422.7392769        1431.1596513        8.4203744102        

1711                30                1 cyclobutene                   2 isopropyl_radical             10 methylene_s                  1189.3609301        1196.4000524        7.0391223630        

1774                39                1 cyclobutene                   1 nonane                        13 methylene_s                  1693.1668281        1702.9889866        9.8221584665        

1789                36                1 cyclobutene                   1 octane                        12 methylene_s                  1557.4814544        1566.5942186        9.1127642395        

1804                27                1 cyclobutene                   1 pentane                       9 methylene_s                   1152.1611423        1158.9219233        6.7607809205        

1842                36                1 cyclobutene                   2 tert_butyl_radical            12 methylene_s                  1470.0795160        1478.7344191        8.6549031862        

2040                30                1 butane                        1 bicyclo[3.1.0]hexane          10 methylene_s                  1314.6938988        1322.3380569        7.6441581742        

2055                27                1 butane                        1 spiropentane                  9 methylene_s                   1147.9168534        1154.6400222        6.7231687427        

2333                27                1 cyclopropene                  1 hexane                        9 methylene_s                   1126.0862169        1132.7787883        6.6925713649        

2351                30                1 cyclopropene                  1 heptane                       10 methylene_s                  1261.4127873        1268.9903302        7.5775429213        

2421                27                1 cyclopropene                  2 isopropyl_radical             9 methylene_s                   1028.0344405        1034.2307313        6.1962908742        

2498                36                1 cyclopropene                  1 nonane                        12 methylene_s                  1531.8403385        1540.8196655        8.9793269777        

2514                33                1 cyclopropene                  1 octane                        11 methylene_s                  1396.1549648        1404.4248975        8.2699327507        

2610                33                1 cyclopropene                  2 tert_butyl_radical            11 methylene_s                  1308.7530263        1316.5650980        7.8120716974        

2674                30                1 2_butyne                      1 hexane                        10 methylene_s                  1284.1575915        1291.6874038        7.5298123616        

2687                33                1 2_butyne                      1 heptane                       11 methylene_s                  1419.4841618        1427.8989457        8.4147839180        

2741                30                1 2_butyne                      2 isopropyl_radical             10 methylene_s                  1186.1058150        1193.1393469        7.0335318709        

2804                39                1 2_butyne                      1 nonane                        13 methylene_s                  1689.9117131        1699.7282810        9.8165679744        

2819                36                1 2_butyne                      1 octane                        12 methylene_s                  1554.2263393        1563.3335131        9.1071737474        

2834                27                1 2_butyne                      1 pentane                       9 methylene_s                   1148.9060272        1155.6612177        6.7551904284        

2872                36                1 2_butyne                      2 tert_butyl_radical            12 methylene_s                  1466.8244009        1475.4737136        8.6493126941        

3316                30                2 ethyl_radical                 1 bicyclo[3.1.0]hexane          10 methylene_s                  1206.6911411        1213.6923830        7.0012419753        

3329                27                2 ethyl_radical                 1 spiropentane                  9 methylene_s                   1039.9140957        1045.9943483        6.0802525437        

3636                30                1 hexane                        1 methylenecyclopropane         10 methylene_s                  1280.3586457        1287.8031868        7.4445410773        

3671                27                1 hexane                        1 propyne                       9 methylene_s                   1142.8335850        1149.4795785        6.6459934784        

3694                36                1 hexane                        1 bicyclo[3.1.0]hexane          12 methylene_s                  1584.9419134        1594.0987817        9.1568682722        

3706                33                1 hexane                        1 spiropentane                  11 methylene_s                  1418.1648681        1426.4007469        8.2358788406        

3720                30                1 hexane                        2 vinyl_radical                 10 methylene_s                  1156.6820121        1163.7298823        7.0478702370        

3769                27                1 heptane                       2 methlidyne_radical            9 methylene_s                   860.36955150        866.73799982        6.3684483122        

3782                33                1 heptane                       1 methylenecyclopropane         11 methylene_s                  1415.6852161        1424.0147287        8.3295126337        

3819                30                1 heptane                       1 propyne                       10 methylene_s                  1278.1601554        1285.6911204        7.5309650348        

3839                39                1 heptane                       1 bicyclo[3.1.0]hexane          13 methylene_s                  1720.2684838        1730.3103236        10.041839828        

3849                36                1 heptane                       1 spiropentane                  12 methylene_s                  1553.4914384        1562.6122888        9.1208503971        

3861                33                1 heptane                       2 vinyl_radical                 11 methylene_s                  1292.0085825        1299.9414243        7.9328417933        

3997                30                1 isobutane                     1 bicyclo[3.1.0]hexane          10 methylene_s                  1316.4037541        1324.0258806        7.6221264914        

4012                27                1 isobutane                     1 spiropentane                  9 methylene_s                   1149.6267088        1156.3278458        6.7011370598        

4092                30                2 isopropyl_radical             1 methylenecyclopropane         10 methylene_s                  1182.3068692        1189.2551298        6.9482605865        

4127                27                2 isopropyl_radical             1 propyne                       9 methylene_s                   1044.7818085        1050.9315215        6.1497129877        

4149                36                2 isopropyl_radical             1 bicyclo[3.1.0]hexane          12 methylene_s                  1486.8901369        1495.5507247        8.6605877814        

4159                33                2 isopropyl_radical             1 spiropentane                  11 methylene_s                  1320.1130916        1327.8526899        7.7395983499        

4279                27                1 isobutylene                   1 cyclopentane                  9 methylene_s                   1179.2875595        1186.0207988        6.7332393215        

4432                29                1 methane                       8 methylene_s                   1 nonane                        -1079.793855        -1085.927556        6.1337014862        

4584                33                2 methlidyne_radical            1 nonane                        11 methylene_s                  1130.7971027        1138.5673351        7.7702323685        

4605                30                2 methlidyne_radical            1 octane                        10 methylene_s                  995.11172901        1002.1725671        7.0608381415        

4805                39                1 methylenecyclopropane         1 nonane                        13 methylene_s                  1686.1127673        1695.8440640        9.7312966900        

4820                36                1 methylenecyclopropane         1 octane                        12 methylene_s                  1550.4273936        1559.4492960        9.0219024631        

4835                27                1 methylenecyclopropane         1 pentane                       9 methylene_s                   1145.1070815        1151.7770006        6.6699191441        

4873                36                1 methylenecyclopropane         2 tert_butyl_radical            12 methylene_s                  1463.0254551        1471.5894965        8.5640414097        

5090                36                1 nonane                        1 propyne                       12 methylene_s                  1548.5877066        1557.5204557        8.9327490911        

5101                45                1 nonane                        1 bicyclo[3.1.0]hexane          15 methylene_s                  1990.6960350        2002.1396589        11.443623884        

5109                42                1 nonane                        1 spiropentane                  14 methylene_s                  1823.9189897        1834.4416241        10.522634453        

5118                39                1 nonane                        2 vinyl_radical                 13 methylene_s                  1562.4361337        1571.7707595        9.3346258497        

5160                30                1 ozone                         9 methylene_s                   3 acetone                       -1331.687808        -1338.794269        7.1064609801        

5198                33                1 octane                        1 propyne                       11 methylene_s                  1412.9023329        1421.1256877        8.2233548642        

5212                42                1 octane                        1 bicyclo[3.1.0]hexane          14 methylene_s                  1855.0106613        1865.7448909        10.734229657        

5221                39                1 octane                        1 spiropentane                  13 methylene_s                  1688.2336159        1698.0468562        9.8132402264        

5232                36                1 octane                        2 vinyl_radical                 12 methylene_s                  1426.7507600        1435.3759916        8.6252316227        

5294                33                1 pentane                       1 bicyclo[3.1.0]hexane          11 methylene_s                  1449.6903492        1458.0725955        8.3822463389        

5308                30                1 pentane                       1 spiropentane                  10 methylene_s                  1282.9133039        1290.3745608        7.4612569074        

5326                27                1 pentane                       2 vinyl_radical                 9 methylene_s                   1021.4304479        1027.7036962        6.2732483037        

5348                40                1 phosphorus_tetramer           6 phosphorus_pentafluoride      10 phosphorus_trifluoride       48.302731178        55.906811926        7.6040807475        

5421                27                1 propane                       1 bicyclo[3.1.0]hexane          9 methylene_s                   1179.0337886        1185.8976854        6.8638968735        

5557                33                1 propyne                       2 tert_butyl_radical            11 methylene_s                  1325.5003944        1333.2658882        7.7654938108        

5638                42                1 bicyclo[3.1.0]hexane          2 tert_butyl_radical            14 methylene_s                  1767.6087228        1777.8850914        10.276368604        

5647                39                1 spiropentane                  2 tert_butyl_radical            13 methylene_s                  1600.8316775        1610.1870567        9.3553791730        

5685                30                1 cyclopentane                  1 cyclopentane                  10 methylene_s                  1330.2882316        1337.9988193        7.7105877121