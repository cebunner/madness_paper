\section{Introduction} \label{sec:introduction}

Motivated by recent studies and discussion
\cite{jsflhb-2017,jensen-2017,fd-2018}, we revisit the errors present in
linear-combination of atomic orbital (LCAO) calculations using
standard atom-centered Gaussian basis sets at the Hartree-Fock (HF)
and density functional theory (DFT) levels of theory.  Converged HF
calculations are an essential foundation for subsequent accurate
many-body studies, and a detailed understanding of basis set error is
essential to obtain the full predictive power from DFT calculations in
finite basis sets using approximate exchange correlation functionals.
The authors extenstively use both Gaussian and \cite{boys-1950} and
multiwavelet (MW) \cite{hfygb-2004} basis sets, and our perspective is that
there is no perfect basis --- it is simply a matter of picking the
right tool for the right job. The main purpose of the paper is to
provide and examine data to inform such decisions, which includes
computational cost as well as accuracy and the nature of errors.  We
advance beyond previous studies by extending the test set to include
heavier atoms, by probing the origin and nature of errors in LCAO calculations
including transferability between HF and different DFT functionals, by
examining errors in reaction energies and by reporting
computational times for the multiwavelet calculations.  Since we
anticipate this heavily curated data set to be a valuable community
resource, in the supplementary data we provide spread sheets
summarizing the results as well as the full set of scripts and raw
output files.

In prior work, we \cite{yfghb-2004,yhh-2005,yfbh-2015,khb-2015} and others
\cite{jfjmrf-2016,syh-2011,rcgt-2016}, have establsihed multiresolution analysis
(MRA) with multiwavelets as a practical tool for computing ground and
excited state properties with guaranteed accuracy, regardless of the
physics.  Convergence of MW results to sub-micro-Hartreee accuracy
for both HF and DFT has already been established for atoms and small
molecules \cite{yfghb-2004-121,yfghb-2004,jfjmrf-2016}, and 
demonstrated for small atoms and molecules in atto-second and
intensely ionizing IR laser pulses
\cite{vhk-2012}.  The guarantee of
accuracy \cite{abgv-2002} arises from dynamic adaptive refinement of the underling
multiwavelet basis to meet the requested accuracy of
computation. Realizing this in practice requires careful analysis and
implementation, with subsequent verification, which includes
comparison with both analytic results as well as converged data from
other computational methods, including Gaussian LCAO expansions.

The dynamic mesh refinement/coarsening introduces numerical errors
(basis incompleteness or truncation error) that are not correlated
betweeen independent calculations, and hence do not cancel in taking
chemical energy differences.  Thus, total MW energies must be computed
to the desired accuracy of any energy differences (or response theory
used to obtain differences directly), which becomes a burden for very
large systems or systems with heavy atoms unless effective-core or
pseudo-potentials are employed (all computations in this paper are all
electron).  A final source of error is regularizing the singularity of
the point nuclear charge through the use of a smooth potential
\cite{hfygb-2004} for which an estimate from perturbation theory provides
precise control over the error introduced.  Note that this
error is systematic and largely cancels in taking energy
differences.  These errors are examined in more detail in Section \ref{sec:methods}.

Increased accuracy comes with additional computational cost,
which is another limit on the accuracy attainable in practice.
However, key computational steps, such as the computation of the
Coulomb potential or orbital update scale exactly linearly in the
number of significant coefficients, and other steps such as
computation of the kinetic and potential energy matrices or
application of the Hartree-Fock exchange operator \cite{yfghb-2004}
have an effective sub-quadratic scaling with system size in localized
orbitals.  These properties hold at any finite accuracy of
computation, which is in sharp contrast to Gaussian LCAO approaches.
The introduction of polarization and especially diffuse functions into
a Gaussian basis set greatly increases the cost of computation, with
the matrix representation of many operators (e.g., the Fock operator
or the density matrix) becoming effectively dense and the condition
number of the overlap matrix becoming large.  These two problems
conspire to make it very hard to translate to large molecues the 
success of Gaussian methods in perfoming demonstrably-converged
calculations on small and medium-sized molecules.

Part of the computational advantage of MW methods for HF and DFT is
that they only compute with the occupied orbitals.  It is possible to
solve for a few virtual orbitals assuming that they are bound,
which is usually not the case for HF.  Other operators can define more
strongly localized virtual orbitals such as used in previous numerical
studies \cite{ab-1987}, but it is clear MW do not provide an
ideal framework for defining a large set of virtual orbitals for
subsequent many-body computations, and that Gaussians retain a strong
advantage in this respect.  Recent work by Valeev
\cite{ppprnv-2017} has demonstrated use of the same C++ software to
solve for PNOs using either Gaussian or MRA approaches.

Computation of forces in the MW basis \cite{yfghb-2004-121} is efficient 
due to the effectively complete basis, however, numerical noise in the 
density near the nucleus makes it hard to attain high accuracy.  Indeed, 
this (along with speed) is the main reason the default MADNESS smoothing 
parameter is set to 10\textsuperscript{-4}\Eh\ rather than smaller, since 
the smoother potential amplifies noise less.


%% THIS WILL HAVE TO BE IN THE FINAL PAPER
%% Disuss prior papers --- both here and in the analysis section --- which ones?
%% \begin{itemize}
%% \item Frank Jensen --- JPCA --- need to use basis sets optimized for DFT (we agree!), Gaussian basis sets can deliver basis set error well below chemical accuracy (we agree for small-medium molecules), and there no fundamental limitations in designing higher pc-n basis sets (we disagree --- cost of computation and large molecules), LDA error comparison for atoms (his fig 1), use of extrapolation, superior convergence of pc-seg basis set relative to cc, basis design involves both primitive set and contraction style (our numbers show there are limitations in pc-n), 
%% \item dixon paper
%% \item stig paper
%% \item other paper?
%% \item https://pubs.acs.org/doi/full/10.1021/acs.jctc.7b00140
%%   \item just putting this here so I don't lose it \verb+https://th.fhi-berlin.mpg.de/sitesub/meetings/dft-workshop-2016/uploads/Meeting/May_11_Schneider.pdf+
%%   \item ditto --- http://fizyka.umk.pl/~jkob/publications.html
%% \end{itemize}

In the following, we adopt much the same approach as \cite{jsflhb-2017} by
identifying a test suite of molecules and computing energies and other
properties using the effectively exact MW approach and using a
wide range of contracted Gaussian basis sets for HF and several
commonly-used density functionals.  We examine results in uncontracted
primitive sets to explore the origin of errors and the transferability
of basis sets from one functional to another.  We conclude by examining
errors in a large set of reactions constructed from the test set, and 
discussing the timing of the MW calculations.


