\section{Methods} \label{sec:methods}

Density functional theory and Hartree-Fock calculations were carried
out in both the multi-wavelet basis of MADNESS (\#06572b98) and a progression of spherical bases in NWChem (version 6.6).
The functionals used were SVWN5(LDA)\cite{svwn}, B3LYP\cite{B3LYP},
and PBE0\cite{PBE0}.  The basis sets used in NWChem were 3-21G\cite{bph-1980},
6-31G*\cite{hp-1973}, cc-pvXz\cite{dunning-1989}, cc-pcvXz\cite{wd-1993}, 
aug-cc-pvXz\cite{dunning-1989}, and aug-cc-pcvXz\cite{wd-1993}  
where X = D,T,Q as well as the pc-segY\cite{jensen-2014} series for 
Y = 1,2,3 and aug-pc-segZ\cite{jensen-2014} for Z=1,2.  To explore the
effect of contraction, both contracted and uncontracted basis sets
were used.  All geometries were taken from the
SI of \cite{jsflhb-2017} or from the NIST database\cite{NIST} (see
Supporting Information for complete listing).  All calculations for
which a time is reported were run on a single compute node of the
Seawulf cluster, consisting of 2 Intel Xeon CPU E5-2683 v3 @ 2.00 GHz
with 28 cores total and 128GB of memory.

NWChem calculations were performed using 26 MPI processes, and NWChem seperately uses a communication thread for Global Arrays.
Open shell calculations were spin and symmetry unrestricted, and all integrals were computed on the fly using the \verb+direct+ keyword; otherwise default
paramters were used except as needed to obtain convergence to the lowest energy state. Input files and job scripts for both MADNESS and NWChem can be found 
in the supporting information.

MADNESS calculations were run using 2 MPI processes, each bound to a different socket, and 11 threads per process. 
Four molecules (bromoform, carbon tetrabromide, nitrogen tribromide, and phosphorous tribromide) required more memory for MADNESS. 
They were run on two nodes with the same number of MPI process and threads, with one MPI process per node.
Default parameters were used for everything other than smoothing parameter for the nuclear potential 
that was set to introduce a maximum error in the energy of 10\textsuperscript{-6}\ \Eh/atom.

There are three sources of error in our MW
results (1) smoothing of the nuclear charge, (2) convergence
error in the wave function with the error in the energy being
quadratic in this error, as is also the case for LCAO, and (3)
truncation error in the numerical basis that is controlled by a single
input parameter ($\epsilon$ in the paper body).  In Table
\ref{table:mraconv} we provide representative results demonstrating (1) numerical
convergence for atoms and small molecules relevant to this study, (2)
that the parameters used in this study (the {\em default} parameters for MADNESS
with smoothing errors reduced to 10\textsuperscript{-6}\ \Eh)
deliver better than 10\textsuperscript{-4}\ \Eh/atom error in total
energies, and (3) that chemical energy differences are
obtained to 0.02 kcal/mol or better.

There were a few instances when open-shell molecules converged to different states with MADNESS and NWChem.  After ensuring that the NWChem
solution was the lowest energy state, MADNESS was given the converged NWChem wave-function in the aug-cc-pvqz basis as a starting guess.
These data were used in the accuracy comparisons only.

Substantial effort was invested in validating the calculations.  An initial check was that the MW energy be lowest, and subsequently the energies of the
highest occupied molecular orbital (HOMO) were compared, with roughly 3 significant figure agreement anticipated for the aug-cc-pvqz basis.  Subsequently, outliers
in errors or trends were examined.  The most problems were observed with open shell systems with symmetry in diffuse basis sets, for which we commonly
disabled symmetry and sought the lowest energy occupation pattern by first converging a closed-shell ion and adding electrons back one at a time.

\begin{table}[p]
  \small
  \begin{center}    
    \begin{tabular}{|l|S|S|S|S|}

      \multicolumn{5}{c}{} \\ \hline
    
    \multicolumn{5}{|l|}{\bf Total energies (\Eh )} \\ \hline

 {\bf System}        &  {\bf Default}   &  {\bf Current}  & {\bf High}   & {\bf Reference} \\ \hline
H                    &    -0.4999535    &     -0.4999995  &   -0.5000000 &   -0.5 \\
Be                   &   -14.5729375    &    -14.5730221  &  -14.5730232 &    -14.5730232 \\    %\textsuperscript{a} \\
C                    &   -37.6936480    &    -37.6937390  &  -37.6937405 &    \\ %-37.6936900\textsuperscript{b} \\
N                    &   -54.4044538    &    -54.4045465  &  -54.4045484 &    \\ %-54.4044691\textsuperscript{b} \\
F                    &   -99.4162084    &    -99.4163041  &  -99.4163062 &    \\%-99.4160879\textsuperscript{b} \\
P                    &  -340.7191678    &   -340.7192694  & -340.7192755 &   \\%-340.7191600\textsuperscript{b} \\
Mg                   &  -199.6141332    &   -199.6146323  & -199.6146367 &   -199.6146363\\%\textsuperscript{a} \\
Br                   & -2572.4483901    &  -2572.4485011  &-2572.4485255 &  \\%-2572.4483668\textsuperscript{b} \\
Sr                   & -3131.5455474    &  -3131.5456566  &-3131.5456855 &  -3131.545686\\%\textsuperscript{a} \\
NH\textsubscript{3}  &   -56.2252214    &    -56.2255208  &  -56.2255258 & \\
PH\textsubscript{3}  &  -342.4949244    &   -342.4952146  & -342.4952195 & \\
HBr                  & -2573.0521070    &  -2573.0522690  &-2573.0523110 & \\
BeF\textsubscript{2} &  -213.7897799    &   -213.7900540  & -213.7900610 & \\
MgF\textsubscript{2} &  -398.7303932    &   -398.7306825  & -398.7307037 & \\
SrF\textsubscript{2} & -3330.6845847    &  -3330.6848854  &-3330.6849449 & \\ \hline

    \multicolumn{5}{|l|}{\bf Atomization energies (kcal./mol.)} \\ \hline

    {\bf System}     &  {\bf Default} &  {\bf Current}  & {\bf High}  & {\bf $|\mbox{Curr. -High}|$} \\ \hline

NH\textsubscript{3}  & 201.372        &   201.415    &  201.417  & 0.002 \\
PH\textsubscript{3}  & 173.127        &   173.159    &  173.158  & 0.001 \\
HBr                  &  65.113        &    65.116    &   65.126  & 0.010 \\
BeF\textsubscript{2} & 241.231        &   241.230    &  241.231  & 0.001 \\
MgF\textsubscript{2} & 178.114        &   177.863    &  177.871  & 0.008 \\
SrF\textsubscript{2} & 192.407        &   192.408    &  192.424  & 0.020 \\ \hline

    \multicolumn{5}{|l|}{\bf Dipole moments (a.u.)} \\ \hline

    {\bf System}     &  {\bf Default} &  {\bf Current}  & {\bf High}  & {\bf $|\mbox{Curr. -High}|$} \\ \hline
NH\textsubscript{3}  & 0.608674      & 0.608617       & 0.608079   & 0.00054 \\
PH\textsubscript{3}  & 0.300499      & 0.300191       & 0.300134   & 0.00006 \\
HBr                  & 0.371128      & 0.371028       & 0.370903   & 0.00013 \\ \hline

\multicolumn{5}{|l|}{\bf Geometries (a.u.)} \\ \hline
    {\bf System} & \multicolumn{4}{l|}{\bf Geometry} \\ \hline
   NH\textsubscript{3} & \multicolumn{4}{l|}{N(-0.201,0,0);  H(0.468,1.763,0);  H(0.468,-0.882,$\pm$1.527)} \\
   HBr                 & \multicolumn{4}{l|}{Br(0,0,-0.074); H(0,0,2.595)} \\
   BeF\textsubscript{2}& \multicolumn{4}{l|}{Be(0,0,0), F(0,0,$\pm$2.627)} \\
   MgF\textsubscript{2}& \multicolumn{4}{l|}{Mg(0,0,0), F(0,0,$\pm$3.286)} \\
   SrF\textsubscript{2}& \multicolumn{4}{l|}{Sr(0,0,0), F(0,0,$\pm$4.112)} \\ \hline
  \end{tabular}
  \end{center}
  \caption{\label{table:mraconv} Hartree-Fock results obtained using (1) {\bf default} MADNESS parameters (truncation threshold $\epsilon$=10\textsuperscript{-6}; density convergence threshold $\Delta\rho$=10\textsuperscript{-4}; smoothing error $\tau$=10\textsuperscript{-4}\Eh /atom), (2) {\bf current} parameters used in this study which are default parameters modified with $\tau$=10\textsuperscript{-6}, and (3) {\bf high} accuracy settings $\epsilon=$10\textsuperscript{-8}; $\Delta\rho$=10\textsuperscript{-6}; $\tau$=10\textsuperscript{-7}).  Reference data from \cite{bbbc-1992}. Open shell systems employ spin-unrestricted HF.}
    
\end{table}


